import com.android.build.api.dsl.LibraryExtension
import org.jetbrains.kotlin.gradle.dsl.KotlinJvmOptions

@Suppress("DSL_SCOPE_VIOLATION")
plugins {
    alias(libs.plugins.android.library) apply false
    alias(libs.plugins.kotlin.android) apply false
}

group =  "org.crumbs"
version = libs.versions.crumbs.library.full.get()

allprojects {
    this.group = rootProject.group
    this.version = rootProject.version

    plugins.withId("com.android.library") {
        project.configure<LibraryExtension> {
            compileSdk = libs.versions.android.target.sdk.get().toInt()

            defaultConfig {
                minSdk = libs.versions.android.min.sdk.get().toInt()
                targetSdk = compileSdk
                testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
                consumerProguardFiles("consumer-rules.pro")
            }

            buildTypes {
                release {
                    isMinifyEnabled = false
                    proguardFiles(
                        getDefaultProguardFile("proguard-android-optimize.txt"),
                        "proguard-rules.pro"
                    )
                }
            }

            packagingOptions {
                resources.excludes.add("META-INF/*.kotlin_module")
                resources.excludes.add("META-INF/LICENSE*.md")
            }

            compileOptions {
                sourceCompatibility = JavaVersion.VERSION_1_8
                targetCompatibility = JavaVersion.VERSION_1_8
            }

            val extension = this as ExtensionAware
            plugins.withId("kotlin-android") {
                extension.extensions.configure<KotlinJvmOptions>("kotlinOptions") {
                    jvmTarget = JavaVersion.VERSION_1_8.toString()
                }
            }

        }
    }
}

tasks.register<Delete>("clean") {
    delete(rootProject.buildDir)
}