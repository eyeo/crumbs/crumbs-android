rootProject.name = "crumbs-android"

pluginManagement {
    repositories {
        gradlePluginPortal()
        google()
        mavenCentral()
    }
}
dependencyResolutionManagement {
    versionCatalogs {
        val isDevelop = System.getenv("CI_COMMIT_BRANCH") != "master"
        val versionSuffix = if (isDevelop) "-SNAPSHOT" else ""
        val isSharedFeature = System.getenv("CI_COMMIT_BRANCH")?.startsWith("feat/shared/") == true
        val versionName =
            if (isSharedFeature) "-" + System.getenv("CI_COMMIT_BRANCH").replace("feat/shared/", "")
                .replace("_", "-").toLowerCase() else ""

        val regex = Regex("crumbs-library-prefix += +\"(.*)\"")
        val crumbsVersion = file("gradle/libs.versions.toml").reader().useLines { lines ->
            val line = lines.first { regex.matches(it) }
            regex.find(line)!!.groupValues.last()
        }
        create("libs") {
            version("crumbs-library-full", crumbsVersion + versionName + versionSuffix)
        }
    }
    repositoriesMode.set(RepositoriesMode.FAIL_ON_PROJECT_REPOS)
    repositories {
        mavenLocal()
        google()
        mavenCentral()
        maven("https://gitlab.com/api/v4/groups/775344/-/packages/maven")
    }
}

include(":crumbs-abp")
include(":crumbs-chromium")
include(":crumbs-ui")
include(":crumbs-webview")
include(":crumbs-test")
include(":demo")
include(":docs")
