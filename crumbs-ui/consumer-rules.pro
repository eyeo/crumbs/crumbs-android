-keep class androidx.preference.SwitchPreferenceCompat
-keepclassmembers class androidx.preference.SwitchPreferenceCompat { *; }
-keep class com.google.android.material.bottomsheet.BottomSheetBehavior { *; }
-keep class androidx.preference.CheckBoxPreference

-keepclassmembers class * {
    @android.webkit.JavascriptInterface <methods>;
}

-keep class org.crumbs.ui.CrumbsInterestsFragment
-keep class org.crumbs.ui.relay.CrumbsEmailRelayFragment

#################   Crumbs   #################
-keepclassmembers class org.crumbs.* {
    @android.webkit.JavascriptInterface <methods>;
}