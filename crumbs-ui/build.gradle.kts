@Suppress("DSL_SCOPE_VIOLATION")
plugins {
    `maven-publish`
    alias(libs.plugins.android.library)
    alias(libs.plugins.kotlin.android)
}

android {
    defaultConfig {
        namespace = "org.crumbs.ui"
        vectorDrawables.useSupportLibrary = true
        multiDexEnabled = true
    }

    buildFeatures {
        viewBinding = true
    }

    testOptions {
        animationsDisabled = true
    }
}

dependencies {
    api(libs.crumbs.core)

    implementation(libs.kotlin.stdlib)
    implementation(libs.androidx.appcompat)
    implementation(libs.androidx.constraintlayout)
    implementation(libs.androidx.lifecycle)
    implementation(libs.androidx.preference.ktx)
    implementation(libs.google.flexbox)
    implementation(libs.google.material)
    implementation(libs.kotlinx.serialization.core)
    implementation(libs.ktor.client.android)

    testImplementation(libs.test.junit)

    androidTestImplementation(project(":crumbs-test"))
    androidTestImplementation(libs.androidx.multidex)
    androidTestImplementation(libs.test.androidx.junit)
    androidTestImplementation(libs.test.androidx.rules)
    androidTestImplementation(libs.test.androidx.espresso.core)
    androidTestImplementation(libs.test.androidx.espresso.contrib)
    androidTestImplementation(libs.test.mockk.android)
    androidTestImplementation(libs.kotlinx.datetime)
}

val interestsPath = "res/raw/data_interests"


val updateInterestFile = tasks.register<ConvertInterestsTask>("updateInterestsFile") {
    interestsUrl = "https://easylist-downloads.adblockplus.org/meta-interests.json"
    outputFile = project.file("src/main/res/xml/interests.xml")
}

tasks.register<TranslateInterestsTask>("updateInterestsTranslations") {
    inputFile = project.file("src/main/res/xml/interests.xml")
    apiKey = System.getenv("GOOGLE_TRANSLATE_API_KEY")
    dependsOn(updateInterestFile)
}

val connectedAndroidTest = "connectedAndroidTest"
val screenShotsFolder = "Download/screenshots_crumbs"
val cleanScreenShotsTask = tasks.register<Exec>("cleanScreenShots") {
    commandLine("adb", "shell", "rm", "-r", "/sdcard/$screenShotsFolder")
    isIgnoreExitValue = true
}

tasks.register("updateScreenShots") {
    dependsOn("connectedAndroidTest", cleanScreenShotsTask)
    doLast {
        val outFolder = rootProject.file("docs/assets")
        File(outFolder, screenShotsFolder).deleteRecursively()
        exec {
            commandLine("adb", "pull", "/sdcard/$screenShotsFolder")
            workingDir = outFolder
        }
        exec {
            commandLine("adb", "shell", "rm", "-r", "/sdcard/$screenShotsFolder")
        }
    }
}

afterEvaluate {
    tasks.getByName(connectedAndroidTest).dependsOn(cleanScreenShotsTask)
}

extra["artifact_id"] = "crumbs-ui-android"

apply(file("../publish.gradle"))