package org.crumbs.ui.utils

import android.annotation.SuppressLint
import android.content.ComponentName
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.util.TypedValue
import android.widget.FrameLayout
import androidx.annotation.RestrictTo
import androidx.annotation.StyleRes
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.util.Preconditions
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.test.core.app.ActivityScenario
import androidx.test.core.app.ApplicationProvider
import org.crumbs.ui.R


class FragmentScenario<F : Fragment?> private constructor(
    fragmentClass: Class<F>,
    activityScenario: ActivityScenario<EmptyFragmentActivity>
) {

    private val mFragmentClass: Class<F> = fragmentClass
    private val mActivityScenario: ActivityScenario<EmptyFragmentActivity> = activityScenario


    @RestrictTo(RestrictTo.Scope.LIBRARY)
    class EmptyFragmentActivity : AppCompatActivity() {
        @SuppressLint("RestrictedApi")
        override fun onCreate(savedInstanceState: Bundle?) {
            setTheme(
                intent.getIntExtra(
                    THEME_EXTRAS_BUNDLE_KEY,
                    R.style.Theme_Crumbs_Settings_Primary
                )
            )
            super.onCreate(savedInstanceState)
            val rootView = FrameLayout(this).apply {
                id = R.id.crumbs_activity_container

                val a = TypedValue()
                theme.resolveAttribute(android.R.attr.windowBackground, a, true)
                if (a.type >= TypedValue.TYPE_FIRST_COLOR_INT && a.type <= TypedValue.TYPE_LAST_COLOR_INT) {
                    // windowBackground is a color
                    setBackgroundColor(a.data)
                } else {
                    // windowBackground is not a color, probably a drawable
                    background = ContextCompat.getDrawable(context, a.resourceId)
                }
            }
            setContentView(rootView)
        }

        companion object {
            const val THEME_EXTRAS_BUNDLE_KEY = "THEME_EXTRAS_BUNDLE_KEY"
        }
    }

    fun moveActivityToState(newState: Lifecycle.State): FragmentScenario<F> {
        mActivityScenario.moveToState(newState)
        return this
    }

    fun moveToState(newState: Lifecycle.State): FragmentScenario<F> {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N && newState == Lifecycle.State.STARTED) {
            throw UnsupportedOperationException(
                "Moving state to STARTED is not supported on Android API level 23 and lower."
                        + " This restriction comes from the combination of the Android framework bug"
                        + " around the timing of onSaveInstanceState invocation and its workaround code"
                        + " in FragmentActivity. See http://issuetracker.google.com/65665621#comment3"
                        + " for more information."
            )
        }
        if (newState == Lifecycle.State.DESTROYED) {
            mActivityScenario.onActivity { activity ->
                val fragment = activity.supportFragmentManager.findFragmentByTag(
                    FRAGMENT_TAG
                )
                // Null means the fragment has been destroyed already.
                if (fragment != null) {
                    activity
                        .supportFragmentManager
                        .beginTransaction()
                        .remove(fragment)
                        .commitNowAllowingStateLoss()
                }
            }
        } else {
            mActivityScenario.onActivity { activity ->
                val fragment = activity.supportFragmentManager.findFragmentByTag(
                    FRAGMENT_TAG
                )
                Preconditions.checkNotNull(
                    fragment,
                    "The fragment has been removed from FragmentManager already."
                )
            }
            mActivityScenario.moveToState(newState)
        }
        return this
    }

    /**
     * Recreates the host Activity.
     *
     *
     * After this method call, it is ensured that the Fragment state goes back to the same state
     * as its previous state.
     *
     *
     * This method cannot be called from the main thread.
     */
    fun recreate(): FragmentScenario<F> {
        mActivityScenario.recreate()
        return this
    }

    fun onFragment(action: (F) -> Unit): FragmentScenario<F> {
        mActivityScenario.onActivity { activity ->
            val fragment = activity.supportFragmentManager.findFragmentByTag(
                FRAGMENT_TAG
            )
            Preconditions.checkNotNull(
                fragment,
                "The fragment has been removed from FragmentManager already."
            )
            Preconditions.checkState(mFragmentClass.isInstance(fragment))
            action(
                Preconditions.checkNotNull(
                    mFragmentClass.cast(
                        fragment
                    )
                )
            )
        }
        return this
    }

    companion object {
        private const val FRAGMENT_TAG = "FragmentScenario_Fragment_Tag"

        inline fun <reified F : Fragment> launchInContainer(
            fragmentArgs: Bundle? = null,
            @StyleRes themeResId: Int = R.style.Theme_AppCompat_DayNight,
        ): FragmentScenario<F> {
            return internalLaunch(
                F::class.java, fragmentArgs, themeResId
            )
        }

        @SuppressLint("RestrictedApi")
        fun <F : Fragment?> internalLaunch(
            fragmentClass: Class<F>, fragmentArgs: Bundle?,
            @StyleRes themeResId: Int,
        ): FragmentScenario<F> {
            val startActivityIntent = Intent.makeMainActivity(
                ComponentName(
                    ApplicationProvider.getApplicationContext(),
                    EmptyFragmentActivity::class.java
                )
            )
                .putExtra(EmptyFragmentActivity.THEME_EXTRAS_BUNDLE_KEY, themeResId)
            val scenario = FragmentScenario(
                fragmentClass,
                ActivityScenario.launch(startActivityIntent)
            )
            scenario.mActivityScenario.onActivity { activity ->
                val fragment = activity.supportFragmentManager
                    .fragmentFactory.instantiate(
                        Preconditions.checkNotNull(
                            fragmentClass.classLoader
                        ),
                        fragmentClass.name
                    )
                fragment.arguments = fragmentArgs
                activity.supportFragmentManager
                    .beginTransaction()
                    .add(
                        R.id.crumbs_activity_container,
                        fragment,
                        FRAGMENT_TAG
                    )
                    .commitNow()
            }
            return scenario
        }
    }
}