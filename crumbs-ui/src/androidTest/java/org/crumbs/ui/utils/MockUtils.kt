package org.crumbs.ui.utils

import io.mockk.CapturingSlot
import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.*
import org.crumbs.CrumbsCore
import org.crumbs.CrumbsProvider
import org.crumbs.models.CrumbsInterest
import org.crumbs.models.ProfileInterest

private fun replaceValue(data: MutableList<ProfileInterest>, newValue: ProfileInterest) {
    val index = data.indexOfFirst { it.id == newValue.id }
    data.removeAt(index)
    data.add(index, newValue)
}

private fun calculateShareable(data: MutableList<ProfileInterest>) {
    val result = data.asSequence().map { it.copy(shareable = false) }.toMutableList()
    result.asSequence().sortedBy { -it.score }
        .filter { it.enabled && it.hasEnoughData }
        .take(10)
        .map { it.copy(shareable = true) }.forEach {
            replaceValue(result, it)
        }
    data.clear()
    data.addAll(result)
}


fun mockInterests(): MutableList<ProfileInterest> {
    return runBlocking {
        val data = arrayListOf(
            ProfileInterest(
                CrumbsInterest(207, "Academic Interests", null, 206, "📚"),
                enabled = true, shareable = false, score = 40f,
            ),
            ProfileInterest(
                CrumbsInterest(208, "Arts and Humanities", null, 207, "🎭"),
                enabled = true, shareable = false, score = 39f,
            ),
            ProfileInterest(
                CrumbsInterest(214, "Language Learning", null, 207, "💬"),
                enabled = true, shareable = false, score = 38f,
            ),
            ProfileInterest(
                CrumbsInterest(215, "Life Sciences", null, 207, "🧬"),
                enabled = true, shareable = false, score = 37f,
            ),
            ProfileInterest(
                CrumbsInterest(223, "Physical Science and Engineering", null, 207, "⚙️"),
                enabled = true, shareable = false, score = 36f,
            ),
            ProfileInterest(
                CrumbsInterest(233, "Social Sciences", null, 207, "📖"),
                enabled = true, shareable = false, score = 35f,
            ),
            ProfileInterest(
                CrumbsInterest(243, "Automotive", null, 206, "🚙"),
                enabled = true, shareable = false, score = 34f,
            ),
            ProfileInterest(
                CrumbsInterest(244, "Auto Buying and Selling", null, 243, "🚗"),
                enabled = true, shareable = false, score = 33f,
            ),
            ProfileInterest(
                CrumbsInterest(245, "Auto Shows", null, 243, "🏎"),
                enabled = true, shareable = false, score = 32f,
            ),
            ProfileInterest(
                CrumbsInterest(246, "Auto Technology", null, 243, "🚗"),
                enabled = true, shareable = false, score = 31f,
            ),
            ProfileInterest(
                CrumbsInterest(247, "Budget Cars", null, 243, "🚗"),
                enabled = true, shareable = false, score = 30f,
            ),
            ProfileInterest(
                CrumbsInterest(248, "Car Culture", null, 243, "🚗"),
                enabled = false, shareable = false, score = 29f,
            ),
        )
        calculateShareable(data)

        val callbackSlot: CapturingSlot<(List<ProfileInterest>) -> Unit> = CapturingSlot()
        val mockCrumbs = mockk<CrumbsCore>(relaxed = true) {
            every { interests().getSettings().enabled }.returns(true)
            every { interests().listenProfileInterests(any(), capture(callbackSlot)) } answers {
                callbackSlot.captured(data)
                mockk(relaxed = true)
            }
            every { interests().resetInterest(any()) } answers {
                GlobalScope.launch(Dispatchers.Main) {
                    withContext(Dispatchers.Default) {
                        val first = data.first { it.id == firstArg() }
                        replaceValue(data, first.copy(score = 0f))
                        calculateShareable(data)
                    }
                    callbackSlot.captured(data)
                }
            }
            every {
                interests().setInterestState(
                    any(),
                    any()
                )
            } answers {
                GlobalScope.launch(Dispatchers.Main) {
                    withContext(Dispatchers.Default) {
                        val first = data.first { it.id == firstArg() }
                        replaceValue(data, first.copy(enabled = secondArg()))
                        calculateShareable(data)
                    }
                    callbackSlot.captured(data)
                }
            }
            every { privacy().getSettings().enabled } returns true
        }
        CrumbsProvider.testInstance = mockCrumbs

        data
    }
}
