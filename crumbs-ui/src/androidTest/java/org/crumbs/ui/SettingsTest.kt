package org.crumbs.ui

import androidx.appcompat.app.AppCompatDelegate
import androidx.appcompat.app.AppCompatDelegate.MODE_NIGHT_YES
import androidx.test.core.app.ActivityScenario.launch
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.doesNotExist
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withText
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.MediumTest
import androidx.test.platform.app.InstrumentationRegistry
import org.crumbs.CrumbsAndroid
import org.crumbs.ui.relay.CrumbsEmailRelayFragment
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith

@MediumTest
@RunWith(AndroidJUnit4::class)
class SettingsTest {


    @Test
    fun testSettingsActivity() {
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        CrumbsAndroid.setup(context)
        AppCompatDelegate.setDefaultNightMode(MODE_NIGHT_YES)

        //tag::settingsActivity[]
        var settingsIntent = CrumbsSettingsActivity.createIntent(context)
        //end::settingsActivity[]

        launch<CrumbsSettingsActivity>(settingsIntent).onActivity { activity ->
            val fragment =
                activity.supportFragmentManager.fragments.find { it is CrumbsSettingsFragment }
            Assert.assertEquals(fragment?.javaClass, CrumbsSettingsFragment::class.java)
        }

        onView(withText(R.string.crumbs_privacy_title)).check(matches(isDisplayed()))
        onView(withText(R.string.crumbs_interests)).check(matches(isDisplayed()))
        onView(withText(R.string.crumbs_email_relay)).check(matches(isDisplayed()))

        //tag::settingsActivityExtra[]
        settingsIntent = CrumbsSettingsActivity.createIntent(
            context,
            selectedFeature = CrumbsSettingsActivity.INTERESTS_FEATURE_ID,
            parentIntent = null, // up navigation
            disabledFeatures = intArrayOf(CrumbsSettingsActivity.EMAIL_RELAY_FEATURE_ID)
        )
        //end::settingsActivityExtra[]

        launch<CrumbsSettingsActivity>(settingsIntent).onActivity { activity ->
            val fragment =
                activity.supportFragmentManager.fragments.find { it is CrumbsInterestsFragment }
            Assert.assertEquals(fragment?.javaClass, CrumbsInterestsFragment::class.java)
        }

        onView(withText(R.string.crumbs_privacy_title)).check(doesNotExist())
        onView(withText(R.string.crumbs_interests)).check(matches(isDisplayed()))
        onView(withText(R.string.crumbs_email_relay)).check(doesNotExist())

        settingsIntent = CrumbsSettingsActivity.createIntent(
            context,
            selectedFeature = R.id.crumbs_email_relay,
        )

        launch<CrumbsSettingsActivity>(settingsIntent).onActivity { activity ->
            val fragment =
                activity.supportFragmentManager.fragments.find { it is CrumbsEmailRelayFragment }
            Assert.assertEquals(fragment?.javaClass, CrumbsEmailRelayFragment::class.java)
        }
        CrumbsAndroid.release()
    }
}