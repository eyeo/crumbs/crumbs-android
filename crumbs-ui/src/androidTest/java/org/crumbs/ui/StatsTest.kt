package org.crumbs.ui

import android.widget.FrameLayout
import androidx.lifecycle.Lifecycle
import androidx.test.core.app.ActivityScenario.launch
import androidx.test.espresso.Espresso
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.MediumTest
import androidx.test.platform.app.InstrumentationRegistry
import org.crumbs.CrumbsAndroid
import org.crumbs.ui.utils.FragmentScenario
import org.crumbs.ui.view.CrumbsButton
import org.crumbs.ui.view.CrumbsPopup
import org.crumbs.ui.view.CrumbsUIContext
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@MediumTest
@RunWith(AndroidJUnit4::class)
class StatsTest {

    private val context = InstrumentationRegistry.getInstrumentation().targetContext

    @Before
    fun setup() {
        CrumbsAndroid.setup(context)
    }

    @After
    fun release(){
        CrumbsAndroid.release()
    }

    @Test
    fun testPopup() {
        launch(FragmentScenario.EmptyFragmentActivity::class.java, null)
            .moveToState(Lifecycle.State.RESUMED)
            .onActivity { activity ->
                val crumbsButton = CrumbsButton(activity)
                activity.setContentView(FrameLayout(activity).apply {
                    addView(
                        crumbsButton,
                        FrameLayout.LayoutParams(
                            FrameLayout.LayoutParams.WRAP_CONTENT,
                            FrameLayout.LayoutParams.WRAP_CONTENT
                        )
                    )
                })
                //tag::CrumbsPopup[]
                CrumbsPopup(activity)
                    .show(anchor = crumbsButton, crumbsUIContext = CrumbsUIContext.Default)
                //end::CrumbsPopup[]
            }
        Espresso.onView(ViewMatchers.withId(R.id.crumbs_board_bottom))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
    }
}