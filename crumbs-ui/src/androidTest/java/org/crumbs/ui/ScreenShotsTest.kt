package org.crumbs.ui

import android.content.res.Resources
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.appcompat.app.AppCompatDelegate.MODE_NIGHT_NO
import androidx.appcompat.app.AppCompatDelegate.MODE_NIGHT_YES
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.test.core.app.ActivityScenario.launch
import androidx.test.espresso.Espresso.onIdle
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.MediumTest
import androidx.test.internal.runner.junit4.statement.UiThreadStatement
import androidx.test.platform.app.InstrumentationRegistry
import com.google.android.material.card.MaterialCardView
import io.mockk.clearMocks
import io.mockk.every
import io.mockk.mockk
import org.crumbs.CrumbsAndroid
import org.crumbs.CrumbsCore
import org.crumbs.CrumbsProvider
import org.crumbs.models.*
import org.crumbs.test.captureScreenshot
import org.crumbs.ui.relay.CrumbsEmailRelayDialogFragment
import org.crumbs.ui.relay.CrumbsEmailRelayFragment
import org.crumbs.ui.relay.CrumbsEmailRelaySetupDialogFragment
import org.crumbs.ui.tnc.CrumbsTnCDialogFragment
import org.crumbs.ui.utils.FragmentScenario
import org.crumbs.ui.utils.mockInterests
import org.crumbs.ui.utils.showOnce
import org.crumbs.ui.view.*
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@MediumTest
@RunWith(AndroidJUnit4::class)
class ScreenShotsTest {

    private val context = InstrumentationRegistry.getInstrumentation().targetContext

    @Before
    fun setup() {
        CrumbsAndroid.setup(context)
        CrumbsAndroid.get().privacy().editSettings().enable(true).apply()
        CrumbsAndroid.get().interests().editSettings().enable(true).apply()
    }

    @After
    fun release() {
        CrumbsAndroid.release()
        CrumbsProvider.testInstance = null
    }

    @Test
    fun capturePrivacyFragment() {
        captureFragment<CrumbsSettingsFragment>("settings_fragment")
    }

    @Test
    fun captureEmailRelayFragment() {
        captureFragment<CrumbsEmailRelayFragment>("email_relay_fragment")
    }

    @Test
    fun captureInterestsFragment() {
        mockInterests()
        captureFragment<CrumbsInterestsFragment>("interests_fragment")
    }

    @Test
    fun captureStatsView() {
        captureView("stats_view", width = toDp(340)) {
            CrumbsStatsView(it).let { statsView ->
                val crumbsStats =
                    CrumbsStats(CrumbsStatsData(12, 8), CrumbsStatsData(100000, 100000))
                //tag::CrumbsStatsView[]
                statsView.setStats(crumbsStats)
                //end::CrumbsStatsView[]
                statsView
            }
        }
    }

    @Test
    fun captureBoardView() {
        captureView("board_view", width = toDp(340)) {
            CrumbsBoardView(it).apply {
                CrumbsUIContext.Default.setActiveTabUrl("0", "https://crumbs.org")
            }
        }
    }

    @Test
    fun captureInterestsBoardView() {
        captureView("interests_board_view", width = toDp(340)) {
            CrumbsInterestsBoardView(it).apply {
                setInterests("https://crumbs.org", emptyList())
            }
        }

        captureView("interests_board_view_share", width = toDp(340)) {
            CrumbsInterestsBoardView(it).let { interestsBoardView ->
                val categories = listOf(
                    CrumbsInterest(-1, "English", "en-US", null, "\uD83C\uDF10"),
                    CrumbsInterest(-2, "Device", "Android 11, mobile", null, "\uD83D\uDC64"),
                    CrumbsInterest(-3, "Location", "USA", null, "\uD83D\uDCCD"),
                    CrumbsInterest(23, "Travel", null, null, "\uD83D\uDDFA"),
                    CrumbsInterest(22, "Technology and Computing", null, null, "\uD83D\uDCBB"),
                )
                val domain = "https://crumbs.org"
                //tag::CrumbsInterestsBoardView[]
                interestsBoardView.setInterests(domain, categories)
                //end::CrumbsInterestsBoardView[]
                interestsBoardView
            }
        }
    }

    @Test
    fun captureButton() {
        val currentStats = CrumbsStats(CrumbsStatsData(50, 10), CrumbsStatsData(250, 250))


        val mockCrumbs = mockk<CrumbsCore>(relaxed = true) {
            every { stats().listen(any(), any()) } answers {
                secondArg<(CrumbsStats) -> Unit>()(currentStats)
                mockk(relaxed = true)
            }
        }
        CrumbsProvider.testInstance = mockCrumbs
        captureView("button") {
            CrumbsButton(it).apply {
                setCrumbsUIContext(mockk(relaxed = true) {
                    every { registerListener(any()) } answers {
                        firstArg<CrumbsUIContext.UIContextListener>().onProtectionUpdated(
                            CrumbsUIContext.ProtectionStatus.ENABLED
                        )
                        mockk(relaxed = true)
                    }
                })
                onTabChanged("0", "https://www.google.com")
            }
        }

        clearMocks(mockCrumbs)
        captureView("button_paused") {
            CrumbsButton(it).apply {
                setCrumbsUIContext(mockk(relaxed = true) {
                    every { registerListener(any()) } answers {
                        firstArg<CrumbsUIContext.UIContextListener>().onProtectionUpdated(
                            CrumbsUIContext.ProtectionStatus.PAUSED
                        )
                        mockk(relaxed = true)
                    }
                })
                onTabChanged("0", "https://www.google.com")
            }
        }

        captureView("button_disabled") {
            CrumbsButton(it).apply {
                setCrumbsUIContext(mockk(relaxed = true) {
                    every { registerListener(any()) } answers {
                        firstArg<CrumbsUIContext.UIContextListener>().onProtectionUpdated(
                            CrumbsUIContext.ProtectionStatus.PAUSED_UNTIL_NEXT_SESSION
                        )
                        mockk(relaxed = true)
                    }
                })
                onTabChanged("0", "https://www.google.com")
            }
        }
    }

    @Test
    fun captureEmailRelayDialogFragment() {
        CrumbsProvider.testInstance = mockk(relaxed = true) {
            every { emailRelay().getSettings().enabled }.returns(true)
            every { emailRelay().isSignedIn() }.returns(true)
            every { emailRelay().retrieve(any(), any(), any()) } answers {
                secondArg<(List<CrumbsEmailAlias>) -> Unit>()(
                    listOf(
                        "sgnzenjufc@relay.crumbs.org",
                        "qsdqsdqsdjj@relay.crumbs.org"
                    ).mapIndexed { index, email ->
                        CrumbsEmailAlias(
                            index, email,
                            isEnabled = true,
                            isForwarding = true,
                            forwarded = 0,
                            spam = 0,
                            blocked = 0
                        )
                    }
                )
                mockk(relaxed = true)
            }
        }

        for (x in THEME_MODES.indices) {
            UiThreadStatement.runOnUiThread { AppCompatDelegate.setDefaultNightMode(THEME_MODES[x]) }
            val scenario = launch(FragmentScenario.EmptyFragmentActivity::class.java)
                .onActivity { activity ->
                    val pageUrl = "https://crumbs.org"
                    //tag::CrumbsEmailRelayDialogFragment[]
                    val dialogFragment = CrumbsEmailRelayDialogFragment.newInstance(pageUrl)
                    dialogFragment.setEmailClickListener(object :
                        CrumbsEmailRelayDialogFragment.EmailClickListener {
                        override fun onEmailClicked(email: String) {
                            // inject email in the field
                        }
                    })
                    dialogFragment.show(activity.supportFragmentManager, "crumbs")
                    //end::CrumbsEmailRelayDialogFragment[]
                }.moveToState(Lifecycle.State.RESUMED)
            onIdle()
            Thread.sleep(CAPTURE_SLEEP_DELAY)
            scenario.onActivity {
                val fragment = it.supportFragmentManager.findFragmentByTag("crumbs")
                captureScreenshot(
                    fragment?.view?.parent as View,
                    "email_relay_dialog_fragment_${THEME_LABELS[x]}"
                )
            }.moveToState(Lifecycle.State.DESTROYED)
        }
    }

    @Test
    fun captureEmailRelaySetupDialogFragment() {
        CrumbsProvider.testInstance = mockk(relaxed = true) {
            every { emailRelay().getSettings().enabled }.returns(true)
            every { emailRelay().isSignedIn() }.returns(false)
        }

        for (x in THEME_MODES.indices) {
            UiThreadStatement.runOnUiThread { AppCompatDelegate.setDefaultNightMode(THEME_MODES[x]) }
            val scenario = launch(FragmentScenario.EmptyFragmentActivity::class.java)
                .onActivity { activity ->
                    //tag::CrumbsEmailRelaySetupDialogFragment[]
                    CrumbsEmailRelaySetupDialogFragment.newInstance()
                        .showOnce(activity.supportFragmentManager, "crumbs")
                    //end::CrumbsEmailRelaySetupDialogFragment[]
                }.moveToState(Lifecycle.State.RESUMED)
            onIdle()
            Thread.sleep(CAPTURE_SLEEP_DELAY)
            scenario.onActivity {
                val fragment = it.supportFragmentManager.findFragmentByTag("crumbs")
                captureScreenshot(
                    fragment?.view?.parent as View,
                    "email_relay_setup_dialog_fragment_${THEME_LABELS[x]}"
                )
            }.moveToState(Lifecycle.State.DESTROYED)
        }
    }

    @Test
    fun captureTnCDialogFragment() {
        CrumbsProvider.testInstance = mockk(relaxed = true) {
            every { emailRelay().getSettings().enabled }.returns(true)
            every { emailRelay().isSignedIn() }.returns(false)
        }

        for (x in THEME_MODES.indices) {
            UiThreadStatement.runOnUiThread { AppCompatDelegate.setDefaultNightMode(THEME_MODES[x]) }
            val scenario = launch(FragmentScenario.EmptyFragmentActivity::class.java)
                .onActivity { activity ->
                    //tag::CrumbsTnCDialogFragment[]
                    CrumbsUI.get().preferences.termsAndConditionsDialog
                    //end::CrumbsTnCDialogFragment[]
                    CrumbsTnCDialogFragment.newInstance()
                        .showOnce(activity.supportFragmentManager, CrumbsTnCDialogFragment.TAG)
                }.moveToState(Lifecycle.State.RESUMED)
            onIdle()
            Thread.sleep(CAPTURE_SLEEP_DELAY)
            scenario.onActivity {
                val fragment =
                    it.supportFragmentManager.findFragmentByTag(CrumbsTnCDialogFragment.TAG) as CrumbsTnCDialogFragment
                captureScreenshot(
                    fragment.dialog?.window?.decorView as View,
                    "tnc_dialog_fragment_${THEME_LABELS[x]}"
                )
            }.moveToState(Lifecycle.State.DESTROYED)
        }
    }

    private fun captureView(
        name: String,
        width: Int = FrameLayout.LayoutParams.WRAP_CONTENT,
        height: Int = FrameLayout.LayoutParams.WRAP_CONTENT,
        builder: (AppCompatActivity) -> View
    ) {
        for (x in THEME_MODES.indices) {
            UiThreadStatement.runOnUiThread { AppCompatDelegate.setDefaultNightMode(THEME_MODES[x]) }
            var view: View? = null
            val scenario =
                launch(FragmentScenario.EmptyFragmentActivity::class.java).onActivity {
                    view = MaterialCardView(it, null, R.attr.materialCardViewElevatedStyle).apply {
                        addView(builder(it), FrameLayout.LayoutParams(width, height))
                    }
                    it.setContentView(
                        FrameLayout(it).apply {
                            addView(
                                view,
                                FrameLayout.LayoutParams(
                                    FrameLayout.LayoutParams.WRAP_CONTENT,
                                    FrameLayout.LayoutParams.WRAP_CONTENT
                                )
                            )
                        },
                        ViewGroup.LayoutParams(
                            ViewGroup.LayoutParams.MATCH_PARENT,
                            ViewGroup.LayoutParams.MATCH_PARENT
                        )
                    )
                }.moveToState(Lifecycle.State.RESUMED)

            onIdle()
            Thread.sleep(CAPTURE_SLEEP_DELAY)
            scenario.onActivity {
                captureScreenshot(view!!, "${name}_${THEME_LABELS[x]}")
            }.moveToState(Lifecycle.State.DESTROYED)
        }
    }

    private inline fun <reified F : Fragment> captureFragment(name: String) {
        for (x in THEME_MODES.indices) {
            UiThreadStatement.runOnUiThread { AppCompatDelegate.setDefaultNightMode(THEME_MODES[x]) }
            val scenario =
                FragmentScenario.launchInContainer<F>(themeResId = R.style.Theme_Crumbs_Settings_Primary)
                    .moveToState(Lifecycle.State.RESUMED)
            onIdle()
            Thread.sleep(CAPTURE_SLEEP_DELAY)
            scenario
                .onFragment {
                    captureScreenshot(it.view?.parent as View, "${name}_${THEME_LABELS[x]}")
                }.moveActivityToState(Lifecycle.State.DESTROYED)
        }
    }

    private fun toDp(value: Int): Int {
        return (value * Resources.getSystem().displayMetrics.density).toInt()
    }


    companion object {
        private val THEME_MODES = arrayOf(MODE_NIGHT_YES, MODE_NIGHT_NO)
        private val THEME_LABELS = arrayOf("night", "light")
        private const val CAPTURE_SLEEP_DELAY = 400L
    }
}