const ICON_IMG_URL = "data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' id='Layer_1' data-name='Layer 1' viewBox='0 0 500 500'%3E%3Cdefs%3E%3Cstyle%3E.cls-1%7Bfill:%2300c49a;%7D%3C/style%3E%3C/defs%3E%3Cpath d='M266.22,48.58c77.74,0,142.54,38.28,177.87,104.24l-77.15,52.43c-25.33-42.41-58.9-63.62-101.31-63.62-61.84,0-106,43.59-106,109,0,32.39,10,58.31,29.45,78.33,20,19.44,45.35,29.45,76.57,29.45,42.41,0,76-21.2,101.31-63.61l77.15,52.42c-35.33,66-100.13,104.25-178.46,104.25C151.37,453.21,53.59,362.51,56,250,54.18,138.69,150.78,46.22,266.22,48.58Z'/%3E%3Ccircle class='cls-1' cx='268.03' cy='250.39' r='63.79'/%3E%3C/svg%3E";
const DEFAULT_INPUT_SELECTOR = 'input:not([disabled])[type="email"],input:not([disabled])[type="text"]';
const AUTO_COMPLETE_HEIGHT = "36px";
const THEME_COLOR = "#00C49A";
const ARROW_ROTATION = "rotate(-135deg)";

const emailRelayAlias = CrumbsRelayUISettings.mainAlias;
const createAliasLabel = CrumbsRelayUISettings.createAliasLabel;

const bgColor = emailRelayAlias ? THEME_COLOR : "white";
const textColor = emailRelayAlias ? "white" : THEME_COLOR;
const HTML = `
<div style="pointer-events: none; position: absolute;">
    <img src="${ICON_IMG_URL}" 
         style="pointer-events: auto; 
                cursor: pointer; 
                position: absolute; 
                top: 0; 
                right: 0; 
                bottom: 0; 
                margin: auto; 
                height: 80%;
                max-height: 80px;
                width: auto;
                min-height: 30px;"/>
    <div style="position: absolute;
                z-index: 100;
                text-align: center;
                top: 100%;
                right: 4px;
                left: 4px;
                height: 0;
                pointer-events: auto;
                cursor: pointer;
                line-height: ${AUTO_COMPLETE_HEIGHT};
                background: ${bgColor};
                border-radius: 8px;
                box-shadow:inset 0 0 0 2px ${THEME_COLOR};
                overflow: hidden;
                color: ${textColor};
                font-weight: bold;
                filter: drop-shadow(2px 2px 2px grey);
                transition: height 300ms ease;"
                ></div>
    <div style="position: absolute;
                z-index: 100;
                height: 10px;
                width: 10px;
                bottom: -7px;
                right: 11px;
                background: ${bgColor};
                border-right: 2px solid ${THEME_COLOR};
                border-bottom: 2px solid ${THEME_COLOR};
                transition: transform 300ms ease;
                transform: scale(0) ${ARROW_ROTATION};"
                ></div>
</div>`;

const inputs = [];

function onIconClicked() {
    CrumbsRelayAccountInterface.onRelayClicked(CrumbsRelayUISettings.tabId, document.URL)
}

function onInputFocusChanged(focused) {
    CrumbsRelayAccountInterface.onInputFocusChanged(CrumbsRelayUISettings.tabId, document.URL, focused)
}

function validateEmail(email) {
    const re = /([a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z0-9_-]+)/;
    return re.test(email);
}

function shouldAddIcon(input) {
    if (input.offsetWidth < 24 || input.offsetHeight < 24) {
        return;
    }

    const lookupAttributes = [
        'type', 'name', 'placeholder', 'id', 'title', 'aria-label',
        'data-tooltip', 'autocomplete',
    ];

    for (const attrName of lookupAttributes) {
        const value = (input.getAttribute(attrName) || '').toLowerCase();
        if (value.indexOf('email') > -1 || value.indexOf('e-mail') > -1 || validateEmail(value)) {
            return true;
        }
    }
    return false;
}

function setIcon(inputElement) {
    if (!shouldAddIcon(inputElement)) {
        return;
    }
    if (inputs.indexOf(inputElement) > -1) {
        return;
    }
    inputs.push(inputElement);


    let shadowDiv = document.createElement("div");
    shadowDiv.style.cssText = 'position: absolute !important; height: 0px !important; width: 0px !important; float: left !important;'

    inputElement.parentElement.insertBefore(shadowDiv, inputElement.nextSibling)
    const shadow = shadowDiv.attachShadow({ mode: "open" });

    var canOpen = true;
    let builder = document.createElement("div");
    escapeHTMLPolicy = trustedTypes.createPolicy("forceInner", {
        createHTML: (to_escape) => to_escape
    })
    builder.innerHTML = escapeHTMLPolicy.createHTML(HTML);
    const overlay = builder.firstElementChild;

    const overlayStyle = overlay.style;
    const resizeOverlay = function () {
        let inputPos = inputElement.getBoundingClientRect();
        let shadowPos = shadowDiv.getBoundingClientRect();
        overlayStyle.top = (inputPos.top - shadowPos.top) + "px";
        overlayStyle.left = (inputPos.left - shadowPos.left) + "px";
        overlayStyle.width = inputElement.offsetWidth + "px";
        overlayStyle.height = inputElement.offsetHeight + "px";
    }

    const ro = new ResizeObserver(resizeOverlay);
    const ao = new MutationObserver(resizeOverlay);
    const focusField = function () {
        inputElement.setAttribute("readonly", "readonly");
        inputElement.focus();
        setTimeout(function () {
            inputElement.removeAttribute("readonly");
        }, 300);
    }


    const button = overlay.firstElementChild;
    button.addEventListener("click", evt => {
        canOpen = false;
        focusField();
        onIconClicked();
    });

    const text = overlay.children[1];
    const arrow = overlay.lastElementChild;
    text.innerHTML = escapeHTMLPolicy.createHTML(emailRelayAlias ? emailRelayAlias : createAliasLabel);

    function openPopup() {
        text.style.height = AUTO_COMPLETE_HEIGHT;
        arrow.style.transform = "scale(1) " + ARROW_ROTATION;
    }

    function closePopup() {
        text.style.height = "0px";
        arrow.style.transform = "scale(0) " + ARROW_ROTATION;
    }

    text.addEventListener("click", evt => {
        focusField();
        if (emailRelayAlias) {
            inputElement.value = emailRelayAlias;
            inputElement.dispatchEvent(new Event('input', { bubbles: true }));
        } else {
            onIconClicked();
        }
        closePopup();
    });
    inputElement.addEventListener('focus', evt => {
        if (canOpen && !inputElement.value) {
            openPopup();
        }
        onInputFocusChanged(true);
    });
    inputElement.addEventListener('blur', evt => {
        closePopup();
        canOpen = true;
        onInputFocusChanged(false);
    });
    inputElement.addEventListener('input', evt => {
        if (inputElement.value) {
            closePopup();
        } else if (canOpen) {
            openPopup();
        }
    });
    resizeOverlay();
    shadow.appendChild(overlay)
    ro.observe(inputElement);
    ao.observe(inputElement, { attributes: true });
    if (inputElement.offsetParent) {
        ro.observe(inputElement.offsetParent);
    }
}

function setupIcons(newInputs) {
    for (const input of newInputs) {
        setIcon(input);
    }
}

function setupObserver() {
    if (typeof (CrumbsRelayAccountInterface) === 'undefined') {
        console.warn("CrumbsRelayAccountInterface not configured");
        return;
    }
    setupIcons(document.querySelectorAll(DEFAULT_INPUT_SELECTOR));
    var mutationObserver = new MutationObserver(function (mutations) {
        mutations.forEach(function (mutation) {
            if (mutation.addedNodes.length > 0) {
                setupIcons(mutation.target.querySelectorAll(DEFAULT_INPUT_SELECTOR));
            }
        });
    });
    mutationObserver.observe(document.body, { childList: true, subtree: true });
}

if (document.readyState === 'complete') {
    setupObserver();
} else {
    window.addEventListener('load', function () {
        setupObserver();
    });
}
