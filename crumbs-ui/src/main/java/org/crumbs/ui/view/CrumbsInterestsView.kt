package org.crumbs.ui.view

import android.content.Context
import android.content.res.Resources
import android.content.res.TypedArray
import android.graphics.Color
import android.util.AttributeSet
import android.util.TypedValue
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.google.android.flexbox.FlexDirection
import com.google.android.flexbox.FlexWrap
import com.google.android.flexbox.FlexboxLayout
import com.google.android.material.card.MaterialCardView
import org.crumbs.ui.R
import org.crumbs.models.CrumbsInterest
import org.crumbs.ui.utils.CrumbsFormatter

class CrumbsInterestsView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : FlexboxLayout(context, attrs, defStyleAttr) {

    init {
        flexDirection = FlexDirection.ROW
        flexWrap = FlexWrap.WRAP
    }

    fun setInterests(
        categories: List<CrumbsInterest>,
        onDeleteClickListener: OnDeleteClickListener?
    ) {
        removeAllViews()
        val padding = (8 * Resources.getSystem().displayMetrics.density).toInt()
        val bfColor = getBackgroundColor()
        categories.forEach {
            val cardView =
                MaterialCardView(context, null, R.attr.materialCardViewElevatedStyle).apply {
                    cardElevation = 0f
                    radius = resources.getDimension(R.dimen.crumbs_card_radius)
                    setCardBackgroundColor(bfColor)
                }
            val textView = TextView(context).apply {
                text = CrumbsFormatter.getInstance(context).getInterestDisplayName(it)
                setTextColor(getTextColor())
                this.setPadding(padding, padding / 2, padding, padding / 2)
                if (onDeleteClickListener != null && it.id >= 0) {
                    this.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.crumbs_remove, 0)
                    this.compoundDrawablePadding =
                        resources.getDimensionPixelSize(R.dimen.crumbs_card_padding)
                    cardView.setOnClickListener { _ ->
                        onDeleteClickListener.onDeleteClick(it)
                    }
                }
            }
            cardView.addView(textView)

            val layoutParams = MarginLayoutParams(
                LayoutParams.WRAP_CONTENT,
                LayoutParams.WRAP_CONTENT
            )
            layoutParams.setMargins(0, padding / 2, padding / 2, 0)
            addView(cardView, layoutParams)
        }
    }

    private fun getBackgroundColor(): Int {
        val typedValue = TypedValue()
        val a: TypedArray =
            context.obtainStyledAttributes(typedValue.data, intArrayOf(R.attr.colorAccent))
        val color = a.getColor(0, ContextCompat.getColor(context, R.color.crumbs_orange_light))
        a.recycle()
        val r = (color shr 16 and 0xff)
        val g = (color shr 8 and 0xff)
        val b = (color and 0xff)
        return Color.argb(48, r, g, b)
    }

    private fun getTextColor(): Int {
        val typedValue = TypedValue()
        val theme: Resources.Theme = context.theme
        theme.resolveAttribute(android.R.attr.textColorPrimary, typedValue, true)
        return context.obtainStyledAttributes(
            typedValue.data, intArrayOf(
                android.R.attr.textColorPrimary
            )
        ).let {
            val color = it.getColor(0, -1)
            it.recycle()
            color
        }
    }

    interface OnDeleteClickListener {
        fun onDeleteClick(interest: CrumbsInterest)
    }
}