package org.crumbs.ui.view

import android.content.Context
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatImageView
import androidx.core.graphics.drawable.DrawableCompat

class CrumbsImageView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : AppCompatImageView(context, attrs, defStyleAttr), CrumbsThemeHelper.ThemeListener, CrumbsUIContext.UIContextAware {

    private val themeHelper = CrumbsThemeHelper(context, false, this)

    override fun setCrumbsUIContext(crumbsUIContext: CrumbsUIContext){
        themeHelper.setCrumbsUIContext(crumbsUIContext)
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        themeHelper.onAttachedToWindow()
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        themeHelper.onDetachedFromWindow()
    }

    override fun onColorChange(color: Int) {
        DrawableCompat.setTint(this.drawable, color)
    }

}