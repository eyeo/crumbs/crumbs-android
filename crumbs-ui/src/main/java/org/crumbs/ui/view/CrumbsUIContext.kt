package org.crumbs.ui.view

import android.content.Context
import androidx.core.content.ContextCompat
import org.crumbs.CrumbsAndroid
import org.crumbs.CrumbsProvider
import org.crumbs.concurrent.Cancellable
import org.crumbs.ui.R
import org.crumbs.utils.UrlExtension.getUrlHost

class CrumbsUIContext : CrumbsProvider {

    private var listenJob: Cancellable? = null
    private var currentUrl: String = ""
    private var currentTabId: String = ""
    private var listeners = arrayListOf<UIContextListener>()
    private var currentStatus = ProtectionStatus.ENABLED

    init {
        updateStatus()
    }

    fun getProtectionStatus(): ProtectionStatus {
        return currentStatus
    }

    fun getCurrentUrl(): String {
        return currentUrl
    }

    fun setActiveTabUrl(tabId: String, url: String) {
        if (currentUrl != url || tabId != currentTabId) {
            for (i in listeners.size - 1 downTo 0) {
                listeners[i].onTabChanged(tabId, url)
            }
        }
        this.currentUrl = url
        this.currentTabId = tabId
        updateStatus()
    }

    fun registerListener(listener: UIContextListener) {
        listeners.add(listener)
        listener.onTabChanged(currentTabId, currentUrl)
        listener.onProtectionUpdated(currentStatus)
        updateListening()
    }

    fun unregisterListener(listener: UIContextListener): Boolean {
        val removed = listeners.remove(listener)
        if (removed) {
            updateListening()
        }
        return removed
    }

    private fun updateListening() {
        if (!CrumbsAndroid.isInitialized()) return
        if (listeners.isEmpty()) {
            listenJob?.cancel()
            listenJob = null
        } else if (listenJob == null) {
            listenJob = requirePrivacy.listenSettings {
                updateStatus()
            }
        }
    }

    private fun updateStatus() {
        if (!CrumbsAndroid.isInitialized()) return
        val lastStatus = currentStatus
        val isTemporaryAllowed =
            currentUrl.isNotEmpty() && requirePrivacy.getSettings().allowedDomainsThisSession.contains(
                currentUrl.getUrlHost()
            )
        val protectionEnabledForUrl = requirePrivacy.isProtectionEnabledForUrl(currentUrl)

        this.currentStatus = when {
            isTemporaryAllowed -> ProtectionStatus.PAUSED_UNTIL_NEXT_SESSION
            !protectionEnabledForUrl -> ProtectionStatus.PAUSED
            else -> ProtectionStatus.ENABLED
        }
        if (lastStatus != currentStatus) {
            for (i in listeners.size - 1 downTo 0) {
                listeners[i].onProtectionUpdated(currentStatus)
            }
        }
    }

    enum class ProtectionStatus {
        ENABLED,
        PAUSED,
        PAUSED_UNTIL_NEXT_SESSION;

        fun getColor(context: Context, lightTheme: Boolean): Int {

            val colorId = when (this) {
                ENABLED -> if (lightTheme) R.color.crumbs_green_light else R.color.crumbs_green
                PAUSED -> if (lightTheme) R.color.crumbs_orange_light else R.color.crumbs_orange
                PAUSED_UNTIL_NEXT_SESSION -> if (lightTheme) R.color.crumbs_yellow_light else R.color.crumbs_yellow
            }
            return ContextCompat.getColor(context, colorId)

        }

        fun getLabel(context: Context): String {
            return when (this) {
                ENABLED -> context.getString(R.string.crumbs_tracking_is_enabled)
                PAUSED -> context.getString(R.string.crumbs_tracking_is_disabled)
                PAUSED_UNTIL_NEXT_SESSION -> context.getString(R.string.crumbs_tracking_is_paused)
            }
        }
    }

    companion object {
        @JvmStatic
        val Default by lazy { CrumbsUIContext() }
    }

    interface UIContextListener {
        fun onTabChanged(tabId: String, documentUrl: String) {}
        fun onProtectionUpdated(status: ProtectionStatus) {}
    }

    interface UIContextAware {
        fun setCrumbsUIContext(crumbsUIContext: CrumbsUIContext)
    }

    internal interface UIContextAwareImpl : UIContextAware {

        var uiContext: CrumbsUIContext

        override fun setCrumbsUIContext(crumbsUIContext: CrumbsUIContext) {
            if (this is UIContextListener) {
                if (this.uiContext.unregisterListener(this)) {
                    crumbsUIContext.registerListener(this)
                }
            }
            this.uiContext = crumbsUIContext
            getContextAwareChildren().forEach {
                it.setCrumbsUIContext(crumbsUIContext)
            }
        }

        fun getContextAwareChildren(): List<UIContextAware> = emptyList()
    }
}