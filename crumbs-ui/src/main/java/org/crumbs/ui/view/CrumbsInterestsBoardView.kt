package org.crumbs.ui.view

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import org.crumbs.models.CrumbsInterest
import org.crumbs.ui.R
import org.crumbs.ui.databinding.CrumbsViewInterestsBinding
import org.crumbs.utils.UrlExtension.getUrlHost

class CrumbsInterestsBoardView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr) {

    private val binding: CrumbsViewInterestsBinding

    init {
        binding = CrumbsViewInterestsBinding.inflate(LayoutInflater.from(context), this, true)
    }

    fun setInterests(domain: String, categories: List<CrumbsInterest>) {
        val hasData = categories.isNotEmpty()
        binding.crumbsInterestsEmptyGroup.visibility = if (hasData) View.GONE else View.VISIBLE
        binding.crumbsInterestsEnableGroup.visibility = if (hasData) View.VISIBLE else View.GONE
        binding.crumbsInterestsSubtitle.text =
            context.getString(R.string.crumbs_interests_shared_with, domain.getUrlHost() ?: domain)
        binding.crumbsInterestsList.setInterests(categories, null)
        binding.crumbsInterestsEmptySubtitle.text =
            context.getString(R.string.crumbs_not_shared, domain.getUrlHost() ?: domain)
    }

    fun setMoreListener(listener: OnClickListener) =
        binding.crumbsInterestsMoreBtn.setOnClickListener(listener)
}