package org.crumbs.ui.view

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import androidx.constraintlayout.widget.ConstraintLayout
import org.crumbs.models.CrumbsStats
import org.crumbs.ui.databinding.CrumbsViewStatsBinding

class CrumbsStatsView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : ConstraintLayout(context, attrs, defStyleAttr), CrumbsUIContext.UIContextAwareImpl {

    private val binding: CrumbsViewStatsBinding

    init {
        binding = CrumbsViewStatsBinding.inflate(LayoutInflater.from(context), this, true)
    }

    override var uiContext: CrumbsUIContext = CrumbsUIContext.Default

    override fun getContextAwareChildren() = listOf<CrumbsUIContext.UIContextAware>(
        binding.crumbsStatsGlobalContainer,
        binding.crumbsStatsMessageContainer,
        binding.crumbsStatsTrackersContainer,
        binding.crumbsStatsTrackersCounterContainer,
        binding.crumbsStatsCookiesContainer,
        binding.crumbsStatsCookiesCounterContainer,
    )

    fun setStats(crumbsStats: CrumbsStats) {
        binding.crumbsStatsCookiesCounterTv.text = "${crumbsStats.tabStats.cookiesBlocked}"
        binding.crumbsStatsTrackersCounterTv.text = "${crumbsStats.tabStats.trackersBlocked}"
        val globalCount =
            crumbsStats.globalStats.trackersBlocked + crumbsStats.globalStats.cookiesBlocked
        binding.crumbsStatsGlobalCounterTv.text = "$globalCount"
    }

}