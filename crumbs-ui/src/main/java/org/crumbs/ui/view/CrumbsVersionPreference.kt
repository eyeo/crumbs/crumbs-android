package org.crumbs.ui.view

import android.content.Context
import android.util.AttributeSet
import android.widget.TextView
import androidx.preference.Preference
import androidx.preference.PreferenceViewHolder
import org.crumbs.CrumbsAndroid
import org.crumbs.ui.R

class CrumbsVersionPreference @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0,
    defStyleRes: Int = 0
) : Preference(context, attrs, defStyleAttr, defStyleRes) {

    init {
        layoutResource = R.layout.crumbs_preference_version
    }

    override fun onBindViewHolder(holder: PreferenceViewHolder) {
        super.onBindViewHolder(holder)
        val text = "${context.resources.getString(R.string.crumbs_powered_by)} v${CrumbsAndroid.get().version}"
        (holder.findViewById(R.id.crumbs_preference_version_tv) as TextView).text = text
    }
}