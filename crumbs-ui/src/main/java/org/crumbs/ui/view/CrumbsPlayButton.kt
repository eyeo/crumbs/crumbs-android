package org.crumbs.ui.view

import android.content.Context
import android.graphics.drawable.Animatable
import android.graphics.drawable.Drawable
import android.os.Handler
import android.os.Looper
import android.util.AttributeSet
import androidx.annotation.DrawableRes
import androidx.appcompat.widget.AppCompatImageButton
import androidx.core.content.ContextCompat
import org.crumbs.CrumbsProvider
import org.crumbs.ui.R

class CrumbsPlayButton @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : AppCompatImageButton(context, attrs, defStyleAttr), CrumbsProvider,
    CrumbsUIContext.UIContextListener, CrumbsUIContext.UIContextAwareImpl {

    private val maximumAnimationDuration by lazy {
        context.resources.getInteger(R.integer.crumbs_play_button_animation_duration).toLong()
    }

    private var currentMode: Mode = Mode.PLAYPAUSE
        set(value) {
            field = value
            setImageDrawable(field)
        }

    private val postHandler = Handler(Looper.getMainLooper())


    private val animationEndRunnable: Runnable = Runnable {
        val oppositeMode = currentMode.getOppositeMode()
        currentMode = oppositeMode
    }

    override var uiContext: CrumbsUIContext = CrumbsUIContext.Default

    init {
        currentMode = uiContext.getProtectionStatus().toMode()
        setOnClickListener {
            val url = uiContext.getCurrentUrl()
            privacy?.apply {
                val editor = editSettings()
                when (uiContext.getProtectionStatus()) {
                    CrumbsUIContext.ProtectionStatus.ENABLED -> {
                        // use allowDomainThisSession instead to resume tracking protection
                        // in the next session instead of persisting the pause
                        editor.allowDomain(url)
                    }
                    CrumbsUIContext.ProtectionStatus.PAUSED,
                    CrumbsUIContext.ProtectionStatus.PAUSED_UNTIL_NEXT_SESSION -> {
                        editor.controlDomain(url)
                    }
                }.apply()
            }
            interests?.apply {
                val editor = editSettings()
                when (uiContext.getProtectionStatus()) {
                    CrumbsUIContext.ProtectionStatus.ENABLED -> {
                        editor.disableDomain(url)
                    }
                    CrumbsUIContext.ProtectionStatus.PAUSED,
                    CrumbsUIContext.ProtectionStatus.PAUSED_UNTIL_NEXT_SESSION -> {
                        editor.enableDomain(url)
                    }
                }.apply()
            }
        }
        setOnLongClickListener {
            val url = uiContext.getCurrentUrl()
            privacy?.apply {
                editSettings().allowDomainThisSession(url).apply()
            }
            interests?.apply {
                editSettings().disableDomainThisSession(url).apply()
            }
            true
        }
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        uiContext.registerListener(this)
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        uiContext.unregisterListener(this)
    }

    override fun onProtectionUpdated(status: CrumbsUIContext.ProtectionStatus) {
        val newMode = status.toMode()
        if ((this.drawable.asAnimatable().isRunning && this.currentMode.getOppositeMode() != newMode) || (!this.drawable.asAnimatable().isRunning && this.currentMode != newMode)) {

            if (this.drawable.asAnimatable().isRunning) {
                postHandler.removeCallbacks(animationEndRunnable)
            }
            this.currentMode = newMode.getOppositeMode()
            this.drawable.asAnimatable().start()

            postHandler.postDelayed(animationEndRunnable, maximumAnimationDuration)
        }
    }

    private fun setImageDrawable(mode: Mode) {
        val animatedVector = ContextCompat.getDrawable(context, mode.drawableRes)
        this.setImageDrawable(animatedVector)
    }

    private fun Drawable.asAnimatable() = (this as Animatable)

    sealed class Mode(@DrawableRes val drawableRes: Int) {
        object PAUSEPLAY : Mode(R.drawable.crumbs_play_to_pause_animation)
        object PLAYPAUSE : Mode(R.drawable.crumbs_pause_to_play_animation)
        //object STOPPLAY : Mode(R.drawable.crumbs_play_to_stop_animation)
        //object PLAYSTOP : Mode(R.drawable.crumbs_stop_to_play_animation)
    }

    private val opposites = mapOf(
        Mode.PLAYPAUSE to Mode.PAUSEPLAY,
        Mode.PAUSEPLAY to Mode.PLAYPAUSE,
        //Mode.PLAYSTOP to Mode.STOPPLAY,
        //Mode.STOPPLAY to Mode.PLAYSTOP
    )

    private fun Mode.getOppositeMode() = opposites[this]!!

    private fun CrumbsUIContext.ProtectionStatus.toMode(): Mode {
        return when (this) {
            CrumbsUIContext.ProtectionStatus.ENABLED -> Mode.PLAYPAUSE
            CrumbsUIContext.ProtectionStatus.PAUSED -> Mode.PAUSEPLAY
            CrumbsUIContext.ProtectionStatus.PAUSED_UNTIL_NEXT_SESSION -> Mode.PAUSEPLAY
        }
    }
}