package org.crumbs.ui.view

import android.content.Context
import android.util.AttributeSet
import com.google.android.material.card.MaterialCardView
import org.crumbs.ui.R

class CrumbsCardView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null
) : MaterialCardView(context, attrs, R.attr.materialCardViewElevatedStyle),
    CrumbsThemeHelper.ThemeListener, CrumbsUIContext.UIContextAware {

    private val themeHelper: CrumbsThemeHelper

    init {
        val crumbsAttrs = context.theme.obtainStyledAttributes(
            attrs,
            R.styleable.CrumbsCardView,
            0, 0
        )

        radius = crumbsAttrs.getDimension(
            R.styleable.CrumbsCardView_crumbsRadius,
            context.resources.getDimension(R.dimen.crumbs_card_radius)
        )
        val lightTheme = crumbsAttrs.getBoolean(R.styleable.CrumbsCardView_crumbsLightTheme, false)
        cardElevation = 0f
        crumbsAttrs.recycle()
        themeHelper = CrumbsThemeHelper(context, lightTheme, this)
    }

    override fun setCrumbsUIContext(crumbsUIContext: CrumbsUIContext) {
        themeHelper.setCrumbsUIContext(crumbsUIContext)
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        themeHelper.onAttachedToWindow()
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        themeHelper.onDetachedFromWindow()
    }

    override fun onColorChange(color: Int) {
        setCardBackgroundColor(color)
    }

}