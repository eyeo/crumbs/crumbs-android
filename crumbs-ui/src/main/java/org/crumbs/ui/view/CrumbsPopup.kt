package org.crumbs.ui.view

import android.content.Context
import android.content.res.Resources
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.View
import android.view.ViewGroup
import android.widget.PopupWindow
import org.crumbs.CrumbsProvider
import org.crumbs.ui.CrumbsCoreExtension.ui
import org.crumbs.ui.R
import org.crumbs.ui.analytics.Event

class CrumbsPopup(context: Context) : PopupWindow(), CrumbsBoardView.NavigationListener,
    CrumbsProvider {

    private val crumbsBoardView: CrumbsBoardView

    init {
        isFocusable = true
        isOutsideTouchable = true
        setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        width = context.resources.getDimensionPixelSize(R.dimen.crumbs_popup_width)
        height = ViewGroup.LayoutParams.WRAP_CONTENT
        crumbsBoardView = CrumbsBoardView(context)
        contentView = crumbsBoardView
        animationStyle = R.style.CrumbsPopupAnimation
    }

    @JvmOverloads
    fun show(anchor: View, crumbsUIContext: CrumbsUIContext = CrumbsUIContext.Default) {
        val padding = (12 * Resources.getSystem().displayMetrics.density).toInt()
        crumbsBoardView.setCrumbsUIContext(crumbsUIContext)
        crumbsBoardView.setNavigationListener(this)
        showAsDropDown(anchor, -padding, -padding)
        update()
        crumbs?.ui()?.notifyUIEvent(Event.Popup.OPEN_SCREEN)
    }

    override fun onLinkOpen() {
        dismiss()
    }

}