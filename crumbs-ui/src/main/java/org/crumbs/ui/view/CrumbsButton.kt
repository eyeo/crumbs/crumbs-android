package org.crumbs.ui.view

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.ViewPropertyAnimator
import android.view.animation.AnticipateInterpolator
import android.view.animation.OvershootInterpolator
import android.widget.FrameLayout
import androidx.annotation.ColorInt
import androidx.core.graphics.drawable.DrawableCompat
import org.crumbs.CrumbsProvider
import org.crumbs.concurrent.Cancellable
import org.crumbs.ui.R
import org.crumbs.ui.databinding.CrumbsViewButtonBinding

class CrumbsButton @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr), CrumbsProvider, CrumbsUIContext.UIContextListener,
    CrumbsUIContext.UIContextAwareImpl {

    private val binding: CrumbsViewButtonBinding
    private var documentUrl: String? = null
    private var currentTabId: String? = null
    private var listeningJob: Cancellable? = null

    init {
        binding = CrumbsViewButtonBinding.inflate(LayoutInflater.from(context), this, true)
        setOnClickListener {
            openDialog()
        }
    }

    fun setColor(@ColorInt color: Int) {
        DrawableCompat.setTint(binding.crumbsButtonIv.drawable, color)
    }

    override var uiContext: CrumbsUIContext = CrumbsUIContext.Default

    override fun getContextAwareChildren(): List<CrumbsUIContext.UIContextAware> = listOf(
        binding.crumbsButtonIvFg,
        binding.crumbsButtonCountCvColor
    )

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        uiContext.registerListener(this)
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        listeningJob?.cancel()
        uiContext.unregisterListener(this)
    }

    override fun onTabChanged(tabId: String, documentUrl: String) {
        this.currentTabId = tabId
        this.documentUrl = documentUrl
        setCount(0)
        listeningJob?.cancel()
        listeningJob = stats?.listen(tabId) {
            setCount(it.tabStats.cookiesBlocked + it.tabStats.trackersBlocked)
        }
    }

    private fun openDialog() {
        currentTabId?.let {
            CrumbsPopup(context).show(this, uiContext)
        }
    }

    private fun setCount(count: Int) {
        val newScale = if (count == 0) 0f else 1.0f

        val counterContainer = binding.crumbsButtonCountCvColor
        val viewTag = counterContainer.getTag(R.id.crumbs_button_tv) as Float? ?: 0f
        // we don't start animation if it's already targeting proper value
        if (viewTag != newScale) {
            counterContainer.setTag(R.id.crumbs_button_tv, newScale)
            val viewPropertyAnimator: ViewPropertyAnimator =
                counterContainer.animate().scaleX(newScale).scaleY(newScale)
                    .setDuration(200)
            if (newScale >= viewTag) {
                viewPropertyAnimator.interpolator = OvershootInterpolator()
            } else {
                viewPropertyAnimator.interpolator = AnticipateInterpolator()
            }
            viewPropertyAnimator.start()
        }

        if (count == 0 && binding.crumbsButtonTv.getTag(R.id.crumbs_button_tv) == true) {
            counterContainer.visibility = View.VISIBLE
            counterContainer.getTag(R.id.crumbs_button_tv)
            counterContainer.animate().scaleX(1f).scaleY(1f).start()
        }

        if (count != 0) {
            binding.crumbsButtonTv.text = count.toString()
        }
    }

    override fun setEnabled(enabled: Boolean) {
        super.setEnabled(enabled)
        alpha = if (enabled) 1.0f else 0.2f
    }


}