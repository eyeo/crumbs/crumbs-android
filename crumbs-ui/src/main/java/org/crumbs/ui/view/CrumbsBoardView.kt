package org.crumbs.ui.view

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import androidx.fragment.app.FragmentActivity
import org.crumbs.CrumbsLogger
import org.crumbs.CrumbsProvider
import org.crumbs.concurrent.Cancellable
import org.crumbs.ui.CrumbsCoreExtension.ui
import org.crumbs.ui.CrumbsSettingsActivity
import org.crumbs.ui.R
import org.crumbs.ui.analytics.Event
import org.crumbs.ui.databinding.CrumbsViewBoardBinding
import org.crumbs.ui.relay.CrumbsEmailRelayDialogFragment
import org.crumbs.ui.relay.CrumbsEmailRelaySetupDialogFragment
import org.crumbs.ui.utils.showOnce
import org.crumbs.utils.ContextExtension.getActivity

class CrumbsBoardView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : FrameLayout(context, attrs, defStyleAttr), CrumbsProvider, CrumbsUIContext.UIContextListener,
    CrumbsUIContext.UIContextAwareImpl {

    private val binding: CrumbsViewBoardBinding
    private var navListener: NavigationListener? = null
    private var interestsJob: Cancellable? = null
    private var statsJob: Cancellable? = null
    override var uiContext: CrumbsUIContext = CrumbsUIContext.Default

    init {
        binding = CrumbsViewBoardBinding.inflate(LayoutInflater.from(context), this, true)
        binding.crumbsDialogIssue.setOnClickListener {
            requireCrumbs.ui().openLink(
                context,
                requireCrumbs.ui().createFeedbackFormUrl(context, uiContext.getCurrentUrl()),
            )
            navListener?.onLinkOpen()
            requireCrumbs.ui().notifyUIEvent(Event.Popup.PRESS_FEEDBACK)
        }
        binding.crumbsDialogSettings.setOnClickListener {
            context.startActivity(
                CrumbsSettingsActivity.createIntent(
                    context,
                    CrumbsSettingsActivity.MAIN_FEATURE_ID,
                    context.getActivity()?.intent
                )
            )
            navListener?.onLinkOpen()
            requireCrumbs.ui().notifyUIEvent(Event.Popup.PRESS_SETTINGS)
        }
        binding.crumbsBoardInterest.setMoreListener {
            context.startActivity(
                CrumbsSettingsActivity.createIntent(
                    context,
                    CrumbsSettingsActivity.INTERESTS_FEATURE_ID,
                    context.getActivity()?.intent
                )
            )
            navListener?.onLinkOpen()
            requireCrumbs.ui().notifyUIEvent(Event.Popup.PRESS_MORE_INTERESTS)
        }
    }

    override fun getContextAwareChildren() = listOf<CrumbsUIContext.UIContextAware>(
        binding.crumbsBoardStats,
        binding.crumbsBoardBottom,
        binding.crumbsBoardPlayBtn,
        binding.crumbsBoardIv,
        binding.crumbsBoardOnOffContainer,
        binding.crumbsBoardOnOffCard
    )

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        uiContext.registerListener(this)
    }

    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        uiContext.unregisterListener(this)
        this.interestsJob?.cancel()
        this.statsJob?.cancel()
    }

    override fun onTabChanged(tabId: String, documentUrl: String) {
        binding.crumbsDialogEmail.apply {
            visibility = if (emailRelay?.getSettings()?.enabled == true) VISIBLE else GONE
            setOnClickListener {
                (context.getActivity() as? FragmentActivity)?.supportFragmentManager?.let {
                    if (requireEmailRelay.isSignedIn()) {
                        CrumbsEmailRelayDialogFragment.newInstance(documentUrl)
                            .showOnce(it, "crumbs_email_relay")
                    } else {
                        CrumbsEmailRelaySetupDialogFragment.newInstance()
                            .showOnce(it, "crumbs_email_relay_setup")
                    }
                }
                navListener?.onLinkOpen()
                requireCrumbs.ui().notifyUIEvent(Event.Popup.PRESS_EMAIL_RELAY)
            }
        }

        this.interestsJob?.cancel()
        this.interestsJob = interests?.listenSharedInterests(tabId, documentUrl) {
            binding.crumbsBoardInterest.setInterests(documentUrl, it)
        }

        this.statsJob?.cancel()
        this.statsJob = stats?.listen(tabId) {
            binding.crumbsBoardStats.setStats(it)
        }
    }

    override fun onProtectionUpdated(status: CrumbsUIContext.ProtectionStatus) {
        val text = status.getLabel(context)
        binding.crumbsStatsOnOffTitle.text =
            context.getString(R.string.crumbs_tracking_protection_is, text)
    }

    fun setNavigationListener(listener: NavigationListener) {
        this.navListener = listener
    }

    interface NavigationListener {
        fun onLinkOpen()
    }

    companion object {
        private val logger = CrumbsLogger("CrumbsBoardView")
    }

}