package org.crumbs.ui.view

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.SwitchCompat
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*
import org.crumbs.CrumbsAndroid
import org.crumbs.CrumbsProvider
import org.crumbs.concurrent.Cancellable
import org.crumbs.models.ProfileInterest
import org.crumbs.ui.CrumbsCoreExtension.ui
import org.crumbs.ui.R
import org.crumbs.ui.analytics.Event
import org.crumbs.ui.databinding.CrumbsCellCategoryBinding
import org.crumbs.ui.databinding.CrumbsCellProfileInterestsBinding
import org.crumbs.ui.databinding.CrumbsCellShowMoreBinding
import org.crumbs.ui.utils.CrumbsFormatter
import org.crumbs.ui.view.InterestStateAdapter.Companion.OTHER_INTERESTS
import org.crumbs.ui.view.InterestStateAdapter.Companion.SHAREABLE_INTERESTS
import java.util.*

class CrumbsProfileRecyclerView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : RecyclerView(context, attrs, defStyleAttr), CrumbsProvider {

    private val itemTouchHelper: ItemTouchHelper
    private var listenJob: Cancellable? = null

    private val crumbsAdapter: InterestStateAdapter
        get() = (adapter as InterestStateAdapter)

    init {
        layoutManager = LinearLayoutManager(context)
        adapter = InterestStateAdapter(CrumbsFormatter.getInstance(context))
        itemTouchHelper = ItemTouchHelper(object : ItemTouchHelper.Callback() {
            override fun getMovementFlags(recyclerView: RecyclerView, viewHolder: ViewHolder): Int {
                if (viewHolder is InterestStateHolder) {
                    if ((viewHolder.currentItem?.id ?: -1) >= 0) {
                        return makeMovementFlags(0, ItemTouchHelper.START or ItemTouchHelper.END)
                    }
                }
                return 0
            }

            override fun onMove(
                recyclerView: RecyclerView,
                viewHolder: ViewHolder,
                target: ViewHolder
            ): Boolean {
                return false
            }

            override fun onSwiped(viewHolder: ViewHolder, direction: Int) {
                val item = crumbsAdapter.getItemAt(viewHolder.adapterPosition)
                showResetInterestDialog(viewHolder, item)
                requireCrumbs.ui().notifyUIEvent(Event.InterestsSettings.SWIPE_INTEREST, item)
            }

        })
        itemTouchHelper.attachToRecyclerView(this)
    }


    private fun showResetInterestDialog(viewHolder: ViewHolder, item: ProfileInterest) {
        MaterialAlertDialogBuilder(context)
            .setMessage(R.string.crumbs_forget_interest)
            .setPositiveButton(R.string.crumbs_reset) { _, _ ->
                interests?.resetInterest(item.id)
                requireCrumbs.ui().notifyUIEvent(Event.InterestsSettings.FORGET_INTEREST, interests)
            }.setNegativeButton(android.R.string.cancel) { _, _ ->
                crumbsAdapter.notifyItemChanged(viewHolder.adapterPosition)
            }
            .show()
    }

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        if (CrumbsAndroid.isInitialized()) {
            listenJob = interests?.listenProfileInterests(true) {
                crumbsAdapter.submitList(it)
            }
        }
    }


    override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        listenJob?.cancel()
    }

    fun search(newText: String) {
        crumbsAdapter.searchText = newText
    }
}

class InterestStateAdapter(private val crumbsFormatter: CrumbsFormatter) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var submitListJob: Job? = null
    private var searchJob: Job? = null
    private var filterJob: Job? = null

    private var searchData = emptyList<ProfileInterest>()
    private var listData = emptyList<Any>()
    private var sortedData = emptyList<ProfileInterest>()

    var showAll: Boolean = false
        set(value) {
            field = value
            refreshListData()
        }

    var searchText: String = ""
        set(value) {
            field = value.lowercase(Locale.ROOT)
            refreshSearchData()
        }

    private val data: List<Any>
        get() = searchData.ifEmpty {
            listData
        }

    init {
        setHasStableIds(true)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return when (viewType) {
            CategoryHolder.ID -> {
                CategoryHolder(CrumbsCellCategoryBinding.inflate(inflater, parent, false))
            }
            InterestStateHolder.ID -> {
                InterestStateHolder(
                    CrumbsCellProfileInterestsBinding.inflate(
                        inflater,
                        parent,
                        false
                    )
                )
            }
            else -> {
                val binding = CrumbsCellShowMoreBinding.inflate(inflater, parent, false)
                binding.crumbsCellShowMoreBtn.setOnClickListener {
                    this.showAll = true
                }
                ButtonHolder(binding.root)
            }
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is InterestStateHolder) {
            holder.setItem(getItemAt(position))
        } else if (holder is CategoryHolder) {
            holder.setTitle(data[position] as String)
        }
    }

    override fun getItemCount(): Int {
        return data.size
    }

    override fun getItemId(position: Int): Long {
        val data = data[position]
        return if (data is ProfileInterest) {
            data.id
        } else {
            (data as String).hashCode()
        }.toLong()
    }

    override fun getItemViewType(position: Int): Int {
        return when {
            data[position] is ProfileInterest -> InterestStateHolder.ID
            data[position] == SHOW_MORE -> ButtonHolder.ID
            else -> CategoryHolder.ID
        }
    }

    fun getItemAt(position: Int): ProfileInterest {
        return data[position] as ProfileInterest
    }

    fun submitList(items: List<ProfileInterest>) {
        submitListJob?.cancel()
        submitListJob = MainScope().launch {
            sortedData = withContext(Dispatchers.Default) {
                items.sortedWith { a, b ->
                    when {
                        a.enabled == b.enabled -> when {
                            a.shareable == b.shareable -> b.score.compareTo(a.score)
                            a.shareable -> -1
                            else -> 1
                        }
                        a.enabled -> -1
                        else -> 1
                    }
                }
            }
            refreshListData()
            refreshSearchData()
        }
    }

    private fun refreshSearchData() {
        searchJob?.cancel()
        if (searchText.isEmpty()) {
            this.searchData = emptyList()
            notifyDataSetChanged()
        } else {
            searchJob = MainScope().launch {
                searchData = sortedData.asFlow()
                    .filter {
                        val displayName = crumbsFormatter.getInterestDisplayName(it.interest)
                        displayName.lowercase(Locale.ROOT).contains(searchText)
                    }
                    .take(20)
                    .cancellable()
                    .flowOn(Dispatchers.Default)
                    .toList()
                    .sortedWith { v1, v2 ->
                        val displayName1 = crumbsFormatter.getInterestDisplayName(v1.interest)
                        val displayName2 = crumbsFormatter.getInterestDisplayName(v2.interest)
                        val startWith = displayName1.indexOf(searchText)
                            .compareTo(displayName2.indexOf(searchText))
                        if (startWith != 0) {
                            startWith
                        } else {
                            displayName1.compareTo(displayName2)
                        }
                    }
                notifyDataSetChanged()
            }

        }
    }

    private fun refreshListData() {
        filterJob = MainScope().launch {
            val result = arrayListOf<Any>()

            val shareableInterests =
                sortedData.asFlow().filter { it.enabled && it.shareable && it.hasEnoughData }
                    .cancellable()
                    .toList()
            if (shareableInterests.isNotEmpty()) {
                result.add(SHAREABLE_INTERESTS)
                result.addAll(shareableInterests)
            }
            val moreInterests = arrayListOf<Any>()


            val otherInterests =
                sortedData.filter { it.enabled && !it.shareable && it.hasEnoughData }
            if (otherInterests.isNotEmpty()) {
                moreInterests.add(OTHER_INTERESTS)
                moreInterests.addAll(otherInterests)
            }
            val disabledInterests = sortedData.filter { !it.enabled }
            if (disabledInterests.isNotEmpty()) {
                moreInterests.add(DISABLED_INTERESTS)
                moreInterests.addAll(disabledInterests)
            }

            if (showAll) {
                result.addAll(moreInterests)
            } else if (!showAll && moreInterests.size > 0) {
                result.add(SHOW_MORE)
            }
            listData = result
            notifyDataSetChanged()
        }
    }

    companion object {
        internal const val SHAREABLE_INTERESTS = "SHAREABLE_INTERESTS"
        internal const val OTHER_INTERESTS = "OTHER_INTERESTS"
        internal const val DISABLED_INTERESTS = "DISABLED_INTERESTS"
        internal const val SHOW_MORE = "SHOW_MORE"
    }

}

private class CategoryHolder(private val binding: CrumbsCellCategoryBinding) :
    RecyclerView.ViewHolder(binding.root) {
    fun setTitle(title: String) {
        val resId = when (title) {
            SHAREABLE_INTERESTS -> R.string.crumbs_shareable_interests
            OTHER_INTERESTS -> R.string.crumbs_other_interests
            else -> R.string.crumbs_disabled_interests
        }
        val descId = when (title) {
            SHAREABLE_INTERESTS -> 0
            OTHER_INTERESTS -> R.string.crumbs_other_interests_desc
            else -> R.string.crumbs_disabled_interests_desc
        }

        binding.crumbsCellCategoryTv.setText(resId)
        binding.crumbsCellCategoryDescTv.apply {
            if (descId != 0) setText(descId)
            visibility = if (descId != 0) View.VISIBLE else View.GONE
        }
    }

    companion object {
        const val ID = 1
    }
}

private class ButtonHolder(view: View) : RecyclerView.ViewHolder(view) {
    companion object {
        const val ID = 2
    }
}

private class InterestStateHolder(binding: CrumbsCellProfileInterestsBinding) :
    RecyclerView.ViewHolder(binding.root),
    CrumbsProvider {

    var currentItem: ProfileInterest? = null
    private val switch: SwitchCompat = binding.crumbsCellProfileInterestSwitch

    fun setItem(item: ProfileInterest) {
        this.currentItem = item
        switch.alpha = when {
            !item.enabled -> 0.8f
            else -> 1f
        }
        // avoid animation restarting if not needed
        if (item.enabled != switch.isChecked) {
            switch.setOnCheckedChangeListener(null)
            switch.isChecked = item.enabled
        }

        switch.text =
            CrumbsFormatter.getInstance(switch.context).getInterestDisplayName(item.interest)
        switch.setOnCheckedChangeListener { _, isChecked ->
            interests?.setInterestState(item.id, isChecked)
            requireCrumbs.ui().notifyUIEvent(
                Event.InterestsSettings.TOGGLE_INTEREST_STATE,
                item.copy(enabled = isChecked)
            )
        }
    }

    companion object {
        const val ID = 3
    }

}