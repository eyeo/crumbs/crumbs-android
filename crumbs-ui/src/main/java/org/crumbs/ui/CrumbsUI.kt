package org.crumbs.ui

import android.content.Context
import androidx.fragment.app.FragmentManager
import io.ktor.http.*
import org.crumbs.CrumbsAndroid
import org.crumbs.CrumbsCore
import org.crumbs.ui.analytics.Event
import org.crumbs.ui.relay.CrumbsEmailRelayDialogFragment
import org.crumbs.ui.relay.CrumbsEmailRelaySetupDialogFragment
import org.crumbs.ui.tnc.CrumbsTnCDialogFragment
import org.crumbs.ui.utils.TermsAndConditionsState
import org.crumbs.ui.utils.UIPreferences
import org.crumbs.ui.utils.showOnce
import org.crumbs.ui.view.CrumbsUIContext
import org.crumbs.utils.ContextExtension.getVersionCode
import org.crumbs.utils.ContextExtension.getVersionName

class CrumbsUI {

    private val eventListeners = arrayListOf<EventListener>()

    val preferences = UIPreferences(CrumbsCore.storageProvider())

    fun defaultContext() = CrumbsUIContext.Default

    fun addEventListener(listener: EventListener) {
        eventListeners.add(listener)
    }

    fun removeEventListener(listener: EventListener): Boolean {
        return eventListeners.remove(listener)
    }

    internal fun notifyUIEvent(id: String, value: Any? = null) {
        val event = Event(id, value)
        eventListeners.iterator().run {
            while (hasNext()) {
                next().onEvent(event)
            }
        }
    }

    fun createFeedbackFormUrl(context: Context, url: String?) =
        URLBuilder("https://crumbs.org/en/report-issue/").also {
            it.parameters.apply {
                append(
                    "version",
                    "${context.packageName}: ${context.getVersionName()} - ${context.getVersionCode()} / Crumbs: ${CrumbsAndroid.get().version}"
                )
                url?.let {
                    append("url", url)
                }
            }
        }.buildString()

    fun openLink(context: Context, url: String, forceInsideApp: Boolean = true) =
        preferences.linkOpener?.openLink(
            context,
            url,
            if (forceInsideApp) context.packageName else null
        )

    /**
     * Sets the Crumbs enable state if terms and conditions were accepted else shows the TnC dialog
     * @param fragmentManager   used to show the TnC dialog
     * @param enable            the new state
     * @return true if the toggle was successful and false otherwise and the TnC dialog was shown
     */
    fun enableCrumbsWithTnCDialog(
        fragmentManager: FragmentManager,
        enable: Boolean
    ): Boolean {
        return if (preferences.getTermsAndConditionsState() == TermsAndConditionsState.ACCEPTED) {
            CrumbsAndroid.get().privacy().editSettings().enable(enable).apply()
            CrumbsAndroid.get().interests().editSettings().enable(enable).apply()
            true
        } else {
            val dialogToShow =
                preferences.termsAndConditionsDialog
                    ?: CrumbsTnCDialogFragment.newInstance(true)
            dialogToShow.showOnce(fragmentManager, CrumbsTnCDialogFragment.TAG)
            false
        }
    }

    /**
     * According to the Email Relay signed in state,
     * launches [CrumbsEmailRelayDialogFragment] or [CrumbsEmailRelaySetupDialogFragment].
     *
     * @param fragmentManager   used to show the dialog
     * @param url               current URL
     * @param onEmailClick      action for [CrumbsEmailRelayDialogFragment]
     */
    fun launchEmailRelayDialog(
        fragmentManager: FragmentManager,
        url: String,
        onEmailClick: ((String) -> Unit)? = null
    ) {
        val emailRelay = CrumbsAndroid.get().emailRelay()
        if (emailRelay.getSettings().enabled && emailRelay.isSignedIn()) {
            val fragment = CrumbsEmailRelayDialogFragment.newInstance(url)
            onEmailClick?.let { fragment.setEmailClickListener(it) }
            fragment.showOnce(fragmentManager, CrumbsEmailRelayDialogFragment.TAG)
        } else {
            CrumbsEmailRelaySetupDialogFragment.newInstance()
                .showOnce(fragmentManager, CrumbsEmailRelaySetupDialogFragment.TAG)
        }
    }

    interface EventListener {
        fun onEvent(event: Event<*>)
    }

    interface LinkOpener {
        fun openLink(context: Context, url: String, targetPackage: String?)
    }

    companion object {
        private var instance: CrumbsUI? = null

        @JvmStatic
        fun get(): CrumbsUI {
            if (instance == null) {
                instance = CrumbsUI()
            }
            return instance!!
        }
    }
}

object CrumbsCoreExtension {
    fun CrumbsCore.ui() = CrumbsUI.get()
}
