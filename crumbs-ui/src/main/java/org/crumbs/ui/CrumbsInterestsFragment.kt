package org.crumbs.ui

import android.app.Activity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.widget.SearchView
import androidx.fragment.app.Fragment
import org.crumbs.CrumbsAndroid
import org.crumbs.CrumbsProvider
import org.crumbs.ui.CrumbsCoreExtension.ui
import org.crumbs.ui.analytics.Event
import org.crumbs.ui.databinding.CrumbsFragmentInterestsBinding

class CrumbsInterestsFragment : Fragment(), CrumbsProvider {

    private lateinit var binding: CrumbsFragmentInterestsBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (!CrumbsAndroid.isInitialized()) return null

        if (savedInstanceState == null) {
            requireCrumbs.ui().notifyUIEvent(Event.InterestsSettings.OPEN_SCREEN)
        }

        binding = CrumbsFragmentInterestsBinding.inflate(inflater, container, false)

        return binding.root.apply {
            val crumbsRv = binding.crumbsInterestsStateRv
            crumbsRv.isNestedScrollingEnabled = false
            val searchView = binding.crumbsInterestsStateSv
            searchView.setOnQueryTextFocusChangeListener { _, hasFocus ->
                if (hasFocus) {
                    requireCrumbs.ui().notifyUIEvent(Event.InterestsSettings.FOCUS_SEARCH)
                    moveToSearchBar()
                }
            }
            searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String?): Boolean {
                    val inputMethodManager =
                        context?.getSystemService(Activity.INPUT_METHOD_SERVICE) as? InputMethodManager
                    inputMethodManager?.hideSoftInputFromWindow(searchView.windowToken, 0)
                    return true
                }

                override fun onQueryTextChange(newText: String): Boolean {
                    crumbsRv.search(newText)
                    moveToSearchBar()
                    return true
                }
            })
        }
    }

    private fun moveToSearchBar() {
        val scrollView = binding.crumbsInterestsScrollview
        scrollView.post {
            scrollView.smoothScrollTo(0, ((binding.crumbsInterestsStateSv.parent) as View).top)
        }
    }

    companion object {
        fun newInstance(): CrumbsInterestsFragment = CrumbsInterestsFragment()
    }
}