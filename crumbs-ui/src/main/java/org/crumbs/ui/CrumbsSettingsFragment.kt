package org.crumbs.ui

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import androidx.preference.*
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import org.crumbs.CrumbsAndroid
import org.crumbs.CrumbsProvider
import org.crumbs.models.FilterLevel
import org.crumbs.ui.CrumbsCoreExtension.ui
import org.crumbs.ui.CrumbsSettingsActivity.Companion.ARGS_DISABLED_FEATURES
import org.crumbs.ui.CrumbsSettingsActivity.Companion.ARGS_HIGHLIGHTED_FEATURE
import org.crumbs.ui.CrumbsSettingsActivity.Companion.EMAIL_RELAY_FEATURE_ID
import org.crumbs.ui.CrumbsSettingsActivity.Companion.INTERESTS_FEATURE_ID
import org.crumbs.ui.CrumbsSettingsActivity.Companion.MAIN_FEATURE_ID
import org.crumbs.ui.analytics.Event

class CrumbsSettingsFragment : PreferenceFragmentCompat(), CrumbsProvider {

    private lateinit var showAdvancedFeatures: CheckBoxPreference
    private lateinit var cookiesCategory: PreferenceCategory
    private lateinit var privacyCategory: PreferenceCategory
    private lateinit var enableTrackingSwitch: SwitchPreferenceCompat
    private lateinit var thirdPartyCookiesSwitch: SwitchPreferenceCompat
    private lateinit var hideCookiesPopupSwitch: SwitchPreferenceCompat
    private lateinit var cookiesDropDown: ListPreference
    private lateinit var hideReferrerSwitch: SwitchPreferenceCompat
    private lateinit var dntSwitch: SwitchPreferenceCompat
    private lateinit var cnameSwitch: SwitchPreferenceCompat
    private lateinit var proxySwitch: SwitchPreferenceCompat
    private lateinit var gpcSwitch: SwitchPreferenceCompat
    private lateinit var removeMarketingParamsSwitch: SwitchPreferenceCompat
    private lateinit var blockHyperlinkAuditingSwitch: SwitchPreferenceCompat
    private lateinit var fingerprintShieldSwitch: SwitchPreferenceCompat
    private lateinit var blockSocialMediaIconsSwitch: SwitchPreferenceCompat
    private lateinit var deAMPSwitch: SwitchPreferenceCompat

    private lateinit var interestsPref: Preference
    private lateinit var relayPref: Preference

    private lateinit var disabledFeatures: ArrayList<Int>
    private var highlightPref: Preference? = null

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        if (!CrumbsAndroid.isInitialized()) return

        setPreferencesFromResource(R.xml.crumbs_preferences, rootKey)

        setupPrivacyFeatures()
        interestsPref = findPreference(getString(R.string.crumbs_key_interests))!!
        relayPref = findPreference(getString(R.string.crumbs_key_email_relay))!!

        if (savedInstanceState == null) {
            requireCrumbs.ui().notifyUIEvent(Event.PrivacySettings.OPEN_SCREEN)
        }
    }

    private fun setupPrivacyFeatures() {
        cookiesCategory = findPreference(getString(R.string.crumbs_key_cookies_section))!!
        privacyCategory = findPreference(getString(R.string.crumbs_key_privacy_section))!!

        enableTrackingSwitch = findPreference(getString(R.string.crumbs_key_trackers_protection))!!
        enableTrackingSwitch.setOnPreferenceChangeListener { _, newValue ->
            val enabled = newValue == true
            return@setOnPreferenceChangeListener requireCrumbs.ui()
                .enableCrumbsWithTnCDialog(
                    parentFragmentManager,
                    enabled
                )
        }

        thirdPartyCookiesSwitch =
            findPreference(getString(R.string.crumbs_key_block_third_party_cookies))!!
        thirdPartyCookiesSwitch.setOnPreferenceChangeListener { _, newValue ->
            val blockThirdPartyCookies = newValue as Boolean
            requirePrivacy.editSettings().blockThirdPartyCookies(blockThirdPartyCookies).apply()
            requireCrumbs.ui().notifyUIEvent(
                Event.PrivacySettings.TOGGLE_BLOCK_THIRD_PARTY_COOKIES, blockThirdPartyCookies
            )
            true
        }
        cookiesDropDown =
            findPreference(getString(R.string.crumbs_key_block_third_party_cookies_dropdown))!!
        cookiesDropDown.entryValues =
            resources.getStringArray(R.array.crumbs_block_third_party_cookies_array)
        cookiesDropDown.entries =
            resources.getStringArray(R.array.crumbs_block_third_party_cookies_array)
        cookiesDropDown.setOnPreferenceChangeListener { _, newValue ->
            val index = cookiesDropDown.findIndexOfValue(newValue as String)
            val cookiesFilterLevel = FilterLevel.values()[index]
            requirePrivacy.editSettings().cookiesFilterLevel(cookiesFilterLevel).apply()
            requireCrumbs.ui().notifyUIEvent(
                Event.PrivacySettings.SELECT_COOKIES_FILTER_LEVEL, cookiesFilterLevel
            )
            true
        }
        hideCookiesPopupSwitch = findPreference(getString(R.string.crumbs_key_hide_cookie_popups))!!
        hideCookiesPopupSwitch.setOnPreferenceChangeListener { _, newValue ->
            val enable = newValue as Boolean
            if (enable) {
                MaterialAlertDialogBuilder(requireContext()).setMessage(R.string.crumbs_hide_cookie_popups_dialog_message)
                    .setPositiveButton(R.string.crumbs_hide_cookie_popups_dialog_validate) { _, _ ->
                        requirePrivacy.editSettings().hideCookieConsentPopups(true).apply()
                        requireCrumbs.ui().notifyUIEvent(
                            Event.PrivacySettings.TOGGLE_HIDE_COOKIE_CONSENT_POPUPS, true
                        )
                    }.setNegativeButton(android.R.string.cancel) { _, _ -> }.show()
                false
            } else {
                requirePrivacy.editSettings().hideCookieConsentPopups(false).apply()
                requireCrumbs.ui().notifyUIEvent(
                    Event.PrivacySettings.TOGGLE_HIDE_COOKIE_CONSENT_POPUPS, false
                )
                true
            }
        }
        hideReferrerSwitch = findPreference(getString(R.string.crumbs_key_hide_referrer_header))!!
        hideReferrerSwitch.setOnPreferenceChangeListener { _, newValue ->
            val hideReferrerHeader = newValue as Boolean
            requirePrivacy.editSettings().hideReferrerHeader(hideReferrerHeader).apply()
            requireCrumbs.ui().notifyUIEvent(
                Event.PrivacySettings.TOGGLE_HIDE_REFERRER_HEADER, hideReferrerHeader
            )

            true
        }
        cnameSwitch = findPreference(getString(R.string.crumbs_key_cname_cloaking_protection))!!
        cnameSwitch.setOnPreferenceChangeListener { _, newValue ->
            val cnameProtection = newValue as Boolean
            requirePrivacy.editSettings().enableCNameCloakingProtection(cnameProtection).apply()
            requireCrumbs.ui().notifyUIEvent(
                Event.PrivacySettings.TOGGLE_CNAME_CLOAKING_PROTECTION, cnameProtection
            )
            true
        }
        dntSwitch = findPreference(getString(R.string.crumbs_key_send_no_track_signal))!!
        dntSwitch.setOnPreferenceChangeListener { _, newValue ->
            val enableDoNotTrack = newValue as Boolean
            requirePrivacy.editSettings().enableDoNotTrack(enableDoNotTrack).apply()
            requireCrumbs.ui()
                .notifyUIEvent(Event.PrivacySettings.TOGGLE_DO_NOT_TRACK, enableDoNotTrack)
            true
        }
        proxySwitch = findPreference(getString(R.string.crumbs_key_proxy_advertising_requests))!!
        proxySwitch.setOnPreferenceChangeListener { _, newValue ->
            val proxyAdvertisingRequests = newValue as Boolean
            requirePrivacy.editSettings().proxyAdvertisingRequests(proxyAdvertisingRequests).apply()
            requireCrumbs.ui().notifyUIEvent(
                Event.PrivacySettings.TOGGLE_PROXY_ADVERTISING_REQUESTS, proxyAdvertisingRequests
            )
            true
        }
        gpcSwitch = findPreference(getString(R.string.crumbs_key_gpc))!!
        gpcSwitch.setOnPreferenceChangeListener { _, newValue ->
            val enableGPC = newValue as Boolean
            requirePrivacy.editSettings().enableGPC(enableGPC).apply()
            requireCrumbs.ui().notifyUIEvent(Event.PrivacySettings.TOGGLE_ENABLE_GPC, enableGPC)
            true
        }
        removeMarketingParamsSwitch =
            findPreference(getString(R.string.crumbs_key_remove_marketing_tracking_parameters))!!
        removeMarketingParamsSwitch.setOnPreferenceChangeListener { _, newValue ->
            val removeMarketingTrackingParameters = newValue as Boolean
            requirePrivacy.editSettings().removeMarketingTrackingParameters(
                removeMarketingTrackingParameters
            ).apply()
            requireCrumbs.ui().notifyUIEvent(
                Event.PrivacySettings.TOGGLE_REMOVE_MARKETING_TRACKING_PARAMETERS,
                removeMarketingTrackingParameters
            )
            true
        }
        blockHyperlinkAuditingSwitch =
            findPreference(getString(R.string.crumbs_key_block_hyperlink_auditing))!!
        blockHyperlinkAuditingSwitch.setOnPreferenceChangeListener { _, newValue ->
            val blockHyperlinkAuditing = newValue as Boolean
            requirePrivacy.editSettings().blockHyperlinkAuditing(
                blockHyperlinkAuditing
            ).apply()
            requireCrumbs.ui().notifyUIEvent(
                Event.PrivacySettings.TOGGLE_BLOCK_HYPERLINK_AUDITING, blockHyperlinkAuditing
            )
            true
        }
        fingerprintShieldSwitch =
            findPreference(getString(R.string.crumbs_key_fingerprint_shield))!!
        fingerprintShieldSwitch.setOnPreferenceChangeListener { _, newValue ->
            val fingerprintShield = newValue as Boolean
            requirePrivacy.editSettings().enableFingerprintShield(fingerprintShield).apply()
            requireCrumbs.ui().notifyUIEvent(
                Event.PrivacySettings.TOGGLE_ANTI_FINGERPRINTING, fingerprintShield
            )
            true
        }

        blockSocialMediaIconsSwitch =
            findPreference(getString(R.string.crumbs_key_social_media_blocking))!!
        blockSocialMediaIconsSwitch.setOnPreferenceChangeListener { _, newValue ->
            val blockSocialMediaIcons = newValue as Boolean
            requirePrivacy.editSettings().blockSocialMediaIconsTracking(blockSocialMediaIcons)
                .apply()
            requireCrumbs.ui().notifyUIEvent(
                Event.PrivacySettings.TOGGLE_BLOCK_SOCIAL_MEDIA_ICONS,
                blockSocialMediaIcons
            )
            true
        }

        deAMPSwitch = findPreference(getString(R.string.crumbs_key_de_amp))!!
        deAMPSwitch.setOnPreferenceChangeListener { _, newValue ->
            val deAMP = newValue as Boolean
            requirePrivacy.editSettings().enableDeAMP(deAMP).apply()
            requireCrumbs.ui().notifyUIEvent(
                Event.PrivacySettings.TOGGLE_DE_AMP,
                deAMP
            )
            true
        }

        showAdvancedFeatures =
            findPreference(getString(R.string.crumbs_key_show_advanced_features))!!
        showAdvancedFeatures.setOnPreferenceChangeListener { _, newValue ->
            val enabled = newValue == true
            cookiesCategory.isVisible = enabled
            privacyCategory.isVisible = enabled
            true
        }
    }

    override fun onResume() {
        super.onResume()
        privacy?.listenSettings {
            enableTrackingSwitch.isChecked = it.enabled
            cookiesCategory.isEnabled = it.enabled
            privacyCategory.isEnabled = it.enabled
            thirdPartyCookiesSwitch.isChecked = it.blockThirdPartyCookies
            hideCookiesPopupSwitch.isChecked = it.hideCookieConsentPopups
            cookiesDropDown.isEnabled = it.blockThirdPartyCookies
            cookiesDropDown.value =
                cookiesDropDown.entries[it.cookiesFilterLevel.ordinal].toString()
            cookiesDropDown.summary = cookiesDropDown.value
            hideReferrerSwitch.isChecked = it.hideReferrerHeader
            dntSwitch.isChecked = it.enableDoNotTrack
            cnameSwitch.isChecked = it.enableCNameCloakingProtection
            proxySwitch.isChecked = it.proxyAdvertisingRequests
            gpcSwitch.isChecked = it.enableGPC
            removeMarketingParamsSwitch.isChecked = it.removeMarketingTrackingParameters
            blockHyperlinkAuditingSwitch.isChecked = it.blockHyperlinkAuditing
            fingerprintShieldSwitch.isChecked = it.enableFingerprintShield
            blockSocialMediaIconsSwitch.isChecked = it.blockSocialMediaIconsTracking
            deAMPSwitch.isChecked = it.deAMP
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        disabledFeatures = arguments?.getIntArray(ARGS_DISABLED_FEATURES)?.toCollection(ArrayList())
            ?: arrayListOf()

        disabledFeatures.forEach {
            when (it) {
                MAIN_FEATURE_ID -> {
                    enableTrackingSwitch.isVisible = false
                    showAdvancedFeatures.isVisible = false
                }

                INTERESTS_FEATURE_ID -> {
                    interestsPref.isVisible = false
                }

                EMAIL_RELAY_FEATURE_ID -> {
                    relayPref.isVisible = false
                }
            }
        }
    }

    @SuppressLint("RestrictedApi")
    override fun onCreateAdapter(preferenceScreen: PreferenceScreen): RecyclerView.Adapter<*> {
        setupHighlight()
        return CrumbsPreferenceGroupAdapter(preferenceScreen, highlightPref?.order)
    }

    private fun setupHighlight() {
        val highlightKey = arguments?.getString(ARGS_HIGHLIGHTED_FEATURE)
        highlightPref = findPreference(highlightKey.toString())

        highlightPref?.let {
            it.parent?.let { parent ->
                val isAdvancedFeature =
                    parent.key == getString(R.string.crumbs_key_privacy_section) || parent.key == getString(
                        R.string.crumbs_key_privacy_section
                    )
                if (isAdvancedFeature) setAdvancedSettings(true)
            }
            scrollToPreference(it)
        }
    }

    @Suppress("SameParameterValue")
    private fun setAdvancedSettings(enabled: Boolean) {
        showAdvancedFeatures.isChecked = enabled
        showAdvancedFeatures.onPreferenceChangeListener?.onPreferenceChange(
            showAdvancedFeatures, enabled
        )
    }
}