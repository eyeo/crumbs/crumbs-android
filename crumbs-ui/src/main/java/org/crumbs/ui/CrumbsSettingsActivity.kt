package org.crumbs.ui

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.add
import androidx.fragment.app.commit
import org.crumbs.CrumbsAndroid
import org.crumbs.CrumbsLogger
import org.crumbs.CrumbsProvider
import org.crumbs.ui.CrumbsCoreExtension.ui
import org.crumbs.ui.analytics.Event
import org.crumbs.ui.databinding.CrumbsActivityBinding
import org.crumbs.ui.relay.CrumbsEmailRelayFragment
import org.crumbs.ui.utils.parcelable
import org.crumbs.ui.utils.setToolbarTitle

class CrumbsSettingsActivity : AppCompatActivity(), CrumbsProvider {

    private lateinit var toolbar: ActionBar

    override fun onCreate(savedInstanceState: Bundle?) {
        if (intent.hasExtra(ARGS_THEME)) {
            setTheme(intent.getIntExtra(ARGS_THEME, 0))
        }
        super.onCreate(savedInstanceState)

        if (!CrumbsAndroid.isInitialized()) {
            finish()
            CrumbsLogger.w(TAG, "Crumbs is not initialized. CrumbsActivity can't be open")
            return
        }

        val binding = CrumbsActivityBinding.inflate(layoutInflater)

        toolbar = supportActionBar!!
        toolbar.setDisplayHomeAsUpEnabled(parentActivityIntent != null)

        if (savedInstanceState == null) {
            crumbs?.ui()?.notifyUIEvent(Event.Settings.OPEN_SCREEN)

            val args = Bundle()
            intent.getIntArrayExtra(ARGS_DISABLED_FEATURES)?.let {
                args.putIntArray(ARGS_DISABLED_FEATURES, it)
            }
            intent.getStringExtra(ARGS_HIGHLIGHTED_FEATURE)?.let {
                args.putString(ARGS_HIGHLIGHTED_FEATURE, it)
            }

            val initialFragmentId = intent.getIntExtra(ARGS_FRAGMENT, MAIN_FEATURE_ID)

            supportFragmentManager.addOnBackStackChangedListener {
                supportFragmentManager.fragments.lastOrNull()?.let {
                    setToolbarTitle(toFeatureId(it))
                }
            }

            supportFragmentManager.commit {
                setReorderingAllowed(true)
                setToolbarTitle(initialFragmentId)
                when (initialFragmentId) {
                    MAIN_FEATURE_ID ->
                        add<CrumbsSettingsFragment>(binding.fragmentContainerView.id, args = args)
                    INTERESTS_FEATURE_ID ->
                        add<CrumbsInterestsFragment>(binding.fragmentContainerView.id)
                    EMAIL_RELAY_FEATURE_ID ->
                        add<CrumbsEmailRelayFragment>(binding.fragmentContainerView.id)
                }
            }
        }
        setContentView(binding.root)
    }

    private fun setToolbarTitle(featureId: Int) {
        when (featureId) {
            MAIN_FEATURE_ID -> {
                setToolbarTitle(getString(R.string.crumbs_privacy_title))
            }
            INTERESTS_FEATURE_ID -> {
                setToolbarTitle(getString(R.string.crumbs_interests))
            }
            EMAIL_RELAY_FEATURE_ID -> {
                setToolbarTitle(getString(R.string.crumbs_email_relay))
            }
        }
    }

    override fun getParentActivityIntent(): Intent? {
        if (intent.hasExtra(ARGS_PARENT)) {
            return intent.parcelable(ARGS_PARENT)
        }
        return super.getParentActivityIntent()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
            return true
        }
        return false
    }

    companion object {

        private const val TAG = "CrumbsActivity"
        internal const val ARGS_DISABLED_FEATURES = "disabled_features"
        internal const val ARGS_HIGHLIGHTED_FEATURE = "highlighted_feature"
        private const val ARGS_FRAGMENT = "fragment_id"
        private const val ARGS_PARENT = "parent_intent"
        private const val ARGS_THEME = "theme"

        @JvmStatic
        val MAIN_FEATURE_ID = R.id.crumbs_privacy

        @JvmStatic
        val INTERESTS_FEATURE_ID = R.id.crumbs_interests

        @JvmStatic
        val EMAIL_RELAY_FEATURE_ID = R.id.crumbs_email_relay

        private fun toFeatureId(fragment: Fragment): Int {
            return when (fragment) {
                is CrumbsSettingsFragment -> MAIN_FEATURE_ID
                is CrumbsInterestsFragment -> INTERESTS_FEATURE_ID
                is CrumbsEmailRelayFragment -> EMAIL_RELAY_FEATURE_ID
                else -> Int.MAX_VALUE
            }
        }

        @JvmStatic
        @JvmOverloads
        fun createIntent(
            context: Context,
            selectedFeature: Int = 0,
            parentIntent: Intent? = null,
            disabledFeatures: IntArray? = null,
            highlightedFeature: String? = null,
            theme: Int? = null
        ) =
            Intent(context, CrumbsSettingsActivity::class.java).apply {
                if (selectedFeature != 0) {
                    putExtra(ARGS_FRAGMENT, selectedFeature)
                }
                parentIntent?.let {
                    putExtra(ARGS_PARENT, parentIntent)
                }
                disabledFeatures?.let {
                    putExtra(ARGS_DISABLED_FEATURES, it)
                }
                highlightedFeature?.let {
                    putExtra(ARGS_HIGHLIGHTED_FEATURE, highlightedFeature)
                }
                theme?.let {
                    putExtra(ARGS_THEME, it)
                }
            }
    }
}