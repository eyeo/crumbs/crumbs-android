package org.crumbs.ui.relay

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.core.widget.ContentLoadingProgressBar
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import org.crumbs.CrumbsAndroid
import org.crumbs.CrumbsProvider
import org.crumbs.concurrent.Cancellable
import org.crumbs.models.CrumbsEmailAlias
import org.crumbs.models.EmailRelayAPIError
import org.crumbs.presenter.EmailRelayPresenter
import org.crumbs.ui.CrumbsCoreExtension.ui
import org.crumbs.ui.R
import org.crumbs.ui.analytics.Event
import org.crumbs.ui.databinding.CrumbsDialogFragmentEmailBinding
import org.crumbs.ui.databinding.CrumbsItemEmailBinding
import org.crumbs.ui.utils.getLocalizedErrorMessage
import org.crumbs.utils.UrlExtension.getUrlHost

class CrumbsEmailRelayDialogFragment : BottomSheetDialogFragment(), CrumbsProvider {

    private var listenUrl: String? = null
    private var emailClickListener: EmailClickListener? = null
    private var job: Cancellable? = null

    private val emailAdapter = EmailAdapter()
    private lateinit var binding: CrumbsDialogFragmentEmailBinding
    private lateinit var progressBar: ContentLoadingProgressBar
    private lateinit var showMoreView: TextView
    private lateinit var onThisPageTv: TextView
    private lateinit var descriptionTv: TextView
    private lateinit var recycleView: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (savedInstanceState != null) {
            dismiss()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (!CrumbsAndroid.isInitialized()) return null
        binding = CrumbsDialogFragmentEmailBinding.inflate(inflater, container, false)
        if (savedInstanceState == null) {
            requireCrumbs.ui().notifyUIEvent(Event.RelayDialog.OPEN_SCREEN)
        }
        return binding.root.apply {
            progressBar = binding.crumbsEmailRelayDialogLoader
            recycleView = binding.crumbsEmailRelayDialogRv
            recycleView.apply {
                layoutManager = LinearLayoutManager(requireContext())
                adapter = emailAdapter
            }
            onThisPageTv = binding.crumbsEmailRelayDialogSubtitle
            descriptionTv = binding.crumbsEmailRelayDialogDescription
            showMoreView = binding.crumbsEmailRelayShowMore
            showMoreView.setOnClickListener {
                it.isVisible = false
                refreshEmails(null)
                requireCrumbs.ui().notifyUIEvent(Event.RelayDialog.PRESS_SHOW_MORE)
            }

            binding.crumbsEmailRelayDialogManagerBtn.setOnClickListener {
                openManageEmails()
            }
            binding.crumbsEmailRelayDialogCreateBtn.setOnClickListener {
                progressBar.show()
                job = emailRelay?.create(getUrl(), onSuccess = { email ->
                    emailAdapter.addEmail(email)
                    post {
                        recycleView.smoothScrollToPosition(0)
                    }
                    descriptionTv.isVisible = false
                    progressBar.hide()
                }, onError = {
                    Log.w(TAG, "can't create an email", it)
                    progressBar.hide()
                    if (it is EmailRelayAPIError) {
                        Log.w(TAG, "error code: ${it.code}, detail: ${it.detail}")
                        Toast.makeText(
                            context,
                            it.getLocalizedErrorMessage(context),
                            Toast.LENGTH_LONG
                        ).show()
                    }
                })
                requireCrumbs.ui().notifyUIEvent(Event.RelayDialog.PRESS_CREATE_EMAIL)
            }
        }
    }

    private fun openManageEmails() {
        val crumbsUI = requireCrumbs.ui()
        crumbsUI.openLink(
            requireContext(),
            EmailRelayPresenter.MANAGE_URL
        )
        dismiss()
        crumbsUI.notifyUIEvent(Event.RelayDialog.PRESS_MANAGE_EMAILS)
    }

    override fun onResume() {
        super.onResume()
        progressBar.show()
        progressBar.isIndeterminate = true
        emailAdapter.submitList(emptyList())
        val url = getUrl()
        refreshEmails(url)
    }

    private fun refreshEmails(url: String?) {
        this.listenUrl = url
        showMoreView.isVisible = listenUrl != null
        onThisPageTv.isVisible = listenUrl != null
        job?.cancel()
        job = emailRelay?.retrieve(url, onSuccess = { emails ->
            if (url != null && emails.isEmpty()) {
                refreshEmails(null)
                return@retrieve
            }
            emailAdapter.submitList(emails)
            recycleView.post {
                recycleView.smoothScrollToPosition(0)
            }
            descriptionTv.isVisible = emails.isEmpty()
            progressBar.hide()
        }, onError = {
            progressBar.hide()
        })
    }

    private fun getUrl(): String {
        return arguments?.getString(KEY_URL) ?: ""
    }

    fun setEmailClickListener(listener: ((String) -> Unit)): EmailClickListener {
        return object : EmailClickListener {
            override fun onEmailClicked(email: String) {
                listener.invoke(email)
            }
        }.also { setEmailClickListener(it) }
    }

    fun setEmailClickListener(listener: EmailClickListener?) {
        emailClickListener = listener
    }

    override fun onPause() {
        super.onPause()
        job?.cancel()
        job = null
    }

    companion object {

        private const val KEY_URL = "KEY_URL"
        const val TAG = "EmailRelayDialog"

        @JvmStatic
        fun newInstance(url: String): CrumbsEmailRelayDialogFragment =
            CrumbsEmailRelayDialogFragment().apply {
                this.arguments = Bundle().apply {
                    putString(KEY_URL, url)
                }

                val defaultTheme = R.style.Theme_Crumbs_BottomSheetDialog
                setStyle(STYLE_NO_FRAME, defaultTheme)
            }
    }

    interface EmailClickListener {
        fun onEmailClicked(email: String)
    }

    private inner class EmailAdapter :
        ListAdapter<CrumbsEmailAlias, EmailViewHolder>(object :
            DiffUtil.ItemCallback<CrumbsEmailAlias>() {
            override fun areItemsTheSame(
                oldItem: CrumbsEmailAlias,
                newItem: CrumbsEmailAlias
            ): Boolean = true

            override fun areContentsTheSame(
                oldItem: CrumbsEmailAlias,
                newItem: CrumbsEmailAlias
            ): Boolean =
                false
        }) {
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EmailViewHolder {
            val binding =
                CrumbsItemEmailBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            return EmailViewHolder(binding)
        }

        override fun onBindViewHolder(holder: EmailViewHolder, position: Int) {
            holder.setEmail(getItem(position))
        }

        fun addEmail(email: CrumbsEmailAlias) {
            val newList = arrayListOf<CrumbsEmailAlias>()
            newList.add(email)
            for (i in 0 until itemCount) {
                newList.add(getItem(i))
            }
            submitList(newList)
        }
    }

    private inner class EmailViewHolder(private val binding: CrumbsItemEmailBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun setEmail(item: CrumbsEmailAlias) {
            binding.crumbsItemEmailIv.setImageResource(if (item.isForwarding) R.drawable.crumbs_email_accepted else R.drawable.crumbs_email_blocked)
            binding.crumbsItemEmailText.text = item.displayAddress
            binding.crumbsItemEmailDomains.apply {
                isVisible = listenUrl == null
                text = item.domains.joinToString(", ")
            }
            this.itemView.setOnClickListener {
                copyEmail(item.displayAddress)
                requireEmailRelay.linkAliasAndDomain(item.displayAddress, getUrl())
                emailClickListener?.onEmailClicked(item.displayAddress)
                requireCrumbs.ui().notifyUIEvent(Event.RelayDialog.PRESS_EMAIL)
            }
            this.itemView.setOnLongClickListener {
                val url = getUrl()
                val host = url.getUrlHost()
                val options: Array<CharSequence> = arrayOf(
                    getString(android.R.string.copy),
                    getString(R.string.crumbs_email_relay_manage),
                    if (item.domains.find { host?.endsWith(it) == true } != null) getString(R.string.crumbs_email_relay_forget_domain) else null,
                ).filterNotNull().toTypedArray()
                MaterialAlertDialogBuilder(requireContext()).setItems(options) { _, which ->
                    when (which) {
                        0 -> {
                            copyEmail(item.displayAddress)
                            requireCrumbs.ui().notifyUIEvent(Event.RelayDialog.PRESS_COPY_EMAIL)
                        }
                        1 -> {
                            openManageEmails()
                        }
                        else -> {
                            requireEmailRelay.unlinkAliasAndDomain(item.displayAddress, url)
                            refreshEmails(url)
                            requireCrumbs.ui().notifyUIEvent(Event.RelayDialog.PRESS_FORGET_EMAIL)
                        }
                    }
                }.show()
                requireCrumbs.ui().notifyUIEvent(Event.RelayDialog.LONG_PRESS_EMAIL)
                true
            }
        }

        fun copyEmail(email: String) {
            val context = itemView.context
            val clip: ClipData =
                ClipData.newPlainText(context.getString(R.string.crumbs_email_relay), email)
            val clipboard =
                context.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
            clipboard.setPrimaryClip(clip)
            dismiss()
            Toast.makeText(context, getString(R.string.crumbs_copied, email), Toast.LENGTH_SHORT)
                .show()
        }
    }
}