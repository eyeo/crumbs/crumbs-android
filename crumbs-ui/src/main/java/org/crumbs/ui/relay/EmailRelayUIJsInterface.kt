package org.crumbs.ui.relay

import android.webkit.JavascriptInterface
import org.crumbs.CrumbsProvider

class EmailRelayUIJsInterface(private val emailRelayUI: EmailRelayUI) : CrumbsProvider {

    @JavascriptInterface
    fun onRelayClicked(tabId: String, documentUrl: String) {
        emailRelayUI.emailRelayEventListeners.iterator().run {
            while (hasNext()) {
                next().onEmailRelayIconClicked(tabId, documentUrl)
            }
        }
    }

    @JavascriptInterface
    fun onInputFocusChanged(tabId: String, documentUrl: String, focused: Boolean) {
        emailRelayUI.emailRelayEventListeners.iterator().run {
            while (hasNext()) {
                next().onInputFocusChanged(tabId, documentUrl, focused)
            }
        }
    }

    companion object {
        const val NAME = "CrumbsRelayAccountInterface"
    }

}