package org.crumbs.ui.relay

import android.app.Activity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import org.crumbs.CrumbsAndroid
import org.crumbs.CrumbsProvider
import org.crumbs.presenter.EmailRelayPresenter
import org.crumbs.ui.CrumbsCoreExtension.ui
import org.crumbs.ui.analytics.Event
import org.crumbs.ui.databinding.CrumbsFragmentEmailBinding

class CrumbsEmailRelayFragment : Fragment(), CrumbsProvider {

    private lateinit var binding: CrumbsFragmentEmailBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (!CrumbsAndroid.isInitialized()) return null

        if (savedInstanceState == null) {
            requireCrumbs.ui().notifyUIEvent(Event.RelaySettings.OPEN_SCREEN)
        }
        binding = CrumbsFragmentEmailBinding.inflate(inflater, container, false)

        return binding.root.apply {
            val enableSwitch = binding.crumbsEmailRelaySwitch
            enableSwitch.isChecked = requireEmailRelay.getSettings().enabled
            enableSwitch.setOnCheckedChangeListener { _, isChecked ->
                requireEmailRelay.editSettings()
                    .enable(isChecked)
                    .apply()
                requireCrumbs.ui().notifyUIEvent(Event.RelaySettings.TOGGLE_FEATURE, isChecked)
                if (isChecked && !requireEmailRelay.isSignedIn()) {
                    openSignUpPage()
                }
                updateUi()
            }
            binding.crumbsEmailRelaySwitchContainer.setOnClickListener {
                enableSwitch.toggle()
            }

            binding.crumbsEmailRelayLoginBtn.setOnClickListener {
                openSignUpPage()
            }
            binding.crumbsEmailRelayManageBtn.setOnClickListener {
                getChromiumSupportActivity()?.apply {
                    requireCrumbs.ui()
                        .openLink(this, EmailRelayPresenter.MANAGE_URL, true)
                    finish()
                    requireCrumbs.ui().notifyUIEvent(Event.RelaySettings.PRESS_MANAGE_EMAILS)
                }
            }
            binding.crumbsEmailRelayLogoutBtn.setOnClickListener {
                requireEmailRelay.signOut()
                updateUi()
                requireCrumbs.ui().notifyUIEvent(Event.RelaySettings.PRESS_LOGOUT)
            }
            updateUi()
        }
    }

    override fun onResume() {
        super.onResume()
        updateUi()
    }

    private fun getChromiumSupportActivity(): Activity? {
        return context as? Activity
    }

    private fun openSignUpPage() {
        getChromiumSupportActivity()?.apply {
            requireCrumbs.ui()
                .openLink(this, EmailRelayPresenter.SIGN_UP_URL, true)
            finish()
            requireCrumbs.ui().notifyUIEvent(Event.RelaySettings.PRESS_LOGIN)
        }
    }

    private fun updateUi() {
        requireCrumbs.apply {
            val enable = emailRelay().getSettings().enabled
            val logged = emailRelay().isSignedIn()
            binding.crumbsEmailRelayLoginBtn.isVisible = !logged && enable
            binding.crumbsEmailRelayLoggedGroup.isVisible = logged && enable
        }
    }

    companion object {
        fun newInstance(): CrumbsEmailRelayFragment = CrumbsEmailRelayFragment()
    }
}