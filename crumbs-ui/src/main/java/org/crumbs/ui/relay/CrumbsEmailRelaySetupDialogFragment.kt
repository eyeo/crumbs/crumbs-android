package org.crumbs.ui.relay

import android.app.Activity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import org.crumbs.CrumbsAndroid
import org.crumbs.CrumbsProvider
import org.crumbs.presenter.EmailRelayPresenter
import org.crumbs.ui.CrumbsCoreExtension.ui
import org.crumbs.ui.R
import org.crumbs.ui.analytics.Event
import org.crumbs.ui.databinding.CrumbsDialogFragmentEmailSetupBinding

class CrumbsEmailRelaySetupDialogFragment : BottomSheetDialogFragment(), CrumbsProvider {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        if (!CrumbsAndroid.isInitialized()) return null
        if (savedInstanceState == null) {
            requireCrumbs.ui().notifyUIEvent(Event.RelaySetupDialog.OPEN_SCREEN)
        }
        val binding = CrumbsDialogFragmentEmailSetupBinding.inflate(inflater, container, false)
        updateButtonVisibility(binding)
        return binding.root.apply {
            binding.crumbsEmailRelaySetupLoginBtn.setOnClickListener {
                getChromiumSupportActivity()?.apply {
                    requireCrumbs.ui().openLink(this, EmailRelayPresenter.SIGN_UP_URL, true)
                }
                dismiss()
                requireCrumbs.ui().notifyUIEvent(Event.RelaySetupDialog.PRESS_LOGIN)
            }
            binding.crumbsEmailRelaySetupDisableBtn.setOnClickListener {
                requireEmailRelay.editSettings()
                    .enable(false)
                    .apply()
                dismiss()
                requireCrumbs.ui().notifyUIEvent(Event.RelaySetupDialog.PRESS_DISABLE)
            }
            binding.crumbsEmailRelaySetupEnableBtn.setOnClickListener {
                requireEmailRelay.editSettings()
                    .enable(true)
                    .apply()
                requireCrumbs.ui().notifyUIEvent(Event.RelaySetupDialog.PRESS_ENABLE)
                if (requireEmailRelay.isSignedIn()) {
                    dismiss()
                } else {
                    updateButtonVisibility(binding)
                }
            }
        }
    }

    private fun updateButtonVisibility(binding: CrumbsDialogFragmentEmailSetupBinding) {
        val enabled = requireEmailRelay.getSettings().enabled
        binding.apply {
            crumbsEmailRelaySetupLoginBtn.isVisible = enabled
            crumbsEmailRelaySetupDisableBtn.isVisible = enabled
            crumbsEmailRelaySetupEnableBtn.isVisible = !enabled
        }
    }

    private fun getChromiumSupportActivity(): Activity? {
        return context as? Activity
    }

    companion object {

        const val TAG = "EmailRelaySetupDialog"

        @JvmStatic
        fun newInstance(): CrumbsEmailRelaySetupDialogFragment =
            CrumbsEmailRelaySetupDialogFragment().apply {
                val defaultTheme = R.style.Theme_Crumbs_BottomSheetDialog
                setStyle(STYLE_NO_FRAME, defaultTheme)
            }
    }

}