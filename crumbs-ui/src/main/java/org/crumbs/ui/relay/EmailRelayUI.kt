package org.crumbs.ui.relay

import android.content.Context
import android.os.Build
import android.webkit.WebView
import androidx.annotation.RequiresApi
import org.crumbs.CrumbsProvider
import org.crumbs.models.RequestType
import org.crumbs.presenter.EmailRelayPresenter
import org.crumbs.presenter.IntegrationPresenter
import org.crumbs.ui.R
import org.crumbs.utils.JsUtils.isolateJavascript
import org.crumbs.utils.JsUtils.toJavascriptObject
import java.io.BufferedReader

class EmailRelayUI(context: Context) : IntegrationPresenter.JavaScriptSnippetBuilder,
    CrumbsProvider {

    internal val emailRelayEventListeners: ArrayList<EmailRelayEventListener> = arrayListOf()
    private val snippet: String
    private val createAliasLabel: String

    init {
        snippet = context.resources.openRawResource(R.raw.relay_interface).bufferedReader()
            .use(BufferedReader::readText).isolateJavascript()
        createAliasLabel = context.getString(R.string.crumbs_email_relay_dialog_create)
    }

    @RequiresApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    fun setupWebViewJsInterface(webView: WebView) {
        webView.addJavascriptInterface(
            EmailRelayUIJsInterface(this),
            EmailRelayUIJsInterface.NAME,
        )
    }

    override fun build(tabId: String, documentUrl: String, requestType: RequestType): String {
        if (requireEmailRelay.getSettings().enabled && !documentUrl.startsWith(EmailRelayPresenter.WEBSITE_URL)) {
            val snippetBuilder = StringBuilder()
            val options = hashMapOf("createAliasLabel" to createAliasLabel, "tabId" to tabId)

            requireEmailRelay.getAliases(documentUrl).firstOrNull()?.let {
                options["mainAlias"] = it.displayAddress
            }
            snippetBuilder.append(options.toJavascriptObject(EMAIL_RELAY_UI_SETTINGS_KEY))
            snippetBuilder.append(snippet)
            return snippetBuilder.toString()
        }
        return ""
    }

    /**
     * Create a JS snippet to inject email in to the selected text field in the web page
     *
     * @param email alias to inject
     *
     * @return js snippet to execute in the browser
     */
    fun createJSSnippetToInjectEmail(email: String): String {
        return """
        if(document.activeElement.tagName === 'INPUT') {
            document.activeElement.value = '$email';
            document.activeElement.dispatchEvent(new Event('input', { bubbles: true }));
        }
        """.trimIndent()
    }

    /**
     * add email relay icon click listener
     *
     * @param listener event listener
     */
    fun addEmailRelayEventListener(listener: EmailRelayEventListener) {
        if (emailRelayEventListeners.isEmpty()) {
            requireIntegration.registerSnippetBuilder(this)
        }
        emailRelayEventListeners.add(listener)
    }

    /**
     * remove email relay icon listener
     *
     * @param listener event listener
     */
    fun removeEmailRelayEventListener(listener: EmailRelayEventListener): Boolean {
        val removed = emailRelayEventListeners.remove(listener)
        if (emailRelayEventListeners.isEmpty()) {
            requireIntegration.unregisterSnippetBuilder(this)
        }
        return removed
    }

    interface EmailRelayEventListener {
        fun onEmailRelayIconClicked(tabId: String, documentUrl: String)
        fun onInputFocusChanged(tabId: String, documentUrl: String, focused: Boolean)
    }

    companion object {
        private const val EMAIL_RELAY_UI_SETTINGS_KEY = "CrumbsRelayUISettings"
    }
}

