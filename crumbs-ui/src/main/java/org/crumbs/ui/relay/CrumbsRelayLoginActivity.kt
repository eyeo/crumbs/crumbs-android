package org.crumbs.ui.relay

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import org.crumbs.CrumbsProvider
import org.crumbs.ui.CrumbsCoreExtension.ui

class CrumbsRelayLoginActivity : Activity(), CrumbsProvider {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (Intent.ACTION_VIEW == intent.action) {
            intent.data?.let {
                crumbs?.ui()?.openLink(this, it.toString())
            }
        }
        finish()
    }
}