package org.crumbs.ui.utils

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.content.pm.ResolveInfo
import androidx.core.content.ContextCompat
import androidx.core.net.toUri
import org.crumbs.ui.CrumbsUI


class CustomTabOpener : CrumbsUI.LinkOpener {
    override fun openLink(context: Context, url: String, targetPackage: String?) {
        val customTabIntent = Intent(Intent.ACTION_VIEW)
            .apply {
                putExtra(EXTRA_SESSION, null as String?)
                putExtra(EXTRA_ENABLE_INSTANT_APPS, true)
                putExtra(EXTRA_SHARE_STATE, 0)
                this.`package` = targetPackage
                data = url.toUri()
            }

        resolveBest(context, customTabIntent)?.let {
            launchIntent(context, customTabIntent, it)
            return
        }

        ContextCompat.startActivity(context, customTabIntent, null)
    }

    @Suppress("DEPRECATION")
    private fun resolveBest(context: Context, intent: Intent): ResolveInfo? {
        val pm = context.packageManager
        val queryIntentActivities = pm.queryIntentActivities(
            intent,
            PackageManager.GET_META_DATA
        ).filter {
            it.activityInfo.metaData == null || !it.activityInfo.metaData.getBoolean(
                "crumbs_ignore_activity",
                false
            )
        }

        val customTabActivities = queryIntentActivities.filter {
            val serviceIntent = Intent()
            serviceIntent.action = ACTION_CUSTOM_TABS_CONNECTION
            serviceIntent.setPackage(it.activityInfo.packageName)
            pm.resolveService(serviceIntent, 0) != null
        }

        // return app custom tab activity
        customTabActivities.firstOrNull { it.activityInfo.packageName == context.packageName }
            ?.let { return it }
        // return an custom tab activity
        customTabActivities.firstOrNull()
            ?.let { return it }
        // return app web activity
        queryIntentActivities.firstOrNull { it.activityInfo.packageName == context.packageName }
            ?.let { return it }
        // return any web activity
        return queryIntentActivities.firstOrNull()
    }

    private fun launchIntent(context: Context, intent: Intent, info: ResolveInfo) {
        val possibleIntent = Intent(intent)
        possibleIntent.component = ComponentName(
            info.activityInfo.packageName,
            info.activityInfo.name
        )
        context.startActivity(possibleIntent)
    }

    companion object {
        const val EXTRA_ENABLE_INSTANT_APPS =
            "android.support.customtabs.extra.EXTRA_ENABLE_INSTANT_APPS"
        const val EXTRA_SHARE_STATE = "androidx.browser.customtabs.extra.SHARE_STATE"
        const val EXTRA_SESSION = "android.support.customtabs.extra.SESSION"
        const val ACTION_CUSTOM_TABS_CONNECTION =
            "android.support.customtabs.action.CustomTabsService"
    }
}