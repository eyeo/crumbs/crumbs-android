package org.crumbs.ui.utils

import android.content.Context
import android.content.res.Resources
import android.content.res.XmlResourceParser
import androidx.annotation.XmlRes
import org.crumbs.models.CrumbsInterest
import org.crumbs.models.EmailRelayAPIError
import org.crumbs.ui.R
import kotlin.collections.set

class CrumbsFormatter private constructor(context: Context) {

    private val interestTranslations = context.resources.getMap(R.xml.interests)

    fun getInterestDisplayName(interest: CrumbsInterest): String {
        val name = "${interest.emoji} ${interestTranslations[interest.id] ?: interest.name}"
        return interest.value?.let { "$name (${interest.value})" } ?: name
    }

    companion object {
        @Volatile
        private lateinit var instance: CrumbsFormatter

        fun getInstance(context: Context): CrumbsFormatter {
            synchronized(this) {
                if (!::instance.isInitialized) {
                    instance = CrumbsFormatter(context)
                }
                return instance
            }
        }

        private const val XML_TAG_ENTRY = "string"
    }

    private fun Resources.getMap(@XmlRes resourceId: Int): Map<Int, String> {
        val defaultsMap: MutableMap<Int, String> = hashMapOf()
        val xmlParser: XmlResourceParser = getXml(resourceId)
        var curTag: String? = null
        var value: String? = null
        var key: Int = Int.MIN_VALUE
        var eventType = xmlParser.eventType
        while (eventType != XmlResourceParser.END_DOCUMENT) {
            if (eventType == XmlResourceParser.START_TAG) {
                curTag = xmlParser.name
                key = xmlParser.getAttributeIntValue(0, Int.MIN_VALUE)
            } else if (eventType == XmlResourceParser.END_TAG) {
                if (xmlParser.name == XML_TAG_ENTRY) {
                    if (key != Int.MIN_VALUE && value != null) {
                        defaultsMap[key] = value
                    }
                    key = Int.MIN_VALUE
                    value = null
                }
                curTag = null
            } else if (eventType == XmlResourceParser.TEXT) {
                if (curTag != null) {
                    value = xmlParser.text.replace("\\'", "'")
                    if (value.firstOrNull() == '"' && value.lastOrNull() == '"') {
                        value = value.substring(1, value.length - 2)
                    }
                }
            }
            eventType = xmlParser.next()
        }
        return defaultsMap
    }

}


fun EmailRelayAPIError.getLocalizedErrorMessage(context: Context): String {
    return when (code) {
        EmailRelayAPIError.AUTH_INVALID_EMAIL -> context.getString(R.string.crumbs_email_relay_auth_invalid_email)
        EmailRelayAPIError.ALIAS_LIMIT_REACHED -> context.getString(R.string.crumbs_email_relay_limit_reach)
        EmailRelayAPIError.AUTH_INVALID_TOKEN_FORMAT,
        EmailRelayAPIError.AUTH_INVALID_TOKEN,
        EmailRelayAPIError.AUTH_TOKEN_ALREADY_USED,
        EmailRelayAPIError.AUTH_NOT_EXISTING_USER,
        EmailRelayAPIError.AUTH_REMOTE_CALL_FAILED,
        EmailRelayAPIError.AUTH_TOKEN_EXPIRED -> context.getString(R.string.crumbs_email_relay_auth_failed)
        EmailRelayAPIError.AUTH_FAILED_TO_CREATE_USER,
        EmailRelayAPIError.AUTH_SENDING_MAGIC_LINK_FAILED,
        EmailRelayAPIError.ALIAS_UPDATE_FAILED,
        EmailRelayAPIError.ALIAS_DELETE_FAILED,
        EmailRelayAPIError.ALIAS_LIST_FAILED,
        EmailRelayAPIError.ALIAS_CREATE_FAILED -> context.getString(R.string.crumbs_email_relay_server_error)
        else -> detail
    }
}