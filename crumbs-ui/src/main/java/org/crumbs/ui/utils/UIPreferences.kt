package org.crumbs.ui.utils

import androidx.fragment.app.DialogFragment
import kotlinx.serialization.builtins.serializer
import org.crumbs.CrumbsAndroid
import org.crumbs.provider.StorageProvider
import org.crumbs.provider.StoredValue
import org.crumbs.ui.CrumbsSettingsFragment
import org.crumbs.ui.CrumbsUI
import org.crumbs.ui.analytics.Event

enum class TermsAndConditionsState {
    ACCEPTED,
    REJECTED,
    LATER,
    NONE
}

class UIPreferences internal constructor(storageProvider: StorageProvider) {
    internal var linkOpener: CrumbsUI.LinkOpener? = CustomTabOpener()
    private val sharedPref = StoredValue(
        TERMS_AND_CONDITION_KEY,
        storageProvider,
        Int.serializer(),
        { TermsAndConditionsState.NONE.ordinal },
        version = 2,
        onUpgrade = { oldVersion, _ ->
            if (oldVersion == 1) {
                if (CrumbsAndroid.get().privacy().getSettings().enabled) {
                    TermsAndConditionsState.ACCEPTED.ordinal
                } else {
                    TermsAndConditionsState.NONE.ordinal
                }
            } else {
                null
            }

        }
    )

    fun setLinkOpener(opener: CrumbsUI.LinkOpener?) {
        linkOpener = opener
    }

    /**
     * A custom terms and conditions dialog to be shown instead of the default one when the user tries to
     * enable Crumbs inside [CrumbsSettingsFragment].
     */
    @JvmField
    var termsAndConditionsDialog: DialogFragment? = null

    fun setTermsAndConditionsState(state: TermsAndConditionsState) {
        sharedPref.store(state.ordinal)
        CrumbsUI.get().notifyUIEvent(Event.TermsAndConditions.CHANGE_TNC_STATE, state)
    }

    fun getTermsAndConditionsState() =
        TermsAndConditionsState.values()[sharedPref.value]

    companion object {
        const val TERMS_AND_CONDITION_KEY = "org.crumbs.ui.tnc.TERMS_AND_CONDITION_KEY"
    }
}