package org.crumbs.ui.tnc

import android.content.Context
import android.graphics.Rect
import android.os.Parcel
import android.text.Spannable
import android.text.Spanned
import android.text.method.LinkMovementMethod
import android.text.method.TransformationMethod
import android.text.style.URLSpan
import android.util.AttributeSet
import android.view.View
import android.widget.TextView
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.text.HtmlCompat
import org.crumbs.CrumbsProvider
import org.crumbs.ui.CrumbsCoreExtension.ui
import org.crumbs.ui.R


class CrumbsTnCTextView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : AppCompatTextView(context, attrs, defStyleAttr) {

    init {
        val spannable = HtmlCompat.fromHtml(
            context.getString(
                R.string.crumbs_tnc_dialog_link,
                context.getString(R.string.crumbs_terms_and_conditions_url)
            ), HtmlCompat.FROM_HTML_MODE_LEGACY
        )
        transformationMethod = LinkTransformationMethod()
        movementMethod = LinkMovementMethod.getInstance()
        text = spannable
    }

    private class CustomTabsURLSpan : URLSpan, CrumbsProvider {
        constructor(url: String?) : super(url)
        constructor(src: Parcel) : super(src)

        override fun onClick(widget: View) {
            val url = url
            crumbs?.ui()?.openLink(widget.context, url, false)
        }
    }

    class LinkTransformationMethod : TransformationMethod {
        override fun getTransformation(source: CharSequence, view: View): CharSequence {
            if (view is TextView) {
                if (view.text == null || view.text !is Spannable) {
                    return source
                }
                val text = view.text as Spannable
                val spans = text.getSpans(
                    0, view.length(),
                    URLSpan::class.java
                )
                for (i in spans.indices.reversed()) {
                    val oldSpan = spans[i]
                    val start = text.getSpanStart(oldSpan)
                    val end = text.getSpanEnd(oldSpan)
                    val url = oldSpan.url
                    text.removeSpan(oldSpan)
                    text.setSpan(
                        CustomTabsURLSpan(url),
                        start,
                        end,
                        Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
                    )
                }
                return text
            }
            return source
        }

        override fun onFocusChanged(
            view: View?,
            sourceText: CharSequence?,
            focused: Boolean,
            direction: Int,
            previouslyFocusedRect: Rect?
        ) {
        }
    }
}