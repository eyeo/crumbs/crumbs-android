package org.crumbs.ui.tnc

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.setFragmentResult
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import org.crumbs.CrumbsProvider
import org.crumbs.ui.CrumbsCoreExtension.ui
import org.crumbs.ui.R
import org.crumbs.ui.analytics.Event
import org.crumbs.ui.databinding.CrumbsActivationDialogBinding
import org.crumbs.ui.utils.TermsAndConditionsState

class CrumbsTnCDialogFragment : DialogFragment(), CrumbsProvider {

    private var enableAfterAcceptance = false
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        requireCrumbs.ui().notifyUIEvent(Event.TermsAndConditions.OPEN_DIALOG)
        val binding = CrumbsActivationDialogBinding.inflate(LayoutInflater.from(context))
        return MaterialAlertDialogBuilder(requireContext())
            .setView(binding.root)
            .setTitle(getString(R.string.crumbs_tnc_dialog_title))
            .setPositiveButton(getString(R.string.crumbs_tnc_dialog_accept)) { _, _ ->
                requireCrumbs.ui().preferences.setTermsAndConditionsState(TermsAndConditionsState.ACCEPTED)
                if (enableAfterAcceptance) {
                    requirePrivacy.editSettings().enable(true).apply()
                    requireInterests.editSettings().enable(true).apply()
                }
            }
            .setNeutralButton(getString(R.string.crumbs_tnc_dialog_later)) { _, _ ->
                setFragmentResult(
                    RESULT_KEY,
                    Bundle().apply { putBoolean(RESULT_VALUE_KEY, false) })
                requireCrumbs.ui().preferences.setTermsAndConditionsState(TermsAndConditionsState.LATER)
            }
            .create()

    }

    companion object {
        fun newInstance(enableAfterAccept: Boolean = false): CrumbsTnCDialogFragment {
            val dialog = CrumbsTnCDialogFragment()
            dialog.enableAfterAcceptance = enableAfterAccept
            return dialog
        }

        const val RESULT_KEY = "org.crumbs.ui.tnc.CrumbsTnCDialogFragment_Result"
        const val RESULT_VALUE_KEY = "result"
        const val TAG = "CrumbsTnCDialogFragment"
    }
}