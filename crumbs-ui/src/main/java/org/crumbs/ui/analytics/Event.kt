package org.crumbs.ui.analytics

data class Event<T> internal constructor(val id: String, val value: T?) {

    fun hasValue() = value == null

    @Suppress("UNCHECKED_CAST")
    fun <T> value(): T = value as T

    //tag::eventIds[]
    /**
     * CrumbsSettingsActivity events
     */
    object Settings {
        /**
         * Crumbs settings activity has been opened
         */
        const val OPEN_SCREEN = "settings_open_screen"
    }

    object TermsAndConditions {
        /**
         * Crumbs accept terms dialog has been opened
         */
        const val OPEN_DIALOG = "terms_and_conditions_open_dialog"

        /**
         * Crumbs terms and conditions state has been changed
         * Event value type is TermsAndConditionsState enum
         */
        const val CHANGE_TNC_STATE = "terms_and_conditions_state_changed"
    }

    /**
     * CrumbsPrivacyFragment events
     */
    object PrivacySettings {

        /**
         * Privacy settings fragment has been opened
         */
        const val OPEN_SCREEN = "privacy_settings_open_screen"

        /**
         * Toggle global privacy protection
         * Event value type is boolean
         */
        const val TOGGLE_FEATURE = "privacy_settings_toggle_feature"

        /**
         * Toggle third party blocking feature
         * Event value type is boolean
         */
        const val TOGGLE_BLOCK_THIRD_PARTY_COOKIES =
            "privacy_settings_toggle_block_third_party_cookies"

        /**
         * Select third party cookies filter level
         * Event value type is FilterLevel
         */
        const val SELECT_COOKIES_FILTER_LEVEL = "privacy_settings_select_cookies_filter_level"

        /**
         * Toggle hide cookie consent popups option
         * Event value type is boolean
         */
        const val TOGGLE_HIDE_COOKIE_CONSENT_POPUPS = "privacy_settings_hide_cookie_consent_popups"

        /**
         * Toggle hide referrer header option
         * Event value type is boolean
         */
        const val TOGGLE_HIDE_REFERRER_HEADER = "privacy_settings_toggle_hide_referrer_header"

        /**
         * Toggle do not track signal
         * Event value type is boolean
         */
        const val TOGGLE_DO_NOT_TRACK = "privacy_settings_toggle_do_not_track"

        /**
         * Toggle proxy advertising requests option
         * Event value type is boolean
         */
        const val TOGGLE_PROXY_ADVERTISING_REQUESTS = "privacy_settings_toggle_advertising_requests"

        /**
         * Toggle GCP signal
         * Event value type is boolean
         */
        const val TOGGLE_ENABLE_GPC = "privacy_settings_toggle_enable_gcp"

        /**
         * Toggle cname cloaking protection
         * Event value type is boolean
         */
        const val TOGGLE_CNAME_CLOAKING_PROTECTION =
            "privacy_settings_toggle_cname_cloaking_protection"

        /**
         * Toggle remove marketing tracking parameters option
         * Event value type is boolean
         */
        const val TOGGLE_REMOVE_MARKETING_TRACKING_PARAMETERS =
            "privacy_settings_toggle_remove_marketing_tracking_parameters"

        /**
         * Toggle block hyperlink auditing option
         * Event value type is boolean
         */
        const val TOGGLE_BLOCK_HYPERLINK_AUDITING =
            "privacy_settings_toggle_block_hyperlink_auditing"

        /**
         * Toggle fingerprinting protection
         * Event value type is boolean
         */
        const val TOGGLE_ANTI_FINGERPRINTING =
            "privacy_settings_toggle_anti_fingerprinting"

        /**
         * Toggle the blocking of social media icons
         * Event value type is boolean
         */
        const val TOGGLE_BLOCK_SOCIAL_MEDIA_ICONS =
            "privacy_settings_toggle_block_social_media_icons"

        /**
         * Toggle the De-AMP feature
         * Event value type is boolean
         */
        const val TOGGLE_DE_AMP = "privacy_settings_toggle_de_amp"
    }

    /**
     * CrumbsInterestsFragment events
     */
    object InterestsSettings {

        /**
         * Interests settings fragment has been opened
         */
        const val OPEN_SCREEN = "interests_settings_open_screen"

        /**
         * Toggle global interests feature
         * Event value type is boolean
         */
        const val TOGGLE_FEATURE = "interests_settings_toggle_feature"

        /**
         * Search bar focused
         */
        const val FOCUS_SEARCH = "interests_settings_focus_search"

        /**
         * Toggle interests state in the profile list
         * Event value type is ProfileInterest
         */
        const val TOGGLE_INTEREST_STATE = "interests_settings_toggle_interest_state"


        /**
         * Swipe interest to forget it
         * Event value type is ProfileInterest
         */
        const val SWIPE_INTEREST = "interests_settings_swipe_interest"

        /**
         * Validate forget interest warning popup
         * Event value type is ProfileInterest
         */
        const val FORGET_INTEREST = "interests_settings_forget_interest"
    }

    /**
     * CrumbsEmailRelayFragment events
     */
    object RelaySettings {

        /**
         * Toggle email relay feature
         * Event value type is boolean
         */
        const val TOGGLE_FEATURE = "relay_settings_toggle_feature"

        /**
         * EmailRelay settings fragment has been opened
         */
        const val OPEN_SCREEN = "relay_settings_open_screen"

        /**
         * Login button has been pressed
         */
        const val PRESS_LOGIN = "relay_settings_press_login"

        /**
         * Logout button has been pressed
         */
        const val PRESS_LOGOUT = "relay_settings_press_logout"

        /**
         * Manage emails button has been pressed
         */
        const val PRESS_MANAGE_EMAILS = "relay_settings_press_manage_emails"

    }

    /**
     * CrumbsEmailRelaySetupDialogFragment events
     */
    object RelaySetupDialog {
        /**
         * EmailRelay setup dialog fragment has been opened
         */
        const val OPEN_SCREEN = "relay_setup_dialog_open_screen"

        /**
         * Login has been pressed
         */
        const val PRESS_LOGIN = "relay_setup_dialog_press_login"

        /**
         * Disable has been pressed
         */
        const val PRESS_DISABLE = "relay_setup_dialog_press_disable"

        /**
         * Enable has been pressed
         */
        const val PRESS_ENABLE = "relay_setup_dialog_press_enable"

    }

    /**
     * CrumbsEmailRelayDialogFragment events
     */
    object RelayDialog {
        /**
         * EmailRelay dialog fragment has been opened
         */
        const val OPEN_SCREEN = "relay_dialog_open_screen"

        /**
         * Show more button has been pressed
         */
        const val PRESS_SHOW_MORE = "relay_dialog_press_show_more"

        /**
         * Email has been pressed
         */
        const val PRESS_EMAIL = "relay_dialog_press_email"

        /**
         * Email has been long pressed
         */
        const val LONG_PRESS_EMAIL = "relay_dialog_long_press_email"

        /**
         * Copy email has been pressed
         */
        const val PRESS_COPY_EMAIL = "relay_dialog_press_copy_email"

        /**
         * Email has been forgotten
         */
        const val PRESS_FORGET_EMAIL = "relay_dialog_press_forget_email"

        /**
         * Disable button has been pressed
         */
        const val PRESS_CREATE_EMAIL = "relay_dialog_press_create_email"

        /**
         * Manage emails button has been pressed
         */
        const val PRESS_MANAGE_EMAILS = "relay_dialog_press_manage_emails"

    }

    /**
     * CrumbsPopup events
     */
    object Popup {
        /**
         * Popup has been opened
         */
        const val OPEN_SCREEN = "popup_open_screen"

        /**
         * Settings button has been pressed
         */
        const val PRESS_SETTINGS = "popup_press_settings"

        /**
         * More interests button has been pressed
         */
        const val PRESS_MORE_INTERESTS = "popup_press_more_interests"

        /**
         * Feedback button has been pressed
         */
        const val PRESS_FEEDBACK = "popup_press_feedback"

        /**
         * Email relay button has been pressed
         */
        const val PRESS_EMAIL_RELAY = "popup_press_email_relay"
    }
    //end::eventIds[]
}