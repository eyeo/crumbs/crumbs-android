@file:Suppress("DEPRECATION")

package org.crumbs.test

import android.graphics.Bitmap
import android.os.Environment
import android.os.Environment.DIRECTORY_DOWNLOADS
import android.view.View
import androidx.test.runner.screenshot.BasicScreenCaptureProcessor
import androidx.test.runner.screenshot.Screenshot
import java.io.File

fun captureScreenshot(screenshotName: String, childFolder: String = "screenshots_crumbs") {
    Screenshot.capture()
        .apply {
            format = Bitmap.CompressFormat.PNG
            name = screenshotName
            process(setOf(CrumbsScreenCaptureProcessor(childFolder)))
        }
}

fun captureScreenshot(view: View, pictureName: String, childFolder: String = "screenshots_crumbs") {
    Screenshot.capture(view)
        .apply {
            format = Bitmap.CompressFormat.PNG
            name = pictureName
            process(setOf(CrumbsScreenCaptureProcessor(childFolder)))
        }
}

private class CrumbsScreenCaptureProcessor(childFolder: String) :
    BasicScreenCaptureProcessor() {

    init {
        mDefaultScreenshotPath = File(
            Environment.getExternalStoragePublicDirectory(DIRECTORY_DOWNLOADS),
            childFolder
        )
    }

    override fun getFilename(prefix: String) = prefix
}