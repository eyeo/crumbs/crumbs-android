@Suppress("DSL_SCOPE_VIOLATION")
plugins {
    alias(libs.plugins.android.library)
    alias(libs.plugins.kotlin.android)
}

android {
    namespace = "org.crumbs.test"
    defaultConfig {
        minSdk = 28
    }
}

dependencies {
    implementation(libs.test.androidx.rules)
    implementation(libs.test.mockk.android)
}