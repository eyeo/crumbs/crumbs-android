#!/bin/bash

export PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )/.." && pwd )"
echo "Project dir: $PROJECT_DIR"

export ANDROID_CLI_URL='https://dl.google.com/android/repository/commandlinetools-linux-8512546_latest.zip'
export ANDROID_CLI_FILE="$(basename "$ANDROID_CLI_URL")"
export ANDROID_CLI_SHA256='2ccbda4302db862a28ada25aa7425d99dce9462046003c1714b059b5c47970d8'

export ANDROID_HOME="$PROJECT_DIR/.android"
export ANDROID_SDK_ROOT="$ANDROID_HOME"
export ANDROID_AVD_HOME="$ANDROID_HOME/avd"
export ANDROID_EMULATOR_HOME="$ANDROID_AVD_HOME"
export ANDROID_NDK_HOME="$ANDROID_HOME/ndk/$ANDROID_NDK_VERSION"
export ANDROID_NDK_ROOT="$ANDROID_NDK_HOME"

export GRADLE_USER_HOME="$PROJECT_DIR/.gradle"
export GRADLE_OPTS="-Dorg.gradle.daemon=false -Dorg.gradle.console=plain"

export BIN_HOME="$PROJECT_DIR/.bin"

export PATH="$PATH:$BIN_HOME/ruby/bin:$ANDROID_SDK_ROOT/emulator:$ANDROID_SDK_ROOT/cmdline-tools/latest/bin:$ANDROID_SDK_ROOT/tools:$ANDROID_SDK_ROOT/tools/bin:$ANDROID_SDK_ROOT/platform-tools"