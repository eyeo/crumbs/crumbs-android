#!/bin/bash

download_and_verify() {
	url=$1
	checksum=$2
	file="$(basename "$url")"
	curl -sSLo "$file" "$url"
	echo "$checksum $file" | sha256sum -c -
}

# Cleanup existing directories
rm -rf "$ANDROID_HOME"
rm -rf "$GRADLE_USER_HOME"
rm -rf "$BIN_HOME"

# Download and extract the command line tools
download_and_verify $ANDROID_CLI_URL $ANDROID_CLI_SHA256
mkdir "$ANDROID_HOME"
unzip -q "$ANDROID_CLI_FILE" -d "$ANDROID_HOME/cmdline-tools"
mv "$ANDROID_HOME/cmdline-tools/cmdline-tools" "$ANDROID_HOME/cmdline-tools/latest"
rm $ANDROID_CLI_FILE

echo '### User Sources for Android SDK Manager' > "$ANDROID_HOME/repositories.cfg"

# Accept the all Android licenses
yes | sdkmanager --licenses && yes | sdkmanager --update
sdkmanager "tools"
sdkmanager --install "cmake;3.22.1"

# Configure emulator
export ANDROID_DEVICE_PACKAGE="system-images;android-32;google_apis;x86_64"
sdkmanager --install $ANDROID_DEVICE_PACKAGE
avdmanager --verbose create avd --name "device-android" --path "$ANDROID_AVD_HOME" --package "$ANDROID_DEVICE_PACKAGE" --device "Nexus 5"
# fix path being ignored
cp /root/.android/avd/device-android.ini $ANDROID_AVD_HOME
avdmanager list avds

# Populate the cache
(
	cd "$PROJECT_DIR"
	./gradlew tasks dependencies
	./gradlew installDocumentationDependencies
	./gradlew --dry-run assemble
)

