@Suppress("DSL_SCOPE_VIOLATION")
plugins {
    `maven-publish`
    alias(libs.plugins.android.library)
    alias(libs.plugins.kotlin.android)
}

android {
    defaultConfig {
        namespace = "org.crumbs.abp"
        minSdk = libs.versions.android.webview.min.sdk.get().toInt()
    }
}

dependencies {
    api(libs.crumbs.core)
    implementation(libs.kotlin.stdlib)
    implementation(libs.bundles.adblock)
}

extra["artifact_id"] = "crumbs-abp-android"
apply(file("../publish.gradle"))
