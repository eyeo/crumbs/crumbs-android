import groovy.json.JsonOutput
import groovy.json.JsonSlurper
import groovy.util.slurpersupport.Node
import groovy.xml.MarkupBuilder
import org.gradle.api.DefaultTask
import org.gradle.api.GradleException
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.InputFile
import org.gradle.api.tasks.OutputFile
import org.gradle.api.tasks.TaskAction

class ConvertInterestsTask extends DefaultTask {

    @Input
    String interestsUrl

    @OutputFile
    File outputFile

    @TaskAction
    void convertFile() {
        def interestsJson = new JsonSlurper().parse(new URL(interestsUrl))
        outputFile.parentFile.mkdirs()

        outputFile.withWriter { writer ->
            def xml = new MarkupBuilder(new IndentPrinter(writer, "    ", true))
            xml.doubleQuotes = true
            xml.mkp.xmlDeclaration(version: "1.0", encoding: "utf-8")
            xml.resources {
                for (interest in interestsJson) {
                    string(name: interest["id"], interest["name"])
                }
            }
        }
    }

}

class TranslateInterestsTask extends DefaultTask {

    @InputFile
    File inputFile

    @Input
    String apiKey

    @TaskAction
    void run() {
        def map = createMap(inputFile)

        def folders = inputFile.parentFile.parentFile.listFiles(new FilenameFilter() {
            @Override
            boolean accept(File file, String name) {
                return file.isDirectory() && name.startsWith("xml-")
            }
        })

        folders.each {
            def labels = map.keySet().toList()
            def translations = this.translate(it.name.split("-").last(), labels)
            def outputFile = new File(it, "interests.xml")
            outputFile.withWriter { writer ->
                def xml = new MarkupBuilder(new IndentPrinter(writer, "    ", true))
                xml.doubleQuotes = true
                xml.mkp.xmlDeclaration(version: "1.0", encoding: "utf-8")
                xml.resources {
                    for (i in 0..<translations.size()) {
                        string(name: map[labels[i]], translations[i])
                    }
                }
            }
        }

    }

    List<String> translate(String targetLanguage, List<String> texts) {
        return texts.collate(128).collect { translateRequest(targetLanguage, it) }.flatten()
    }

    List<String> translateRequest(String targetLanguage, List<String> texts) {
        def data = [
                "q"     : texts,
                "target": targetLanguage,
                "source": "en",
                "format": "text"
        ]
        def baseUrl = new URL("https://translation.googleapis.com/language/translate/v2?key=$apiKey")
        def text = baseUrl.openConnection().with {
            requestMethod = 'POST'
            doOutput = true
            setRequestProperty("Content-Type", "application/json")
            outputStream.withWriter { writer ->
                writer << JsonOutput.toJson(data)
            }

            def code = getResponseCode();
            if (code != 200) {
                throw new GradleException("Google Translate API error: " + getErrorStream().text)
            }
            content.text
        }
        def translatedTexts = new JsonSlurper().parseText(text)["data"]["translations"].collect { it["translatedText"] }
        return translatedTexts

    }

    def createMap(File file) {
        def list = new XmlSlurper().parse(file)
        def map = [:]
        list.childNodes().forEachRemaining({
            Node node ->
                def id = node.attributes["name"]
                map[node.text()] = id
        })
        return map
    }


}