import org.gradle.api.DefaultTask
import org.gradle.api.tasks.InputDirectory
import org.gradle.api.tasks.TaskAction
import java.io.File

abstract class EmbedScreenshotsTask : DefaultTask() {

    @get:InputDirectory
    abstract var inputDirectory : File

    @TaskAction
    fun embedScreenshots() {
        val reportsDirectory = inputDirectory.parent
        val screenshotsDirectoryName = inputDirectory.name

        if (!inputDirectory.exists()) {
            println("Could not find screenshots...")
            return
        }
        inputDirectory.listFiles()?.forEach {
            val className = it.name
            val classFolder = File("${reportsDirectory}/$screenshotsDirectoryName/$className")
            classFolder.listFiles()?.forEach { testFile ->
                val testFileName = testFile.name
                val testName = testFileName.substringBefore("_")
                val testReportFile = File(reportsDirectory, "${className}.html")
                if (!testReportFile.exists()) {
                    println("Could not find ${testReportFile.name}")
                    return
                }

                val original = "<h3 class=\"failures\">${testName}</h3>"
                val embedded = original +
                        "\n<img src=\"$screenshotsDirectoryName/$className/${testFileName}\" width =\"320\" />"

                val content = testReportFile.readText().replace(original, embedded)
                testReportFile.writeText(content)
            }
        }
    }

}