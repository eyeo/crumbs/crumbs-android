plugins {
    id("org.jetbrains.kotlin.jvm") version "1.7.20"
}

repositories {
    mavenCentral()
    maven("https://jcenter.bintray.com")
}

dependencies {
    implementation("io.github.http-builder-ng:http-builder-ng-okhttp:1.0.4")
    implementation("com.squareup.okhttp3:logging-interceptor:4.9.0")
}