Crumbs Android
=================

This repository contains Android Crumbs integration libraries.

## [Documentation](https://eyeo.gitlab.io/crumbs/crumbs-android)

To use Crumbs Android integration libraries please check the [user documentation](https://eyeo.gitlab.io/crumbs/crumbs-android)
You can also check Crumbs Core details thought the [core documentation](https://eyeo.gitlab.io/crumbs/crumbs-core).
Crumbs Android [ChangeLog](https://eyeo.gitlab.io/crumbs/crumbs-android/changeLog) is available in the documentation.

## Requirements

In order to run the unit test suite or build the project you need a working java environment.
This project has been build with Android SDK

## Developer commands

|Command|Description|
|--|--|
|`./gradlew assemble`| assemble modules |
|`./gradlew build`| assemble and test modules |
|`cd ./docs && bundle exec jekyll serve --livereload`| work on documentation locally |
|`./gradlew updateScreenShots`| run android instrumentation tests to update documentation screenshots|

## CI

Artifacts are uploaded on the [maven repository](https://gitlab.com/api/v4/groups/8621009/-/packages/maven) with the following version name:
- master => X.X.X
- develop => X.X.X-SNAPSHOT
- feat/shared/branch_name => X.X.X-branch-name-SNAPSHOT

During development, when it is required to share a feature between repositories depending on each other, consider using `feat/shared/branch_name` for all repositories until their merge to develop.

## License

Licensed under GPL 3.0;
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    https://www.gnu.org/licenses/gpl-3.0.en.html