import java.io.FileInputStream
import java.util.*

plugins {
    id("com.android.application")
    id("kotlin-android")
}


android {
    compileSdk = libs.versions.android.target.sdk.get().toInt()

    defaultConfig {
        applicationId = "org.crumbs.webview.demo"
        minSdk = libs.versions.android.webview.min.sdk.get().toInt()
        targetSdk = compileSdk
        versionCode = 1
        versionName = project.version.toString()
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    val keystorePropertiesFile = file("keystore.properties")
    if (keystorePropertiesFile.exists()) {
        val keystoreProperties = Properties()
        keystoreProperties.load(FileInputStream(keystorePropertiesFile))

        signingConfigs {
            create("release") {
                keyAlias = keystoreProperties.getProperty("keyAlias")
                keyPassword = keystoreProperties.getProperty("storePassword")
                storeFile = file(keystoreProperties.getProperty("storeFile"))
                storePassword = keystoreProperties.getProperty("storePassword")
            }
        }
    }
    buildTypes {
        val demoSigningConfig =
            signingConfigs.findByName(if (keystorePropertiesFile.exists()) "release" else "debug")
        debug {
            signingConfig = demoSigningConfig
        }
        release {
            isMinifyEnabled = true
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
            signingConfig = demoSigningConfig
        }
    }

    packagingOptions {
        resources.excludes.add("META-INF/*.kotlin_module")
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }
}

dependencies {
    implementation(project(":crumbs-webview"))
    implementation(project(":crumbs-ui"))
    implementation(project(":crumbs-abp"))
    implementation(libs.kotlin.stdlib)
    implementation(libs.androidx.core.ktx)
    implementation(libs.androidx.appcompat)
    implementation(libs.androidx.constraintlayout)
    implementation(libs.androidx.swiperefreshlayout)
    implementation(libs.androidx.cardview)
    implementation(libs.androidx.lifecycle)
    implementation(libs.google.material)
    implementation(libs.bundles.adblock)
    implementation(libs.adblock.settings)
}