package org.crumbs.webview.demo

import android.app.Application
import org.crumbs.CrumbsAndroid
import org.crumbs.CrumbsLogger
import org.crumbs.CrumbsLoggerLevel
import org.crumbs.models.Visit
import org.crumbs.provider.HistoryCallback
import org.crumbs.provider.HistoryProvider
import org.crumbs.service.EmailRelayService
import org.crumbs.ui.CrumbsUI
import org.crumbs.ui.analytics.Event
import timber.log.Timber
import java.util.concurrent.TimeUnit

class DemoApp : Application() {

    override fun onCreate() {
        super.onCreate()
        EmailRelayService.TEST_DOMAIN = "stage-relay.crumbs.org"
        EmailRelayService.TEST_API_DOMAIN = "api-${EmailRelayService.TEST_DOMAIN}"

        CrumbsLogger.logLevel = CrumbsLoggerLevel.DEBUG
        Timber.plant(Timber.DebugTree())
        //tag::history[]
        CrumbsAndroid.get().integration().setHistoryProvider(object : HistoryProvider {
            override fun queryBrowserHistory(
                days: Int,
                completionCallback: HistoryCallback
            ) {
                completionCallback.onHistoryReady(fakeHistory())
            }
        })
        //end::history[]

        //tag::eventListener[]
        CrumbsUI.get().addEventListener(object : CrumbsUI.EventListener {
            override fun onEvent(event: Event<*>) {
                Timber.d("Crumbs event: ${event.id}")
                when (event.id) {
                    Event.PrivacySettings.TOGGLE_FEATURE -> {
                        val isEnabled = event.value<Boolean>()
                        if (isEnabled) {
                            Timber.d("Privacy protection is enabled")
                        }
                    }
                }
            }
        })
        //end::eventListener[]

        // showcase sharing of interests when accessing www.crumbs.org/en/how-it-works
        CrumbsAndroid.get().interests().setFakeShareableProfile()
    }

    private fun fakeHistory(): ArrayList<Visit> {
        val visits = arrayListOf<Visit>()
        for (i in 0..5) {
            visits.add(
                Visit(
                    "https://github.com/dialogflow/dialogflow-android-client/issues/57",
                    TimeUnit.DAYS.toMillis(i.toLong())
                )
            )
        }
        //should only count 1
        for (i in 0..5) {
            visits.add(
                Visit(
                    "http://www.cotation-immobiliere.fr/aspx/cotations/CotationPriceSearchCriteria.aspx",
                    TimeUnit.MINUTES.toMillis(i.toLong())
                )
            )
        }
        return visits
    }

}