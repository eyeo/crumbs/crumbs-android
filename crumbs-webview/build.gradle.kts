import org.jetbrains.kotlin.incremental.cleanDirectoryContents

plugins {
    id("com.android.library")
    id("kotlin-android")
    id("maven-publish")
}

android {
    defaultConfig {
        namespace = "org.crumbs.webview"
        minSdk = libs.versions.android.webview.min.sdk.get().toInt()
    }
    testOptions {
        animationsDisabled = true
    }
}

tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile>().configureEach {
    kotlinOptions {
        freeCompilerArgs = listOf("-opt-in=io.ktor.util.InternalAPI")
    }
}

dependencies {
    api(libs.crumbs.core)

    implementation(libs.kotlin.stdlib)
    implementation(libs.kotlinx.coroutines.android)
    implementation(libs.androidx.core.ktx)
    implementation(libs.androidx.appcompat)
    implementation(libs.androidx.startup)
    implementation(libs.jsoup)
    implementation(libs.ktor.client.android)
    implementation(libs.ktor.client.encoding)
    implementation(libs.bundles.adblock)

    testImplementation(libs.test.junit)

    androidTestImplementation(project(":crumbs-abp"))
    androidTestImplementation(project(":crumbs-test"))
    androidTestImplementation(libs.test.androidx.junit)
    androidTestImplementation(libs.test.androidx.rules)
    androidTestImplementation(libs.test.androidx.espresso.core)
    androidTestImplementation(libs.test.androidx.espresso.web)
}

val reportsDirectory = "$buildDir/reports/androidTests/connected"
val deviceScreenShotsFolder = "/sdcard/Download/screenshots_crumbs"
val screenshotsFolderName = "failShots"
val hostScreenshotsFolder = "$reportsDirectory/$screenshotsFolderName"

val cleanScreenshotsTask by tasks.registering(Exec::class) {
    description = "cleans the screenshots from the device"
    commandLine("adb", "shell", "rm", "-r", deviceScreenShotsFolder)
    isIgnoreExitValue = true
}

val pullScreenshotsTask by tasks.registering {
    description =
        "copies screenshots from the device to the host"
    doLast {
        File(hostScreenshotsFolder).cleanDirectoryContents()
        exec {
            commandLine("adb", "pull", deviceScreenShotsFolder)
            workingDir = File(hostScreenshotsFolder)
            isIgnoreExitValue = true
        }
    }
}

val embedScreenshotsTask by tasks.registering(EmbedScreenshotsTask::class) {
    description = "embeds failed tests screenshots in the JUnit HTML report"
    onlyIf { tasks.getByName("connectedDebugAndroidTest").state.failure != null }
    dependsOn(pullScreenshotsTask)
    inputDirectory = File(hostScreenshotsFolder)
}

afterEvaluate {
    tasks.getByName("connectedDebugAndroidTest") {
        dependsOn(cleanScreenshotsTask)
        finalizedBy(embedScreenshotsTask)
    }
}

extra["artifact_id"] = "crumbs-webview-android"
apply(file("../publish.gradle"))

