
-keepclassmembers class org.crumbs.webview.* {
    @android.webkit.JavascriptInterface <methods>;
}


# allow reflexion to insert webclient
-keep class org.adblockplus.libadblockplus.android.webview.AdblockWebView { *; }
-keep class org.adblockplus.libadblockplus.android.webview.AdblockWebView.AdblockWebViewClient { *; }