package org.crumbs.webview

import android.content.Intent
import androidx.lifecycle.Lifecycle
import androidx.test.core.app.ActivityScenario
import androidx.test.espresso.web.model.Atoms
import androidx.test.espresso.web.sugar.Web
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.MediumTest
import androidx.test.platform.app.InstrumentationRegistry
import org.crumbs.CrumbsProvider
import org.crumbs.models.FilterLevel
import org.crumbs.webview.WebTestUtils.retryTest
import org.junit.Assert
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestName
import org.junit.runner.RunWith

@MediumTest
@RunWith(AndroidJUnit4::class)
class CrumbsTestPages : CrumbsProvider {

    @get:Rule
    var name = TestName()

    @Test
    fun testFilteringCMP() = retryTest {
        requirePrivacy.editSettings()
            .enable(false)
            .hideCookieConsentPopups(true)
            .apply()
        Assert.assertEquals(TestStatus.Failed, runTest(TEST_CMP_URL))
        requirePrivacy.editSettings()
            .enable(true)
            .hideCookieConsentPopups(false)
            .apply()
        Assert.assertEquals(TestStatus.Failed, runTest(TEST_CMP_URL))
        requirePrivacy.editSettings()
            .enable(true)
            .hideCookieConsentPopups(true)
            .apply()
        Thread.sleep(5000)
        Assert.assertEquals(TestStatus.Passed, runTest(TEST_CMP_URL))
    }

    @Test
    fun testFilteringNetworkRequests() = retryTest {
        requirePrivacy.editSettings()
            .enable(false)
            .apply()
        Assert.assertEquals(TestStatus.Failed, runTest(TEST_NETWORK_REQUESTS_URL))
        requirePrivacy.editSettings()
            .enable(true)
            .apply()
        Assert.assertEquals(TestStatus.Passed, runTest(TEST_NETWORK_REQUESTS_URL))
    }

    @Test
    fun testFilteringUTM() = retryTest {
        requirePrivacy.editSettings()
            .enable(false)
            .removeMarketingTrackingParameters(true)
            .apply()
        Assert.assertEquals(TestStatus.Failed, runTest(TEST_UTM_URL))
        requirePrivacy.editSettings()
            .enable(true)
            .removeMarketingTrackingParameters(false)
            .apply()
        Assert.assertEquals(TestStatus.Failed, runTest(TEST_UTM_URL))
        requirePrivacy.editSettings()
            .enable(true)
            .removeMarketingTrackingParameters(true)
            .apply()
        Assert.assertEquals(TestStatus.Passed, runTest(TEST_UTM_URL))
    }

    @Test
    fun testDomReferrer() = retryTest {
        requirePrivacy.editSettings()
            .enable(false)
            .hideReferrerHeader(true)
            .apply()
        Assert.assertEquals(TestStatus.Failed, runTest(TEST_REFERRER_URL))
        requirePrivacy.editSettings()
            .enable(true)
            .hideReferrerHeader(false)
            .apply()
        Assert.assertEquals(TestStatus.Failed, runTest(TEST_REFERRER_URL))
        requirePrivacy.editSettings()
            .enable(true)
            .hideReferrerHeader(true)
            .apply()
        Assert.assertEquals(TestStatus.Passed, runTest(TEST_REFERRER_URL))
    }

    @Test
    fun testDomDNT() = retryTest {
        requirePrivacy.editSettings()
            .enable(false)
            .enableDoNotTrack(true)
            .apply()
        Assert.assertEquals(TestStatus.Failed, runTest(TEST_DNT_URL))
        requirePrivacy.editSettings()
            .enable(true)
            .enableDoNotTrack(false)
            .apply()
        Assert.assertEquals(TestStatus.Failed, runTest(TEST_DNT_URL))
        requirePrivacy.editSettings()
            .enable(true)
            .enableDoNotTrack(true)
            .apply()
        Assert.assertEquals(TestStatus.Passed, runTest(TEST_DNT_URL))
    }

    @Test
    fun testDomGPC() = retryTest {
        requirePrivacy.editSettings()
            .enable(false)
            .enableGPC(true)
            .apply()
        Assert.assertEquals(TestStatus.Failed, runTest(TEST_GPC_URL))
        requirePrivacy.editSettings()
            .enable(true)
            .enableGPC(false)
            .apply()
        Assert.assertEquals(TestStatus.Failed, runTest(TEST_GPC_URL))
        requirePrivacy.editSettings()
            .enable(true)
            .enableGPC(true)
            .apply()
        Assert.assertEquals(TestStatus.Passed, runTest(TEST_GPC_URL))
    }

    @Test
    fun testSetHeaders() = retryTest {
        requirePrivacy.editSettings()
            .enable(false)
            .enableDoNotTrack(true)
            .enableGPC(true)
            .apply()
        Assert.assertEquals(TestStatus.Failed, runTest(TEST_SET_HEADERS_URL))
        requirePrivacy.editSettings()
            .enable(true)
            .enableDoNotTrack(false)
            .enableGPC(false)
            .apply()
        Assert.assertEquals(TestStatus.Failed, runTest(TEST_SET_HEADERS_URL))
        requirePrivacy.editSettings()
            .enable(true)
            .enableDoNotTrack(true)
            .enableGPC(true)
            .apply()
        Assert.assertEquals(TestStatus.Passed, runTest(TEST_SET_HEADERS_URL))
    }

    @Test
    fun testRemoveHeaders() = retryTest {
        requirePrivacy.editSettings()
            .enable(true)
            .enableFingerprintShield(true)
            .apply()
        Assert.assertEquals(TestStatus.Passed, runTest(TEST_REMOVED_HEADERS_URL))
    }

    @Test
    fun testFingerprintingNavigator() = retryTest {
        requirePrivacy.editSettings()
            .enable(false)
            .enableFingerprintShield(true)
            .apply()
        Assert.assertEquals(TestStatus.Failed, runTest(TEST_FINGERPRINTING_NAVIGATOR_URL))
        requirePrivacy.editSettings()
            .enable(true)
            .enableFingerprintShield(false)
            .apply()
        Assert.assertEquals(TestStatus.Failed, runTest(TEST_FINGERPRINTING_NAVIGATOR_URL))
        requirePrivacy.editSettings()
            .enable(true)
            .enableFingerprintShield(true)
            .apply()
        Assert.assertEquals(TestStatus.Passed, runTest(TEST_FINGERPRINTING_NAVIGATOR_URL))
    }

    @Test
    fun testCookieSandboxing() = retryTest {
        requirePrivacy.editSettings()
            .enable(false)
            .cookiesFilterLevel(FilterLevel.SANDBOX)
            .apply()
        Assert.assertEquals(TestStatus.Failed, runTest(TEST_SANDBOXING_URL))
        requirePrivacy.editSettings()
            .enable(true)
            .cookiesFilterLevel(FilterLevel.ALL)
            .apply()
        Assert.assertEquals(TestStatus.Failed, runTest(TEST_SANDBOXING_URL))
        requirePrivacy.editSettings()
            .enable(true)
            .cookiesFilterLevel(FilterLevel.SANDBOX)
            .apply()
        Assert.assertEquals(TestStatus.Passed, runTest(TEST_SANDBOXING_URL))
    }

    private fun runTest(
        url: String
    ): TestStatus {
        val intent =
            Intent(InstrumentationRegistry.getInstrumentation().context, TestActivity::class.java)
        intent.putExtra(TestActivity.ARG_URL, url)
        intent.putExtra(TestActivity.ARG_ADBLOCK, true)

        val scenario = ActivityScenario.launch<TestActivity>(intent)
        try {
            scenario.moveToState(Lifecycle.State.RESUMED)
            InstrumentationRegistry.getInstrumentation().waitForIdleSync()
            var retry = 0
            while (retry < 15) {
                Thread.sleep(1000)
                val testResult = getTestResult()
                if (testResult == TestStatus.InProgress) {
                    retry++
                } else {
                    return testResult
                }
            }
            throw IllegalStateException("Can't get test result: TIMEOUT")
        } catch (e: Throwable) {
            captureScreenshot(name.methodName, scenario)
            throw e
        } finally {
            scenario.moveToState(Lifecycle.State.DESTROYED)
        }
    }


    private fun captureScreenshot(
        methodName: String,
        activityScenario: ActivityScenario<TestActivity>,
    ) {
        activityScenario.onActivity {
            org.crumbs.test.captureScreenshot(
                methodName,
                childFolder = SCREENSHOTS_FOLDER
            )
        }
        Thread.sleep(3000)
    }

    private fun getTestResult(): TestStatus {
        InstrumentationRegistry.getInstrumentation().waitForIdleSync()
        val result = Web.onWebView()
            .perform(
                Atoms.script(
                    "return Array.from(document.getElementsByClassName(\"$TEST_RESULT_CLASS\")).map(value => value.getAttribute(\"data-result\"))",
                    Atoms.castOrDie(List::class.java)
                )
            ).get()
        val status = result.map { TestStatus.valueOf(it as String) }
        if (status.contains(TestStatus.Failed)) return TestStatus.Failed
        if (status.isEmpty() || status.contains(TestStatus.InProgress)) return TestStatus.InProgress
        return TestStatus.Passed
    }

    enum class TestStatus {
        InProgress,
        Passed,
        Failed
    }

    companion object {

        private val SCREENSHOTS_FOLDER =
            "screenshots_crumbs/${CrumbsTestPages::class.java.name}"

        private const val TEST_RESULT_CLASS = "TestResult"
        private const val TEST_PAGE_URL = "https://test-pages.crumbs.org"
        private const val TEST_CMP_URL = "$TEST_PAGE_URL/filtering/cmp"
        private const val TEST_NETWORK_REQUESTS_URL = "$TEST_PAGE_URL/filtering/network-requests"
        private const val TEST_UTM_URL = "$TEST_PAGE_URL/filtering/utm?autoStart"
        private const val TEST_REFERRER_URL = "$TEST_PAGE_URL/dom/referrer?autoStart"
        private const val TEST_DNT_URL = "$TEST_PAGE_URL/dom/dnt"
        private const val TEST_GPC_URL = "$TEST_PAGE_URL/dom/gpc"
        private const val TEST_SET_HEADERS_URL = "$TEST_PAGE_URL/headers/set-headers"
        private const val TEST_REMOVED_HEADERS_URL = "$TEST_PAGE_URL/headers/removed-headers"
        private const val TEST_FINGERPRINTING_NAVIGATOR_URL =
            "$TEST_PAGE_URL/fingerprinting/navigator"
        private const val TEST_SANDBOXING_URL = "$TEST_PAGE_URL/cookies/sandboxing?step=1"

        private const val TEST_PROXY_URL = "$TEST_PAGE_URL/geo/proxy"
    }
}