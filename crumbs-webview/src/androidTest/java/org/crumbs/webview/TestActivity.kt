package org.crumbs.webview

import android.app.Activity
import android.os.Bundle
import android.webkit.CookieManager
import android.webkit.WebSettings
import android.webkit.WebView
import org.adblockplus.libadblockplus.android.AdblockEngine
import org.adblockplus.libadblockplus.android.AndroidBase64Processor
import org.adblockplus.libadblockplus.android.AndroidHttpClient
import org.adblockplus.libadblockplus.android.SingleInstanceEngineProvider
import org.adblockplus.libadblockplus.android.webview.AdblockWebView
import org.adblockplus.libadblockplus.security.JavaSignatureVerifier
import org.adblockplus.libadblockplus.security.SignatureVerifier
import org.adblockplus.libadblockplus.sitekey.PublicKeyHolder
import org.adblockplus.libadblockplus.sitekey.PublicKeyHolderImpl
import org.adblockplus.libadblockplus.sitekey.SiteKeyVerifier
import org.adblockplus.libadblockplus.sitekey.SiteKeysConfiguration
import org.adblockplus.libadblockplus.util.Base64Processor
import org.crumbs.CrumbsLogger
import org.crumbs.CrumbsLoggerLevel
import org.crumbs.abp.ABPListener

class TestActivity : Activity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        CrumbsLogger.logLevel = CrumbsLoggerLevel.DEBUG

        val useAdblockWebView = intent.getBooleanExtra(ARG_ADBLOCK, true)
        val testUrl = intent.getStringExtra(ARG_URL) ?: "https://crumbs-benchmark.web.app"

        val webView = if (useAdblockWebView) {
            AdblockWebView(this).apply {
                siteKeysConfiguration = this@TestActivity.getSiteKeysConfiguration()
                val listener = ABPListener()
                val engineProvider = SingleInstanceEngineProvider(
                    AdblockEngine.builder(this@TestActivity, "adblock")
                )
                engineProvider.addEngineCreatedListener {
                    listener.setupEngine(it as AdblockEngine)
                }
                setProvider(engineProvider)
                listener.setupEventListener(this)
            }

        } else {
            WebView(this)
        }

        WebView.setWebContentsDebuggingEnabled(true)
        CookieManager.getInstance().apply {
            setAcceptCookie(true)
            setAcceptThirdPartyCookies(webView, true)
            removeAllCookies { }
        }
        applicationContext.cacheDir.listFiles()?.forEach {
            it.deleteRecursively()
        }
        val crumbsWebViewClient = CrumbsWebViewClient()
        webView.webViewClient = crumbsWebViewClient
        crumbsWebViewClient.setupWebView(webView, false)
        webView.settings.apply {
            databaseEnabled = true
            domStorageEnabled = true
            mediaPlaybackRequiresUserGesture = true
            cacheMode = WebSettings.LOAD_NO_CACHE
            javaScriptEnabled = true
        }
        webView.keepScreenOn = true
        setContentView(webView)
        webView.loadDataWithBaseURL("", "<HTML><BODY><H3>Test</H3></BODY></HTML>","text/html","utf-8","")
        webView.postDelayed({
            webView.stopLoading()
            webView.loadUrl(testUrl)
        }, 500)

    }

    private fun getSiteKeysConfiguration(): SiteKeysConfiguration {
        val signatureVerifier: SignatureVerifier = JavaSignatureVerifier()
        val publicKeyHolder: PublicKeyHolder = PublicKeyHolderImpl()
        val httpClient = AndroidHttpClient(true)
        val base64Processor: Base64Processor = AndroidBase64Processor()
        val siteKeyVerifier =
            SiteKeyVerifier(signatureVerifier, publicKeyHolder, base64Processor)
        return SiteKeysConfiguration(
            signatureVerifier, publicKeyHolder, httpClient, siteKeyVerifier
        )
    }

    companion object {
        const val ARG_ADBLOCK = "UseAdblock"
        const val ARG_URL = "URL"
    }
}