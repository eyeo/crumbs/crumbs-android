package org.crumbs.webview

import android.content.Context
import androidx.startup.Initializer
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.distinctUntilChanged
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import org.crumbs.CrumbsAndroid
import org.crumbs.CrumbsCore
import org.crumbs.webview.network.CrumbsWebViewHttpClient

class CrumbsInitializer : Initializer<CrumbsCore> {

    override fun create(context: Context): CrumbsCore {
        setup(context)
        return CrumbsAndroid.get()
    }

    override fun dependencies(): List<Class<out Initializer<*>>> {
        // No dependencies on other libraries.
        return emptyList()
    }

    companion object {

        internal lateinit var httpClient: CrumbsWebViewHttpClient

        internal fun setup(context: Context) {
            if (!CrumbsAndroid.isInitialized()) {
                //tag::initialize[]
                CrumbsAndroid.setup(context)
                //end::initialize[]
            }

            httpClient = CrumbsWebViewHttpClient()
            MainScope().launch {
                CrumbsAndroid.get().privacy().listenSettings()
                    .map { it.adsProxyUrl }
                    .distinctUntilChanged()
                    .collect {
                        httpClient.setProxy(it)
                    }
            }
        }
    }
}