package org.crumbs.webview

import android.content.Context
import android.webkit.JavascriptInterface
import android.webkit.WebResourceResponse
import android.webkit.WebView
import org.crumbs.CrumbsAndroid
import org.crumbs.CrumbsLogger
import org.crumbs.CrumbsProvider
import org.crumbs.models.RequestType
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import org.jsoup.select.Elements
import java.io.ByteArrayInputStream
import java.io.InputStream
import java.nio.charset.Charset
import java.util.*

class CrumbsJsInterface internal constructor(
    private val webView: WebView,
    private val webViewClient: CrumbsWebViewClient,
    private val incognito: Boolean
) : CrumbsProvider {

    @JavascriptInterface
    fun onInteract() {
        webView.post {
            webView.url?.let {
                integration?.onInteract(it, incognito)
            }
        }
    }

    @JavascriptInterface
    fun isAudioPlaying(isPlaying: Boolean) {
        webView.post {
            webView.url?.let {
                integration?.onAudioChange(it, incognito, isPlaying)
            }
        }
    }

    @JavascriptInterface
    fun interceptBody(requestId: String, body: String?) {
        body?.let {
            webViewClient.addRequestBody(requestId, body)
        }
    }

    @JavascriptInterface
    fun iFrameIsLoading(url: String) {
        webViewClient.iFrameIsLoading(webView, url)
    }

    companion object {

        private var crumbsLogger = CrumbsLogger("CrumbsJsInterface")
        private var interceptHeader: String? = null
        internal const val NAME = "CrumbsInterface"
        internal const val URL_SEPARATOR = "CRUMBS_REQUEST_ID"

        fun injectJsHeader(
            tabId: String,
            documentUrl: String,
            requestType: RequestType,
            response: WebResourceResponse,
            context: Context
        ): WebResourceResponse {
            val encoding = response.encoding
            val mime = response.mimeType.lowercase(Locale.ROOT)
            val responseData: InputStream = response.data
            val injectedResponseData: InputStream = injectInterceptToStream(
                context,
                tabId,
                documentUrl,
                requestType,
                responseData,
                mime,
                Charset.forName(response.encoding ?: Charsets.UTF_8.toString())
            )
            return WebResourceResponse(mime, encoding, injectedResponseData).apply {
                responseHeaders = response.responseHeaders
            }
        }

        private fun injectInterceptToStream(
            context: Context,
            tabId: String,
            documentUrl: String,
            requestType: RequestType,
            input: InputStream,
            mime: String,
            charset: Charset
        ): InputStream {
            return try {
                var pageContents: ByteArray = input.readBytes()
                if (mime == "text/html") {
                    pageContents =
                        injectHeaderInHtml(
                            context,
                            tabId,
                            documentUrl,
                            requestType,
                            String(pageContents, charset)
                        )
                            .toByteArray(charset)
                }
                ByteArrayInputStream(pageContents)
            } catch (e: Exception) {
                crumbsLogger.w("Can't inject JS header", e)
                input
            }
        }

        fun injectHeaderInHtml(
            context: Context,
            tabId: String,
            documentUrl: String,
            requestType: RequestType,
            data: String
        ): String {
            if (interceptHeader == null) {
                interceptHeader = String(
                    context.assets.open("CrumbsJsInterfaceHeader.html").readBytes()
                )
            }
            if (data.indexOf("<head>") < 0) return data
            val doc: Document = Jsoup.parse(data)
            doc.outputSettings().prettyPrint(true)
            val element: Elements = doc.getElementsByTag("head")
            if (element.isNotEmpty()) {
                element[0].prepend(interceptHeader!!)
                val snippet =
                    CrumbsAndroid.get().integration()
                        .createJavaScriptSnippet(tabId, documentUrl, requestType)
                if (snippet.isNotEmpty()) {
                    element[0].prepend("""<script language="JavaScript">$snippet</script>""")
                }
            }
            return doc.toString()
        }
    }
}