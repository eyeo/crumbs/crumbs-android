package org.crumbs.webview

import android.graphics.Bitmap
import android.net.Uri
import android.webkit.WebResourceRequest
import android.webkit.WebResourceResponse
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.core.net.toUri
import io.ktor.http.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.runBlocking
import org.crumbs.CrumbsLogger
import org.crumbs.CrumbsProvider
import org.crumbs.javascript.CrumbsCoreJsInterface
import org.crumbs.models.RequestType
import org.crumbs.service.DomainService
import org.crumbs.utils.UIExtension.getTabId
import org.crumbs.utils.UrlExtension.toUrlBuilder
import org.crumbs.webview.network.CrumbsWebViewHttpClient
import java.net.CookieHandler
import java.util.*
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

open class CrumbsWebViewClient() : WebViewClientWrapper(), CrumbsProvider {

    private var adblockClient: WebViewClient? = null
    private val httpClient = CrumbsInitializer.httpClient
    private var tabListener: TabListener? = null
    private val url2Referrer = hashMapOf<String, HashMap<String, String>>()
    private val requestData = hashMapOf<String, String>()

    private fun getReferrerMap(tabId: String): HashMap<String, String> {
        return url2Referrer.getOrPut(tabId, defaultValue = { hashMapOf() })
    }

    @SuppressWarnings("unused")
    constructor(nestedWebViewClient: WebViewClient) : this() {
        this.setWebViewClient(nestedWebViewClient)
    }

    fun setupWebView(webView: WebView, incognito: Boolean) {
        webView.removeJavascriptInterface(CrumbsJsInterface.NAME)
        webView.addJavascriptInterface(
            CrumbsJsInterface(webView, this, incognito),
            CrumbsJsInterface.NAME
        )
        webView.removeJavascriptInterface(CrumbsCoreJsInterface.NAME)
        webView.addJavascriptInterface(
            CrumbsCoreJsInterface(),
            CrumbsCoreJsInterface.NAME
        )
        webView.setTag(R.id.crumbs_tag_incognito, incognito)

        //TODO remove me when AdblockWebView allow client reordering
        if (webView.javaClass.simpleName == "AdblockWebView") {
            val field = webView::class.java.getDeclaredField("intWebViewClient")
            field.isAccessible = true
            @Suppress("UNCHECKED_CAST")
            adblockClient = field.get(webView) as WebViewClient
        }
    }

    fun setTabListener(listener: TabListener?) {
        this.tabListener = listener
    }

    fun addRequestBody(requestId: String, data: String) {
        this.requestData[requestId] = data
    }


    fun iFrameIsLoading(webView: WebView, url: String) {
        @Suppress("UNCHECKED_CAST")
        val iFrameUrls: ArrayList<String> =
            webView.getTag(R.id.crumbs_tag_iframe_urls) as? ArrayList<String> ?: arrayListOf()
        iFrameUrls.add(url)
        webView.setTag(R.id.crumbs_tag_iframe_urls, iFrameUrls)
    }


    override fun onPageStarted(view: WebView, url: String, favicon: Bitmap?) {
        super.onPageStarted(view, url, favicon)
        val tabId = view.getTabId()
        integration?.onTabStartLoading(tabId)
        getReferrerMap(tabId).clear()
        tabListener?.onActiveTabUrl(tabId, url)
    }

    override fun shouldInterceptRequest(
        view: WebView,
        request: WebResourceRequest
    ): WebResourceResponse? {

        if (request.url.scheme == "data") {
            return null
        }
        val crumbsRequest = CrumbsRequest(view, request)

        if (crumbsRequest.isForMainFrame && crumbsRequest.requestUrl != crumbsRequest.baseRequestUrl) {
            view.post {
                view.loadUrl(crumbsRequest.requestUrl)
            }
            return null
        }

        if (crumbsRequest.isBlockedByCrumbs()) {
            return CrumbsWebViewHttpClient.createBlockRequest()
        }

        runBlocking(Dispatchers.Unconfined) {
            try {
                crumbsRequest.checkAlias()
            } catch (e: Throwable) {
                logger.w("Can't get host alias", e)
            }
        }


        if (crumbsRequest.hasAlias()) {
            if (isRequestBlockedByFilters(crumbsRequest.toAliasRequest())) {
                return CrumbsWebViewHttpClient.createBlockRequest()
            }
        }

        return super.shouldInterceptRequest(view, crumbsRequest) ?: try {
            //TODO fix UnsupportedOperationException crash with org.adblockplus.libadblockplus.android.webview.SharedCookieManager.get(SharedCookieManager.java:120)
            CookieHandler.setDefault(defaultHandler)
            httpClient.performRequest(view, crumbsRequest)
        } catch (e: Exception) {
            logger.w("Can't intercept request: ${crumbsRequest.baseRequestUrl}", e)
            null
        }
    }

    override fun doUpdateVisitedHistory(view: WebView, url: String, isReload: Boolean) {
        super.doUpdateVisitedHistory(view, url, isReload)
        integration?.onVisitedUrl(url, view.getTag(R.id.crumbs_tag_incognito) == true)
    }

    //TODO remove me when AdblockWebView allow client reordering
    private fun isRequestBlockedByFilters(request: WebResourceRequest): Boolean {
        return adblockClient?.let {
            val method =
                it.javaClass.getDeclaredMethod(
                    "shouldAbpBlockRequest",
                    WebResourceRequest::class.java
                )
            method.isAccessible = true
            val invoke = method.invoke(it, request)
            invoke?.toString() == "BLOCK_LOAD"
        } ?: false
    }

    companion object {
        private val logger = CrumbsLogger("CrumbsWebViewClient")

        private val defaultHandler = CookieHandler.getDefault()
    }

    interface TabListener {
        fun onActiveTabUrl(tabId: String, url: String)
    }

    inner class AliasRequest(private val request: CrumbsRequest) : WebResourceRequest by request {

        override fun getUrl(): Uri {
            return request.aliasUrl?.toUri() ?: request.url
        }

    }

    open inner class CrumbsRequest(webView: WebView, request: WebResourceRequest) :
        WebResourceRequest by request {


        private var iFrameUrls: ArrayList<*>?
        val tabId = webView.getTabId()
        var documentUrl: String
        var baseRequestUrl: String
        var requestUrl: String
        var body: String?
        var aliasUrl: String? = null
        val referrerChain = arrayListOf<String>()
        val rewriteHeaders = request.requestHeaders.toList().toMutableList()
        var rewriteMethod = request.method
        var isIncognito: Boolean


        init {
            baseRequestUrl = request.url.toString().encodeURLQueryComponent()
            var requestId = baseRequestUrl

            if (baseRequestUrl.contains(CrumbsJsInterface.URL_SEPARATOR)) {
                baseRequestUrl.split(CrumbsJsInterface.URL_SEPARATOR).let {
                    baseRequestUrl = it.first()
                    requestId = it.last()
                }
            }
            if (request.isForMainFrame) {
                webView.setTag(R.id.crumbs_tag_url, baseRequestUrl)
                webView.setTag(R.id.crumbs_tag_iframe_urls, arrayListOf<String>())
            }

            documentUrl = webView.getTag(R.id.crumbs_tag_url) as? String ?: baseRequestUrl

            request.requestHeaders["Referer"]?.takeIf { baseRequestUrl != it }?.let {
                getReferrerMap(tabId)[baseRequestUrl] = it
            }

            var parent: String = baseRequestUrl
            while (getReferrerMap(tabId)[parent]?.also { parent = it } != null) {
                if (referrerChain.contains(parent)) {
                    // Detected referrer loop, finished creating referrers list
                    break
                }
                referrerChain.add(0, parent)
            }
            this.requestUrl =
                requireIntegration.rewriteRequestUrl(documentUrl, baseRequestUrl)


            body = when (method) {
                "DELETE", "PATCH", "POST", "PUT" -> {
                    requestData.remove(requestId)
                }
                else -> null
            }

            iFrameUrls = webView.getTag(R.id.crumbs_tag_iframe_urls) as? ArrayList<*>
            isIncognito = webView.getTag(R.id.crumbs_tag_incognito) == true

        }

        fun isBlockedByCrumbs(): Boolean {
            return requireIntegration.shouldBlockRequest(
                tabId,
                documentUrl,
                requestUrl,
                getRequestType(),
                requestHeaders.toList()
            )
        }

        suspend fun checkAlias() {
            val hostAlias = suspendCoroutine<String?> { continuation ->
                requireIntegration.getHostAlias(
                    documentUrl,
                    requestUrl,
                    object : DomainService.DnsRequestListener {
                        override fun onAliasReceived(alias: String?) {
                            val value: String? = alias?.takeIf { it.isNotEmpty() }
                            continuation.resume(value)
                        }
                    })
            }

            if (hostAlias != null) {
                aliasUrl = requestUrl.toUrlBuilder()?.apply {
                    host = hostAlias
                }?.buildString()
            }

        }

        override fun getUrl(): Uri = requestUrl.toUri()

        override fun getMethod(): String = rewriteMethod

        override fun getRequestHeaders(): MutableMap<String, String> =
            rewriteHeaders.toMap().toMutableMap()

        fun getRequestType(): RequestType {
            // we don't store request type because iFrameIsLoading can be called after shouldInterceptRequest sometime
            return when {
                isForMainFrame -> {
                    RequestType.MAIN_FRAME
                }
                iFrameUrls?.contains(baseRequestUrl) == true -> {
                    RequestType.I_FRAME
                }
                else -> {
                    RequestType.RESOURCE
                }
            }
        }

        fun hasAlias() = aliasUrl != null

        fun toAliasRequest() = AliasRequest(this)

    }
}

