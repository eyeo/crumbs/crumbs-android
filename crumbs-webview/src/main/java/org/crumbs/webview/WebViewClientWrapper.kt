package org.crumbs.webview

import android.graphics.Bitmap
import android.net.http.SslError
import android.os.Build
import android.os.Message
import android.view.KeyEvent
import android.webkit.*
import androidx.annotation.RequiresApi

open class WebViewClientWrapper : WebViewClient() {

    private var nestedWebViewClient: WebViewClient? = null

    fun setWebViewClient(nestedWebViewClient: WebViewClient?) {
        this.nestedWebViewClient = nestedWebViewClient
    }

    @Suppress("deprecation")
    @Deprecated("")
    override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
        return nestedWebViewClient?.shouldOverrideUrlLoading(view, url)
            ?: super.shouldOverrideUrlLoading(view, url)
    }

    @RequiresApi(Build.VERSION_CODES.N)
    override fun shouldOverrideUrlLoading(view: WebView, request: WebResourceRequest): Boolean {
        return nestedWebViewClient?.shouldOverrideUrlLoading(view, request)
            ?: super.shouldOverrideUrlLoading(view, request)
    }

    override fun onPageStarted(view: WebView, url: String, favicon: Bitmap?) {
        nestedWebViewClient?.onPageStarted(view, url, favicon)
    }

    override fun onPageFinished(view: WebView, url: String) {
        nestedWebViewClient?.onPageFinished(view, url)
    }

    override fun onLoadResource(view: WebView, url: String) {
        nestedWebViewClient?.onLoadResource(view, url)
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onPageCommitVisible(view: WebView, url: String) {
        nestedWebViewClient?.onPageCommitVisible(view, url)
    }

    @Suppress("deprecation")
    @Deprecated(
        """Use {@link #shouldInterceptRequest(WebView, WebResourceRequest)
     *             shouldInterceptRequest(WebView, WebResourceRequest)} instead."""
    )
    override fun shouldInterceptRequest(
        view: WebView,
        url: String
    ): WebResourceResponse? {
        return nestedWebViewClient?.shouldInterceptRequest(view, url)
    }

    override fun shouldInterceptRequest(
        view: WebView,
        request: WebResourceRequest
    ): WebResourceResponse? {
        return nestedWebViewClient?.shouldInterceptRequest(view, request)
            ?: super.shouldInterceptRequest(view, request)
    }

    @Suppress("deprecation")
    @Deprecated(
        """This method is no longer called. When the WebView encounters
                  a redirect loop, it will cancel the load."""
    )
    override fun onTooManyRedirects(
        view: WebView?,
        cancelMsg: Message,
        continueMsg: Message?
    ) {
        nestedWebViewClient?.onTooManyRedirects(view, cancelMsg, continueMsg)
            ?: super.onTooManyRedirects(view, cancelMsg, continueMsg)
    }

    @Suppress("deprecation")
    @Deprecated(
        """Use {@link #onReceivedError(WebView, WebResourceRequest, WebResourceError)
     *             onReceivedError(WebView, WebResourceRequest, WebResourceError)} instead."""
    )
    override fun onReceivedError(
        view: WebView?,
        errorCode: Int,
        description: String?,
        failingUrl: String?
    ) {
        nestedWebViewClient?.onReceivedError(view, errorCode, description, failingUrl)
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onReceivedError(
        view: WebView?,
        request: WebResourceRequest,
        error: WebResourceError
    ) {
        super.onReceivedError(view, request, error)
        nestedWebViewClient?.onReceivedError(view, request, error)
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onReceivedHttpError(
        view: WebView?, request: WebResourceRequest?, errorResponse: WebResourceResponse?
    ) {
        nestedWebViewClient?.onReceivedHttpError(view, request, errorResponse)
    }

    override fun onFormResubmission(
        view: WebView?, dontResend: Message,
        resend: Message?
    ) {
        nestedWebViewClient?.onFormResubmission(view, dontResend, resend)
            ?: super.onFormResubmission(view, dontResend, resend)
    }

    override fun doUpdateVisitedHistory(
        view: WebView, url: String,
        isReload: Boolean
    ) {
        nestedWebViewClient?.doUpdateVisitedHistory(view, url, isReload)
    }

    override fun onReceivedSslError(
        view: WebView?, handler: SslErrorHandler,
        error: SslError?
    ) {
        nestedWebViewClient?.onReceivedSslError(view, handler, error) ?: super.onReceivedSslError(
            view,
            handler,
            error
        )
    }

    override fun onReceivedClientCertRequest(view: WebView?, request: ClientCertRequest) {
        nestedWebViewClient?.onReceivedClientCertRequest(view, request)
    }

    override fun onReceivedHttpAuthRequest(
        view: WebView?,
        handler: HttpAuthHandler, host: String?, realm: String?
    ) {
        nestedWebViewClient?.onReceivedHttpAuthRequest(view, handler, host, realm)
            ?: super.onReceivedHttpAuthRequest(view, handler, host, realm)
    }

    override fun shouldOverrideKeyEvent(view: WebView?, event: KeyEvent?): Boolean {
        return nestedWebViewClient?.shouldOverrideKeyEvent(view, event)
            ?: super.shouldOverrideKeyEvent(view, event)
    }

    override fun onUnhandledKeyEvent(view: WebView, event: KeyEvent) {
        nestedWebViewClient?.onUnhandledKeyEvent(view, event) ?: super.onUnhandledKeyEvent(
            view,
            event
        )
    }

    override fun onReceivedLoginRequest(
        view: WebView?, realm: String?,
        account: String?, args: String?
    ) {
        nestedWebViewClient?.onReceivedLoginRequest(view, realm, account, args)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onRenderProcessGone(view: WebView?, detail: RenderProcessGoneDetail?): Boolean {
        return nestedWebViewClient?.onRenderProcessGone(view, detail) ?: super.onRenderProcessGone(
            view,
            detail
        )
    }

    @RequiresApi(Build.VERSION_CODES.O_MR1)
    override fun onSafeBrowsingHit(
        view: WebView?, request: WebResourceRequest?,
        threatType: Int, callback: SafeBrowsingResponse
    ) {
        nestedWebViewClient?.onSafeBrowsingHit(view, request, threatType, callback)
            ?: super.onSafeBrowsingHit(view, request, threatType, callback)
    }
}