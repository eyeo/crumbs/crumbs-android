package org.crumbs.webview.network

import android.webkit.WebResourceResponse
import android.webkit.WebView
import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.engine.*
import io.ktor.client.engine.android.*
import io.ktor.client.plugins.compression.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.http.*
import io.ktor.http.content.*
import kotlinx.coroutines.*
import org.crumbs.CrumbsLogger
import org.crumbs.CrumbsProvider
import org.crumbs.models.RequestContentType
import org.crumbs.utils.ContentTypeDetector
import org.crumbs.utils.UrlExtension.toUrl
import org.crumbs.webview.CrumbsJsInterface
import org.crumbs.webview.CrumbsWebViewClient
import java.io.InputStream
import java.util.*

@Suppress("EXPERIMENTAL_API_USAGE")
class CrumbsWebViewHttpClient : CrumbsProvider {

    private val contentTypeDetector = ContentTypeDetector()

    private val client = HttpClient(Android) {
        setupDefaultConfig()
    }

    private val proxyClient = HttpClient(Android) {
        setupDefaultConfig()
    }

    internal fun setProxy(proxyUrl: String?) {
        proxyUrl?.toUrl()?.let {
            proxyClient.engineConfig.proxy = when (it.protocol) {
                URLProtocol.SOCKS -> ProxyBuilder.socks(it.host, it.port)
                URLProtocol.HTTP, URLProtocol.HTTPS -> ProxyBuilder.http(it)
                else -> null
            }
        }
    }

    private fun HttpClientConfig<AndroidEngineConfig>.setupDefaultConfig() {
        followRedirects = false
        expectSuccess = false
        install(ContentEncoding) {
            deflate(1.0F)
            gzip(0.9F)
        }
        install(HttpCrumbsInterceptor)
        engine {
            threadsCount = 8
            pipelining = true
        }
    }

    @OptIn(DelicateCoroutinesApi::class)
    fun performRequest(
        view: WebView,
        crumbsRequest: CrumbsWebViewClient.CrumbsRequest,
    ): WebResourceResponse? {
        val requestUrl = crumbsRequest.requestUrl
        logger.d("${crumbsRequest.method}:$requestUrl")
        if (requestUrl.startsWith("data:")) {
            return null
        }
        GlobalScope.run {
            return runBlocking {


                val (response, wasRedirect) =
                    performHttpRequest(crumbsRequest)
                val charset = response.charset() ?: Charsets.UTF_8
                val mineType = response.contentType()?.withoutParameters().toString()
                if (crumbsRequest.isForMainFrame && wasRedirect) {
                    val content = CrumbsJsInterface.injectHeaderInHtml(
                        view.context,
                        crumbsRequest.tabId,
                        crumbsRequest.documentUrl,
                        crumbsRequest.getRequestType(),
                        response.bodyAsText(charset)
                    )
                    view.post {
                        view.stopLoading()
                        view.loadDataWithBaseURL(
                            response.request.url.toString(), content,
                            mineType, charset.name(), requestUrl
                        )
                    }
                    createBlockRequest()
                } else {

                    WebResourceResponse(
                        mineType,
                        charset.name(),
                        response.body<InputStream>()
                    ).apply {
                        try {
                            setStatusCodeAndReasonPhrase(
                                response.status.value,
                                response.status.description
                            )
                        } catch (e: IllegalArgumentException) {
                            logger.w("Can't set status code ${response.status.value}", e)
                            return@runBlocking null
                        }
                        val mapValues =
                            response.request.attributes[HttpCrumbsInterceptor.crumbsHeaders]
                                .mapValues { it.value.joinToString("; ") }
                        responseHeaders = mapValues
                    }.let {
                        if (it.mimeType == "text/html") {
                            CrumbsJsInterface.injectJsHeader(
                                crumbsRequest.tabId,
                                crumbsRequest.documentUrl,
                                crumbsRequest.getRequestType(),
                                it,
                                view.context
                            )
                        } else {
                            it
                        }
                    }
                }
            }
        }
    }

    private suspend fun performHttpRequest(
        crumbsRequest: CrumbsWebViewClient.CrumbsRequest,
        redirect: Int = 0
    ): Pair<HttpResponse, Boolean> {
        return withContext(Dispatchers.IO) {
            val headers = crumbsRequest.rewriteHeaders
            if (headers.find { it.first == "Referer" } == null && crumbsRequest.referrerChain.isNotEmpty()) {
                headers.add("Referer" to crumbsRequest.referrerChain.first())
            }

            if (headers.find { it.first == "Accept-Encoding" } == null) {
                headers.add("Accept-Encoding" to if (crumbsRequest.isForMainFrame) "gzip" else "gzip, deflate")
            }

            if (headers.find { it.first == "Sec-Fetch-Mode" } == null) {
                headers.add("Sec-Fetch-Mode" to if (crumbsRequest.isForMainFrame) "navigation" else "no-cors")
            }

            //https://developer.mozilla.org/en-US/docs/Web/API/RequestDestination
            val requestUrl = crumbsRequest.requestUrl
            if (headers.find { it.first == "Sec-Fetch-Dest" } == null) {
                when (contentTypeDetector.detect(requestUrl)) {
                    RequestContentType.SCRIPT -> "script"
                    RequestContentType.IMAGE -> "image"
                    RequestContentType.STYLESHEET -> "style"
                    RequestContentType.OBJECT -> "object"
                    RequestContentType.SUBDOCUMENT -> "embed"
                    RequestContentType.DOCUMENT -> "document"
                    RequestContentType.MEDIA -> "video"
                    RequestContentType.FONT -> "font"
                    else -> null
                }?.let {
                    headers.add("Sec-Fetch-Dest" to it)
                }
            }

            if (headers.find { it.first == "Sec-Fetch-Site" } == null) {
                val docUrl = crumbsRequest.documentUrl.toUrl()
                val reqUrl = requestUrl.toUrl()
                headers.add(
                    "Sec-Fetch-Site" to
                            when {
                                docUrl?.host == reqUrl?.host -> {
                                    "same-origin"
                                }
                                integration?.isThirdPartyRequest(
                                    crumbsRequest.documentUrl,
                                    requestUrl
                                ) == false -> {
                                    "same-site"
                                }
                                else -> {
                                    "cross-origin"
                                }
                            }
                )
            }

            val requestClient =
                if (integration?.getRequestProxy(
                        crumbsRequest.documentUrl,
                        requestUrl
                    ) != null
                ) proxyClient else client
            val response = requestClient.request(requestUrl) {
                attributes.put(HttpCrumbsInterceptor.crumbsRequest, crumbsRequest)
                this.method = HttpMethod.parse(crumbsRequest.method)
                var contentType: ContentType = ContentType.Any
                headers.forEach {
                    if (it.first.lowercase(Locale.getDefault()) != HttpHeaders.ContentType.lowercase(
                            Locale.getDefault()
                        )
                    ) {
                        this.headers.append(it.first, it.second)
                    } else {
                        contentType = ContentType.parse(it.second)
                    }
                }
                crumbsRequest.body?.let { body ->
                    this.body = TextContent(body, contentType = contentType)
                }
            }

            logger.d("response $requestUrl = ${response.status.value}")

            if (redirect < 20 && response.status.value in 300..310 && response.headers.contains(
                    "Location"
                )
            ) {
                val location = response.headers["Location"]!!.let { location ->
                    requestUrl.toUrl()?.let { url ->
                        when {
                            location.startsWith("/") -> {
                                url.protocol.name + "://" + url.authority + location
                            }
                            location.startsWith(url.protocol.name) -> {
                                location
                            }
                            else -> { // relative path
                                url.protocol.name + "://" + url.authority + url.encodedPath + "/" + location
                            }
                        }
                    } ?: location
                }

                logger.d("redirect $requestUrl to $location")
                headers.removeAll { it.first.lowercase(Locale.getDefault()) == "referer" }
                headers.add("Referer" to requestUrl)

                crumbsRequest.body =
                    if (response.status == HttpStatusCode.TemporaryRedirect) crumbsRequest.body else null
                crumbsRequest.rewriteMethod =
                    if (response.status == HttpStatusCode.TemporaryRedirect) crumbsRequest.method else "GET"

                crumbsRequest.requestUrl = location
                performHttpRequest(
                    crumbsRequest,
                    redirect + 1
                )
            } else {
                Pair(response, redirect > 0)
            }
        }
    }

    companion object {
        private val logger = CrumbsLogger("CrumbsHttpClient")

        fun createBlockRequest() = WebResourceResponse(
            "text/plain",
            Charsets.UTF_8.toString(),
            null
        ).apply {
            responseHeaders = emptyMap()
        }
    }

}