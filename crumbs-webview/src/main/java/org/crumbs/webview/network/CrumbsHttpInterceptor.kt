package org.crumbs.webview.network

import android.webkit.CookieManager
import io.ktor.client.*
import io.ktor.client.plugins.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.util.*
import io.ktor.util.pipeline.*
import org.crumbs.CrumbsLogger
import org.crumbs.CrumbsProvider
import org.crumbs.webview.CrumbsWebViewClient
import java.util.*

class HttpCrumbsInterceptor : CrumbsProvider {

    private val logger = CrumbsLogger("HttpCrumbsInterceptor")

    fun interceptRequestBlock(): suspend PipelineContext<Any, HttpRequestBuilder>.(Any) -> Unit = {
        crumbs?.apply {
            val crumbsRequest = context.attributes[crumbsRequest]

            val headers =
                context.headers.entries().flatMap { it.value.map { v -> it.key to v } }
                    .toMutableList()

            CookieManager.getInstance().getCookie(
                crumbsRequest.requestUrl
            )?.takeIf { it.isNotBlank() }?.let {
                headers.add(Pair("Cookie", it))
            }

            val filterHeaders =
                integration().filterSendHeaders(
                    crumbsRequest.tabId,
                    crumbsRequest.documentUrl,
                    crumbsRequest.requestUrl,
                    crumbsRequest.getRequestType(),
                    crumbsRequest.isIncognito,
                    headers
                )
            context.headers {
                clear()
                filterHeaders.onEach {
                    append(it.first, it.second)
                }
            }
        }
    }

    fun interceptResponseBlock(): suspend PipelineContext<HttpResponse, Unit>.(HttpResponse) -> Unit =
        { response ->
            crumbs?.apply {
                val crumbsRequest = response.request.attributes[crumbsRequest]
                val headers =
                    response.headers.entries().flatMap { it.value.map { v -> it.key to v } }
                        .toMutableList()
                val filterHeaders =
                    integration().filterReceivedHeaders(
                        crumbsRequest.tabId,
                        crumbsRequest.documentUrl,
                        crumbsRequest.requestUrl,
                        crumbsRequest.getRequestType(),
                        crumbsRequest.isIncognito,
                        headers
                    )
                filterHeaders.filter { it.first.lowercase(Locale.getDefault()) == "set-cookie" }
                    .forEach { setCookie ->
                        logger.d("Set new cookie: ${setCookie.second}")
                        CookieManager.getInstance()
                            .setCookie(crumbsRequest.requestUrl, setCookie.second)
                        CookieManager.getInstance().flush()
                    }

                val headersValues = filterHeaders.groupBy({ it.first }, { it.second })
                response.request.attributes.put(
                    crumbsHeaders,
                    headersValues
                )
            }
        }

    class Config {
        internal fun build(): HttpCrumbsInterceptor = HttpCrumbsInterceptor()
    }

    companion object : HttpClientPlugin<Config, HttpCrumbsInterceptor> {
        override fun prepare(block: Config.() -> Unit): HttpCrumbsInterceptor =
            Config().apply(block).build()

        override val key: AttributeKey<HttpCrumbsInterceptor> = AttributeKey("HttpCrumbs")

        override fun install(plugin: HttpCrumbsInterceptor, scope: HttpClient) {
            scope.sendPipeline.intercept(HttpSendPipeline.Before, plugin.interceptRequestBlock())
            scope.receivePipeline.intercept(
                HttpReceivePipeline.After,
                plugin.interceptResponseBlock()
            )
        }

        val crumbsRequest = AttributeKey<CrumbsWebViewClient.CrumbsRequest>("crumbsRequest")
        val crumbsHeaders = AttributeKey<Map<String, List<String>>>("crumbsHeaders")
    }

}
