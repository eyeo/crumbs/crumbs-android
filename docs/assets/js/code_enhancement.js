(function () {

    function copyToClipboard(text) {
        const textarea = document.createElement('textarea');
        textarea.id = 'temp_element'
        textarea.style.height = 0;
        document.body.appendChild(textarea);
        textarea.value = text;
        const selector = document.querySelector('#temp_element');
        selector.select();
        document.execCommand('copy');
        document.body.removeChild(textarea);
    }

    function showSnackbar(text) {
        const x = document.getElementById("snackbar");
        x.innerText = text;
        x.className = "show";
        setTimeout(function () {
            x.className = x.className.replace("show", "");
        }, 3000);
    }

    document.addEventListener("DOMContentLoaded", function () {
        document.querySelectorAll('.listingblock').forEach(
            blockElement => {
                const copyButton = document.createElement("div");
                copyButton.classList.add("copy-button");
                copyButton.classList.add("button")
                const codeElement = blockElement.getElementsByClassName("content")[0];
                copyButton.onclick = function () {
                    copyToClipboard(codeElement.innerText);
                    showSnackbar("Copied!")
                };
                codeElement.appendChild(copyButton);

                const title = blockElement.getElementsByClassName("title")[0];
                if (title) {

                    const folderButton = document.createElement("div");
                    folderButton.classList.add("folder-button");
                    folderButton.classList.add("button");
                    folderButton.onclick = function (e) {
                        console.log("folder")
                        copyToClipboard(title.innerText);
                        showSnackbar("Path copied!")
                        e.stopPropagation();
                    }
                    title.appendChild(folderButton);

                    const openButton = document.createElement("div");
                    openButton.classList.add("open-button");
                    openButton.classList.add("button")
                    title.appendChild(openButton);
                    title.onclick = function () {
                        console.log("ici")
                        let openClassName = "collapsed";
                        if (blockElement.classList.contains(openClassName)) {
                            blockElement.classList.remove(openClassName);
                        } else {
                            blockElement.classList.add(openClassName);
                        }
                    }
                }
            }
        );
    });

})();

