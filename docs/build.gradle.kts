val rubyDir = rootProject.file(".bin/ruby")
val bundleFile = File(rubyDir, "bin/bundle")

val installTask = tasks.register("installDocumentationDependencies") {
    val readLines = file("Gemfile.lock").readLines()
    var versionIndex = 0;
    readLines.forEachIndexed { index, s ->
        if (s.contains("BUNDLED WITH", false)) {
            versionIndex = index + 1
        }
    }
    val version = readLines[versionIndex].trim()
    exec {
        commandLine("gem", "install", "bundler", "-v", version, "--install-dir", rubyDir.path)
    }
    exec {
        commandLine(bundleFile.path, "config", "set", "--local", rubyDir.path)
        commandLine(bundleFile.path, "install")
    }
    inputs.file("Gemfile")
    val lockFile = File(buildDir, ".gemlock")
    outputs.file(lockFile)
    doLast {
        lockFile.parentFile.mkdirs()
        lockFile.createNewFile()
    }
}

fun getConfigValue(key: String): String {
    return file("config.adoc").readLines().find { it.startsWith(":$key:") }?.replace(":$key: ", "")!!
}

val prepareDiffTask = tasks.register("prepareDiff") {
    doFirst {
        val chromiumRepo = getConfigValue("crumbs_chromium_repo")
        val hash = getConfigValue("crumbs_chromium_commit_hash")
        val diffFolder = File(buildDir, "diff")
        diffFolder.mkdirs()
        val diffFile = File(diffFolder, "${hash}.diff")
        val diffAdoc = File(diffFolder, "${hash}.txt")
        val diffUrl = "$chromiumRepo/-/commit/${hash}.diff"
        ant.withGroovyBuilder { "get"("src" to diffUrl, "dest" to diffFile) }
        val pattern = "diff --git a/([^ ]+) .*".toRegex()
        diffAdoc.writeText("")
        var lastFile = ""
        var skipLine = 0
        diffFile.readLines().forEach { line ->
            val result = pattern.find(line)
            result?.let {
                if (lastFile.isNotEmpty()) {
                    diffAdoc.appendText("//end::${lastFile}[]\n")
                }
                lastFile = result.groups[1]!!.value
                diffAdoc.appendText("//tag::${lastFile}[]\n")
                skipLine = 4
            }

            if (skipLine > 0) {
                skipLine--
            } else {
                diffAdoc.appendText(line + "\n")
            }
        }
        diffAdoc.appendText("//end::${lastFile}[]\n")
    }
}


val screenshotTask = rootProject.tasks.findByPath(":crumbs-ui:updateScreenShots")
tasks.register<Exec>("startDocumentationServer") {
    commandLine("bundle", "exec", "jekyll", "serve")
    dependsOn(installTask, prepareDiffTask, screenshotTask)
}

tasks.register<Exec>("buildDocumentation") {
    commandLine("bundle", "exec", "jekyll", "build", "-d", "../public")
    dependsOn(installTask, prepareDiffTask, screenshotTask)
}

tasks.register<Delete>("clean") {
    delete(buildDir)
}