---
layout: default
title: ChangeLog
nav_order: 6
description: "Crumbs Android ChangeLog"
permalink: /changeLog
---
include::../../docs/config.adoc[]

:crumbs_core_changelog: https://eyeo.gitlab.io/crumbs/crumbs-core/changeLog


== ChangeLog

=== 1.3.6 - (15/03/2023)

==== Fixes and Improvements

- Rename anti fingerprint to fingerprint shield
- Fix email relay icon position
- link:{crumbs_core_changelog}#_1.3.6[Crumbs Core 1.3.6]

=== 1.3.5 - (23/01/2023)

==== Fixes and Improvements

- Improve Email Relay login UX
- link:{crumbs_core_changelog}#_1.3.5[Crumbs Core 1.3.5]

=== 1.3.4 - (05/01/2023)

==== Fixes and Improvements

- Refactor email relay icon size
- Encode browser url
- link:{crumbs_core_changelog}#_1.3.4[Crumbs Core 1.3.4]

=== 1.3.3 - (20/12/2022)

==== Fixes and Improvements

- Update TnC documentation
- Rename settings activity
- link:{crumbs_core_changelog}#_1.3.3[Crumbs Core 1.3.3]

=== 1.3.2 - (14/12/2022)

=== Features

- Show Terms and Conditions dialog before enabling privacy features

==== Fixes and Improvements

- Update Chromium documentation
- Add interests translations and update strings related to Crumbs
- link:{crumbs_core_changelog}#_1.3.2[Crumbs Core 1.3.2]

=== 1.3.1 - (17/11/2022)

==== Fixes and Improvements

- Add Crumbs incognito label
- refactor email relay dialog
- link:{crumbs_core_changelog}#_1.3.1[Crumbs Core 1.3.1]

=== 1.3.0 - (25/10/2022)

==== Features

- Add highlight settings feature
- Simplify settings UI
- Display Email Relay API errors
- Integrate Crumbs Core new privacy settings

==== Fixes and Improvements

- Rename insights to interests in the UI
- Change report issue link
- Fix Email Relay popup UI when the feature is disabled
- link:{crumbs_core_changelog}#_1.3.0[Crumbs Core 1.3.0]

=== 1.2.7 - (08/08/2022)

==== Features

- Add Anti-Fingerprinting setting

==== Fixes and Improvements

- Use latest Material Design themes and components
- Fix script not injected in WebView
- Update project dependencies
- link:{crumbs_core_changelog}#_1.2.7[Crumbs Core 1.2.7]

=== 1.2.6 - (12/07/2022)

- Bump version to align to the Crumbs Core fixes
- link:{crumbs_core_changelog}#_1.2.6[Crumbs Core 1.2.6]

=== 1.2.5 - (23/06/2022)

==== Fixes and Improvements

- Remove gender interest
- link:{crumbs_core_changelog}#_1.2.5[Crumbs Core 1.2.5]

=== 1.2.4 - (16/05/2022)

==== Fixes and Improvements

- Fix Protection Level setting UI
- link:{crumbs_core_changelog}#_1.2.4[Crumbs Core 1.2.4]

=== 1.2.3 - (22/02/2022)

==== Fixes and Improvements

- Fix crash in CrumbsUIContext with empty url
- link:{crumbs_core_changelog}#_1.2.3[Crumbs Core 1.2.3]

=== 1.2.2 - (02/02/2022)

==== Fixes and Improvements

- Add Crumbs SDK version in settings
- link:{crumbs_core_changelog}#_1.2.2[Crumbs Core 1.2.2]

=== 1.2.1 - (13/01/2022)

==== Fixes and Improvements

- Documentation fixes
- Fix WebView http client not following redirection
- Fix WebView using CNAME
- Change license to GPL V3
- Migrate crumbs-abp
- link:{crumbs_core_changelog}#_1.2.1[Crumbs Core 1.2.1]

=== 1.2.0 - (07/12/2021)

==== Fixes and Improvements

- Documentation fixes
- Enable proguard for demo app
- Integrate iframe JavaScript sandboxing
- Fix translation
- Update play button of demo app to temporary pause the tracking protection until the next session on long click
- link:{crumbs_core_changelog}#_1.2.0[Crumbs Core 1.2.0]

=== 1.1.6 - (13/08/2021)

==== Fixes and Improvements

- Improve email relay dialog
- link:{crumbs_core_changelog}#_1.1.6[Crumbs Core 1.1.6]

=== 1.1.5 - (10/08/2021)

==== Fixes and Improvements

- Update translation
- Add email relay setup dialog
- link:{crumbs_core_changelog}#_1.1.5[Crumbs Core 1.1.5]

=== 1.1.4 - (09/07/2021)

==== Fixes and Improvements

- Improve privacy settings UI
- link:{crumbs_core_changelog}#_1.1.4[Crumbs Core 1.1.4]

=== 1.1.3 - (18/06/2021)

==== Fixes and Improvements

- Improve Email Relay UI
- Add new Crumbs Settings UI events
- link:{crumbs_core_changelog}#_1.1.3[Crumbs Core 1.1.3]

=== 1.1.2 - (10/06/2021)

==== Fixes and Improvements

- Improve chromium compatibility
- Clean documentation
- link:{crumbs_core_changelog}#_1.1.2[Crumbs Core 1.1.2]

=== 1.1.1 - (10/06/2021)

==== Fixes and Improvements

- Update email relay dialog UI
- New translations
- link:{crumbs_core_changelog}#_1.1.1[Crumbs Core 1.1.1]

=== 1.1.0 - (26/04/2021)

==== Features

- Add CNAME cloaking option in UI

==== Fixes and Improvements

- link:{crumbs_core_changelog}#_1.1.0[Crumbs Core 1.1.0]

=== 1.0.4 - (20/04/2021)

==== Fixes and Improvements

- link:{crumbs_core_changelog}#_1.0.4[Crumbs Core 1.0.4]

=== 1.0.3 - (08/04/2021)

==== Fixes and Improvements

- Remove age interest from UI
- link:{crumbs_core_changelog}#_1.0.3[Crumbs Core 1.0.3]


=== 1.0.2 - (26/03/2021)

==== Features

- Add CrumbsUI event listeners to allow analytics tracking in Crumbs UI components.

==== Fixes and Improvements

- Update translation
- Fix document url in Webview
- link:{crumbs_core_changelog}#_1.0.2[Crumbs Core 1.0.2]


=== 1.0.1 - (12/03/2021)

==== Fixes and Improvements

- Improve Email Relay bottom dialog UI
- link:{crumbs_core_changelog}#_1.0.1[Crumbs Core 1.0.1]


=== 1.0.0 - (01/03/2021)

Initial release
