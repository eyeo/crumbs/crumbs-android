---
layout: default
title: UI
nav_order: 4
description: "UI module"
permalink: /ui/home
has_children: true
has_toc: true
---
include::../../docs/config.adoc[]

= UI
:showtitle: true

This module provides handy UI components to build your application interface with Crumbs.