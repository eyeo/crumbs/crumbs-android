-keepclassmembers class org.crumbs.chromium.CrumbsJni { *; }
-keep class org.crumbs.chromium.CrumbsJni { *; }

-keepclassmembers class kotlin.Pair { *; }
-keep class kotlin.Pair { *; }

-keepattributes EnclosingMethod
