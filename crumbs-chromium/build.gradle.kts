@Suppress("DSL_SCOPE_VIOLATION")
plugins {
    `maven-publish`
    alias(libs.plugins.android.library)
    alias(libs.plugins.kotlin.android)
}

val cppFolder = "$buildDir/generated/cpp"

android {

    defaultConfig {
        namespace = "org.crumbs.chromium"
        externalNativeBuild {
            cmake {
                cppFlags.clear()
            }
        }
    }

    sourceSets {
        named("main") {
            assets.srcDir(cppFolder)
        }
    }

    externalNativeBuild {
        cmake {
            path = file("src/main/cpp/CMakeLists.txt")
            version = "3.22.1"
        }
    }
}

tasks.register<Copy>("copyCppSources") {
    doFirst {
        file(cppFolder).deleteRecursively()
    }

    from("src/main/cpp") {
        include("**/*.h")
        rename {
            "included/$it"
        }
    }
    from("src/main/cpp") {
        include("**/*.cpp")
        rename {
            "src/$it"
        }
    }
    into(cppFolder)

    doLast {
        val outDir = fileTree(cppFolder) {
            include("**/*.h")
            include("**/*.cpp")
        }
        val paths = outDir.joinToString(",\n   ") {
            "\"" + it.path.replace(cppFolder, "") + "\""
        }
        val output = file("$cppFolder/crumbs_config.gni")
        output.delete()
        output.createNewFile()
        output.writeText("crumbs_sources = [ \n   $paths,\n]")
    }
}

tasks.getByName("preBuild").dependsOn("copyCppSources")

dependencies {
    api(libs.crumbs.core)
    implementation(libs.kotlin.stdlib)
    implementation(libs.ktor.client.encoding)
}
extra["artifact_id"] = "crumbs-chromium-android"
apply(file("../publish.gradle"))
