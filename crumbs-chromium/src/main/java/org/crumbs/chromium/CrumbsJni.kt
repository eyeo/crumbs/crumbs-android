package org.crumbs.chromium

import io.ktor.http.*
import org.crumbs.CrumbsAndroid
import org.crumbs.models.RequestType
import org.crumbs.service.DomainService

class CrumbsJni {

    companion object {

        private val integration by lazy { CrumbsAndroid.get().integration() }

        @JvmStatic
        fun isInitialized(): Boolean = CrumbsAndroid.isInitialized()

        @JvmStatic
        fun getAliasForUrl(
            documentUrl: String,
            requestUrl: String,
            requestId: Int
        ) = integration.getHostAlias(
            documentUrl.encodeURLQueryComponent(),
            requestUrl.encodeURLQueryComponent(),
            object : DomainService.DnsRequestListener {
                override fun onAliasReceived(alias: String?) {
                    onAliasResult(requestId, alias ?: "")
                }
            })

        @Suppress("KotlinJniMissingFunction")
        @JvmStatic
        external fun onAliasResult(requestId: Int, alias: String)

        @JvmStatic
        fun filterReceivedHeaders(
            tabId: String,
            documentUrl: String,
            requestUrl: String,
            requestType: Int,
            incognito: Boolean,
            headers: List<Pair<String, String>>
        ) = integration.filterReceivedHeaders(
            tabId,
            documentUrl.encodeURLQueryComponent(),
            requestUrl.encodeURLQueryComponent(),
            RequestType.values()[requestType],
            incognito,
            headers
        ).toMutableList()

        @JvmStatic
        fun filterSendHeaders(
            tabId: String,
            documentUrl: String,
            requestUrl: String,
            requestType: Int,
            incognito: Boolean,
            headers: List<Pair<String, String>>
        ) = integration.filterSendHeaders(
            tabId,
            documentUrl.encodeURLQueryComponent(),
            requestUrl.encodeURLQueryComponent(),
            RequestType.values()[requestType],
            incognito,
            headers
        ).toMutableList()

        @JvmStatic
        fun rewriteRequestUrl(
            documentUrl: String,
            requestUrl: String,
        ) =
            integration.rewriteRequestUrl(
                documentUrl.encodeURLQueryComponent(),
                requestUrl.encodeURLQueryComponent()
            ).decodeURLQueryComponent()

        @JvmStatic
        fun shouldBlockRequest(
            tabId: String,
            documentUrl: String,
            requestUrl: String,
            requestType: Int,
            headers: List<Pair<String, String>>
        ) = integration.shouldBlockRequest(
            tabId,
            documentUrl.encodeURLQueryComponent(),
            requestUrl.encodeURLQueryComponent(),
            RequestType.values()[requestType],
            headers
        )

        @JvmStatic
        fun createJavaScriptSnippet(tabId: String, documentUrl: String, requestType: Int) =
            integration.createJavaScriptSnippet(
                tabId,
                documentUrl.encodeURLQueryComponent(),
                RequestType.values()[requestType]
            )

    }

}