#ifndef CRUMBS_ANDROID_CRUMBSBRIDGE_H
#define CRUMBS_ANDROID_CRUMBSBRIDGE_H

#include <functional>
#include <jni.h>
#include <string>
#include <map>
#include <vector>
#include <scoped_allocator>

namespace crumbs {
    class CrumbsBridge {

    public:
        CrumbsBridge(JNIEnv *env, jclass crumbsClass, jclass pairClass);

        ~CrumbsBridge();

        bool IsInitialized();

        void FilterSendHeaders(
                const std::string &tab_id,
                const std::string &document_url,
                const std::string &request_url,
                const int request_type,
                bool incognito,
                std::vector<std::pair<std::string, std::string>> &headers
        );

        void FilterReceivedHeaders(
                const std::string &tab_id,
                const std::string &document_url,
                const std::string &request_url,
                const int request_type,
                bool incognito,
                std::vector<std::pair<std::string, std::string>> &headers
        );

        bool ShouldBlockRequest(
                const std::string &tab_id,
                const std::string &document_url,
                const std::string &request_url,
                const int request_type,
                std::vector<std::pair<std::string, std::string>> &headers
        );

        std::string RewriteRequestUrl(
                const std::string &document_url,
                const std::string &request_url
        );

        void GetHostAlias(const std::string &document_url, const std::string &request_url,
                          int request_id,
                          std::function<void(std::string)> callback);

        std::string CreateJavaScriptSnippet(const std::string &tab_id,
                                            const std::string &document_url,
                                            const int request_type);

        static const int REQUEST_TYPE_MAIN_FRAME = 0;
        static const int REQUEST_TYPE_I_FRAME = 1;
        static const int REQUEST_TYPE_RESOURCE = 2;

    private:
        static void onAliasResult(JNIEnv *env, jclass clazz, int request_id, jstring j_alias);

        jobject
        ConvertHeadersToJava(const std::vector<std::pair<std::string, std::string>> &headers);

        std::vector<std::pair<std::string, std::string>> ConvertHeadersToStd(const jobject headers);

        jstring StringToJava(const std::string &str);

        jobject CreateJavaList(int size);

        jobject CreatePair(jstring first, jstring second);

        static std::string StringToStd(JNIEnv *env, jstring str);

        JNIEnv *env;
        jclass global_crumbs_class;
        jclass global_list_class;
        jclass global_pair_class;

    };

}

#endif //CRUMBS_ANDROID_CRUMBSBRIDGE_H
