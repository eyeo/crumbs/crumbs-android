#include "CrumbsBridge.h"

#include <utility>
#include <android/log.h>

static jmethodID crumbsIsInitialized, crumbsFilterSendHeaders, crumbsFilterReceivedHeaders, crumbsRewriteRequestUrl, crumbsShouldBlockRequest, crumbsGetAliasForUrl, crumbsCreateJavaScriptSnippet;
static jmethodID listInit, listGet, listSize, listAdd;
static jmethodID pairInit, pairFirst, pairSecond;

crumbs::CrumbsBridge::CrumbsBridge(JNIEnv *env, jclass crumbs_class, jclass pair_class) {
    this->env = env;

    global_crumbs_class = crumbs_class;
    global_pair_class = pair_class;
    if (!global_crumbs_class) {
        global_crumbs_class = reinterpret_cast<jclass>(env->NewGlobalRef(
                env->FindClass("org/crumbs/chromium/CrumbsJni")));
    }
    if (!global_crumbs_class) {
        global_pair_class = reinterpret_cast<jclass>(env->NewGlobalRef(
                env->FindClass("kotlin/Pair")));
    }
    global_list_class = reinterpret_cast<jclass>(env->NewGlobalRef(
            env->FindClass("java/util/ArrayList")));

    crumbsIsInitialized = env->GetStaticMethodID(global_crumbs_class, "isInitialized", "()Z");
    crumbsFilterSendHeaders = env->GetStaticMethodID(global_crumbs_class,
                                                     "filterSendHeaders",
                                                     "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZLjava/util/List;)Ljava/util/List;");
    crumbsFilterReceivedHeaders = env->GetStaticMethodID(global_crumbs_class,
                                                         "filterReceivedHeaders",
                                                         "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZLjava/util/List;)Ljava/util/List;");
    crumbsRewriteRequestUrl = env->GetStaticMethodID(global_crumbs_class,
                                                     "rewriteRequestUrl",
                                                     "(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;");
    crumbsShouldBlockRequest = env->GetStaticMethodID(global_crumbs_class,
                                                      "shouldBlockRequest",
                                                      "(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/util/List;)Z");
    crumbsGetAliasForUrl = env->GetStaticMethodID(global_crumbs_class,
                                                  "getAliasForUrl",
                                                  "(Ljava/lang/String;Ljava/lang/String;I)V");
    crumbsCreateJavaScriptSnippet = env->GetStaticMethodID(global_crumbs_class,
                                                           "createJavaScriptSnippet",
                                                           "(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;");

    listInit = env->GetMethodID(global_list_class, "<init>", "(I)V");
    listGet = env->GetMethodID(global_list_class, "get", "(I)Ljava/lang/Object;");
    listSize = env->GetMethodID(global_list_class, "size", "()I");
    listAdd = env->GetMethodID(global_list_class, "add", "(Ljava/lang/Object;)Z");

    pairInit = env->GetMethodID(global_pair_class, "<init>",
                                "(Ljava/lang/Object;Ljava/lang/Object;)V");
    pairFirst = env->GetMethodID(global_pair_class, "getFirst", "()Ljava/lang/Object;");
    pairSecond = env->GetMethodID(global_pair_class, "getSecond", "()Ljava/lang/Object;");

    JNINativeMethod methods[] = {
            {"onAliasResult", "(ILjava/lang/String;)V", reinterpret_cast<void *>(onAliasResult)},
    };
    env->RegisterNatives(global_crumbs_class, methods, sizeof(methods) / sizeof(JNINativeMethod));

}

crumbs::CrumbsBridge::~CrumbsBridge() {
    env->DeleteGlobalRef(global_crumbs_class);
    global_crumbs_class = nullptr;

    env->DeleteGlobalRef(global_list_class);
    global_list_class = nullptr;

    env->DeleteGlobalRef(global_pair_class);
    global_pair_class = nullptr;

    this->env = nullptr;
}

bool crumbs::CrumbsBridge::IsInitialized() {
    jboolean result = env->CallStaticBooleanMethod(global_crumbs_class, crumbsIsInitialized);
    return (bool) (result == JNI_TRUE);
}

void crumbs::CrumbsBridge::FilterSendHeaders(const std::string &tab_id,
                                             const std::string &document_url,
                                             const std::string &request_url,
                                             const int request_type,
                                             bool incognito,
                                             std::vector<std::pair<std::string, std::string>> &headers) {
    jstring j_tab_id = StringToJava(tab_id);
    jstring j_document_url = StringToJava(document_url);
    jstring j_request_url = StringToJava(request_url);
    jobject j_headers = ConvertHeadersToJava(headers);
    jobject j_fix_headers = env->CallStaticObjectMethod(global_crumbs_class,
                                                        crumbsFilterSendHeaders,
                                                        j_tab_id, j_document_url,
                                                        j_request_url, request_type, incognito,
                                                        j_headers);
    env->DeleteLocalRef(j_tab_id);
    env->DeleteLocalRef(j_document_url);
    env->DeleteLocalRef(j_request_url);
    env->DeleteLocalRef(j_headers);
    std::vector<std::pair<std::string, std::string>> fixHeaders = ConvertHeadersToStd(
            j_fix_headers);
    headers.clear();
    headers.insert(headers.end(), fixHeaders.begin(), fixHeaders.end());
}

void crumbs::CrumbsBridge::FilterReceivedHeaders(const std::string &tab_id,
                                                 const std::string &document_url,
                                                 const std::string &request_url,
                                                 const int request_type,
                                                 bool incognito,
                                                 std::vector<std::pair<std::string, std::string>> &headers) {
    jstring j_tab_id = StringToJava(tab_id);
    jstring j_document_url = StringToJava(document_url);
    jstring j_request_url = StringToJava(request_url);
    jobject j_headers = ConvertHeadersToJava(headers);
    jobject j_fix_headers = env->CallStaticObjectMethod(global_crumbs_class,
                                                        crumbsFilterReceivedHeaders,
                                                        j_tab_id, j_document_url,
                                                        j_request_url, request_type, incognito, j_headers);
    env->DeleteLocalRef(j_tab_id);
    env->DeleteLocalRef(j_document_url);
    env->DeleteLocalRef(j_request_url);
    env->DeleteLocalRef(j_headers);
    std::vector<std::pair<std::string, std::string>> fixHeaders = ConvertHeadersToStd(
            j_fix_headers);
    headers.clear();
    headers.insert(headers.end(), fixHeaders.begin(), fixHeaders.end());
}

std::string crumbs::CrumbsBridge::RewriteRequestUrl(const std::string &document_url,
                                                    const std::string &request_url) {
    jstring j_document_url = StringToJava(document_url);
    jstring j_request_url = StringToJava(request_url);
    auto newUrl = static_cast<jstring>(env->CallStaticObjectMethod(global_crumbs_class,
                                                                   crumbsRewriteRequestUrl,
                                                                   j_document_url,
                                                                   j_request_url));
    env->DeleteLocalRef(j_document_url);
    env->DeleteLocalRef(j_request_url);
    return StringToStd(env, newUrl);
}


bool crumbs::CrumbsBridge::ShouldBlockRequest(const std::string &tab_id,
                                              const std::string &document_url,
                                              const std::string &request_url,
                                              const int request_type,
                                              std::vector<std::pair<std::string, std::string>> &headers) {
    jstring j_tab_id = StringToJava(tab_id);
    jstring j_document_url = StringToJava(document_url);
    jstring j_request_url = StringToJava(request_url);
    jobject j_headers = ConvertHeadersToJava(headers);
    auto blocked = env->CallStaticBooleanMethod(global_crumbs_class,
                                                crumbsShouldBlockRequest,
                                                j_tab_id,
                                                j_document_url,
                                                j_request_url, request_type,
                                                j_headers);
    env->DeleteLocalRef(j_tab_id);
    env->DeleteLocalRef(j_document_url);
    env->DeleteLocalRef(j_request_url);
    env->DeleteLocalRef(j_headers);
    return blocked;
}

static std::map<int, std::function<void(std::string)>> alias_callbacks;

void crumbs::CrumbsBridge::GetHostAlias(const std::string &document_url,
                                        const std::string &request_url,
                                        int request_id,
                                        std::function<void(std::string)> callback) {
    alias_callbacks[request_id] = std::move(callback);
    jstring j_document_url = StringToJava(document_url);
    jstring j_request_url = StringToJava(request_url);
    env->CallStaticVoidMethod(global_crumbs_class,
                              crumbsGetAliasForUrl,
                              j_document_url,
                              j_request_url,
                              request_id);
    env->DeleteLocalRef(j_document_url);
    env->DeleteLocalRef(j_request_url);
}

void crumbs::CrumbsBridge::onAliasResult(JNIEnv *env, jclass clazz, int request_id,
                                         jstring jAlias) {
    auto alias = crumbs::CrumbsBridge::StringToStd(env, jAlias);
    alias_callbacks[request_id](alias);
    alias_callbacks.erase(request_id);
}


std::string crumbs::CrumbsBridge::CreateJavaScriptSnippet(const std::string &tab_id,
                                                          const std::string &document_url,
                                                          const int request_type) {
    jstring jTabId = StringToJava(tab_id);
    jstring j_document_url = StringToJava(document_url);
    auto snippet = static_cast<jstring>(env->CallStaticObjectMethod(global_crumbs_class,
                                                                    crumbsCreateJavaScriptSnippet,
                                                                    jTabId,
                                                                    j_document_url,
                                                                    request_type));
    env->DeleteLocalRef(j_document_url);
    return StringToStd(env, snippet);
}

std::string crumbs::CrumbsBridge::StringToStd(JNIEnv *env, jstring str) {
    if (!str) {
        return std::string();
    }

    const char *cStr = env->GetStringUTFChars(str, 0);
    std::string ret(cStr);
    env->ReleaseStringUTFChars(str, cStr);
    env->DeleteLocalRef(str);
    return ret;
}

jstring crumbs::CrumbsBridge::StringToJava(const std::string &str) {
    return env->NewStringUTF(str.c_str());
}

jobject crumbs::CrumbsBridge::CreateJavaList(int size) {
    return env->NewObject(global_list_class, listInit, size);
}

jobject crumbs::CrumbsBridge::CreatePair(jstring first, jstring second) {
    jobject jPair = env->NewObject(global_pair_class, pairInit, first, second);
    env->DeleteLocalRef(first);
    env->DeleteLocalRef(second);
    return jPair;
}

jobject crumbs::CrumbsBridge::ConvertHeadersToJava(
        const std::vector<std::pair<std::string, std::string>> &headers) {
    jobject list = CreateJavaList(headers.size());

    for (const auto &entry: headers) {
        jstring key = StringToJava(entry.first);
        jstring value = StringToJava(entry.second);
        jobject pair = CreatePair(key, value);
        env->CallBooleanMethod(list, listAdd, pair);
        env->DeleteLocalRef(pair);
    }
    return list;
}

std::vector<std::pair<std::string, std::string>>
crumbs::CrumbsBridge::ConvertHeadersToStd(const jobject headers) {
    std::vector<std::pair<std::string, std::string>> result;

    for (int i = 0; i < env->CallIntMethod(headers, listSize); ++i) {
        jobject entry = env->CallObjectMethod(headers, listGet, i);
        auto j_first = static_cast<jstring>(env->CallObjectMethod(entry, pairFirst));
        auto j_second = static_cast<jstring>(env->CallObjectMethod(entry, pairSecond));
        env->DeleteLocalRef(entry);
        result.emplace_back(StringToStd(env, j_first), StringToStd(env, j_second));
    }

    return result;
}


